<?php

/**
 * @file
 * Admin configuration file of vactory_cross_content module.
 */

/**
 * Implements hook_form().
 */
function vactory_cross_content_config_form($form, &$form_state) {
  $ct = node_type_get_types();

  $form['vcc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vactory cross content configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['vcc']['vcc_node_types_list'] = array('#type' => 'vertical_tabs');
  $fieldset = array();
  foreach ($ct as $type) {

    $default_values = variable_get("vcc_{$type->type}") ? variable_get("vcc_{$type->type}") : array();

    $fieldset[$type->type] = array(
      '#type' => 'fieldset',
      '#title' => $type->name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'vcc_node_types_list',
    );

    $fieldset[$type->type]["vcc_{$type->type}_enable_type"] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Cross content for this content type'),
      '#description' => t('Check this to enable Cross Content for this content type'),
      '#default_value' => isset($default_values['enable_type']) ? $default_values['enable_type'] : FALSE,
    );

    $fieldset[$type->type]["vcc_{$type->type}_block_title"] = array(
      '#type' => 'textfield',
      '#title' => t('Cross Content Block title'),
      '#description' => t('Cross Content Block title. Keep empty for no title'),
      '#default_value' => isset($default_values['block_title']) ? $default_values['block_title'] : "",
    );

    $fieldset[$type->type]["vcc_{$type->type}_tags_field"] = array(
      '#title' => t('Taxonomy field'),
      '#type' => 'select',
      '#description' => t('Taxonomy field to select cross content'),
      '#options' => _vactory_cross_content_get_term_reference_fields($type),
      '#default_value' => isset($default_values['tags_field']) ? $default_values['tags_field'] : "",
    );

    $fieldset[$type->type]["vcc_{$type->type}_nbr_elements"] = array(
      '#type' => 'textfield',
      '#title' => t('Number of nodes to display'),
      '#description' => t('Select the number of node to display in the cross content bloc'),
      '#default_value' => isset($default_values['nbr_elements']) ? $default_values['nbr_elements'] : 3,
    );

    $fieldset[$type->type]["vcc_{$type->type}_use_ims"] = array(
      '#type' => 'checkbox',
      '#title' => t("Utiliser l'affichage en deux tableaux."),
      '#description' => t('Utiliser le widget "Improved Multi Select" sur le champ "cross content"'),
      '#default_value' => isset($default_values['use_ims']) ? $default_values['use_ims'] : 1,
    );

    $fieldset[$type->type]["vcc_{$type->type}_ct_to_include"] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content type to include as cross content'),
      '#options' => _vactory_cross_content_get_ct_names(),
      '#default_value' => isset($default_values['ct_to_include']) ? $default_values['ct_to_include'] : array(),
    );

    $form['vcc'][$type->type] = $fieldset[$type->type];
  }

  $form['#submit'][] = 'vactory_cross_content_config_form_submit';

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  return $form;
}

/**
 * Vactory_cross_content_config_form submit handler.
 */
function vactory_cross_content_config_form_submit($form, &$form_state) {

  $ct = node_type_get_types();
  $enabled_type = array();
  foreach ($ct as $type) {
    $values = array();
    $values['enable_type'] = $form_state['values']["vcc_{$type->type}_enable_type"];
    $values['block_title'] = $form_state['values']["vcc_{$type->type}_block_title"];
    $values['tags_field'] = $form_state['values']["vcc_{$type->type}_tags_field"];
    $values['nbr_elements'] = $form_state['values']["vcc_{$type->type}_nbr_elements"];
    $values['use_ims'] = $form_state['values']["vcc_{$type->type}_use_ims"];
    $values['ct_to_include'] = $form_state['values']["vcc_{$type->type}_ct_to_include"];

    if ($values['enable_type'] == 1) {
      $enabled_types[] = $type->type;
    }

    variable_set("vcc_{$type->type}", $values);

    _vactory_cross_content_set_cc_field($type, $values['ct_to_include'], $values['enable_type']);

    // Utiliser le widget IMS.
    if ($values['use_ims'] == 1) {
      if (module_exists("improved_multi_select")) {
        $ct_name_id = drupal_clean_css_identifier($type->type);
        $cross_content_selector = "select#edit-field-{$ct_name_id}-cc-und";
        $ims_value = variable_get("improved_multi_select:selectors_white");
        if (strpos($ims_value, $cross_content_selector) === FALSE) {
          variable_set("improved_multi_select:selectors_white", $ims_value . PHP_EOL . $cross_content_selector);
        }
      }
    }
  }
  variable_set("vcc_enabled_types", $enabled_types);

}
