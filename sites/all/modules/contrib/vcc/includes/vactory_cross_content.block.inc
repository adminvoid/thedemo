<?php

/**
 * @file
 * Block functions of vactory_cross_content module.
 */

/**
 * Implements hook_block_info().
 */
function vactory_cross_content_block_info() {
  $blocks['cross_content_block'] = array(
    'info' => t('cross content block'),
    'module' => 'vactory_cross_content',
    'delta'  => 'cross_content_block',
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function vactory_cross_content_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case('cross_content_block'):
      $block['subject'] = NULL;
      $block['content'] = _vactory_cross_content_render();
      break;
  }

  return $block;
}
