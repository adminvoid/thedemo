<?php

/**
 * @file
 * Page functions of vactory_cross_content module.
 */

/**
 * Implements hook_page_build().
 */
function vactory_cross_content_page_build(&$page) {
  if (!path_is_admin(current_path())) {
    $added_ct = variable_get('vcc_enabled_types', array());
    if ($node = menu_get_object()) {

      if (in_array($node->type, $added_ct)) {

        $block = block_load('vactory_cross_content', 'cross_content_block');
        $block_render = _block_get_renderable_array(_block_render_blocks(array($block)));
        $page['content']['cross_content_block'] = $block_render;
      }
    }
  }

}
