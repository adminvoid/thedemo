<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<article
  class="article-card standing-layout <?php print $classes; ?>"<?php print $attributes; ?>">

<div class="article-card__thumbnail">
  <a
    href="<?php print $node_url; ?>">
    <?php print v_gen_render_image($visual); ?>
  </a>

</div>

<div class="article-card__content">

  <?php if (!empty($title)): ?>
    <h3 class="article-card__title"<?php print $title_attributes; ?>><a
        href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>

  <div
    class="article-card__permalink">
    <a class="permalink"
       href="<?php print $node_url; ?>"><?php print t('See more'); ?></a>
  </div>


</div>

</article>
