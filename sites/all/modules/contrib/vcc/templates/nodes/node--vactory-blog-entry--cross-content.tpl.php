<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<article
  class="article-card standing-layout <?php print $classes; ?>"<?php print $attributes; ?>">

<div class="article-card__thumbnail">
  <a
    href="<?php print $node_url; ?>">
    <?php print render($content['field_v_blog_image']); ?>
  </a>
  <div
    class="article-card__tags pinned"><?php print render($content['field_v_blog_taxonomy']); ?></div>
</div>

<div class="article-card__content">

  <?php if (!empty($title)): ?>
    <h3 class="article-card__title"<?php print $title_attributes; ?>><a
        href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
    <div class="article-card__hr"></div>
  <?php endif; ?>

  <div
    class="article-card__excerpt">
    <?php print render($content['field_v_blog_description']); ?>
  </div>

  <div
    class="article-card__permalink">
    <a class="permalink"
       href="<?php print $node_url; ?>"><?php print t('Read more'); ?></a>
  </div>


</div>

</article>
