<?php

/**
 * @file
 * Cross content block template.
 */
?>
<div class="cross-content-wrapper container">

  <?php if (!empty($cross_content_nodes)): ?>
    <h2 class="cross-content__title text-center"><?php print t($block_title); ?></h2>
  <?php endif; ?>

  <div class="cross-content__content slider-grid row">
    <?php foreach ($cross_content_nodes as $node): ?>
      <div class="col-md-4"><?php print drupal_render($node); ?></div>
    <?php endforeach; ?>
  </div>

</div>
