<?php
/**
 * @file
 * vactory_accordion.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function vactory_accordion_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_accordion'.
  $field_bases['field_accordion'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_accordion',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'faqfield',
    'settings' => array(
      'advanced' => array(
        'answer_rows' => 3,
        'answer_title' => 'Answer',
        'question_length' => 255,
        'question_rows' => 1,
        'question_title' => 'Question',
      ),
      'answer_widget' => 'text_format',
      'format' => 'full_html',
    ),
    'translatable' => 0,
    'type' => 'faqfield',
  );

  // Exported field_base: 'field_accordion_description'.
  $field_bases['field_accordion_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_accordion_description',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_accordion_link'.
  $field_bases['field_accordion_link'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_accordion_link',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  return $field_bases;
}
