<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

if ($background['type'] == 'image' && !empty($background['image_url'])) {
  $classes .= " is-image";
}

?>

<div class="background <?php print $classes; ?>"
  <?php if (in_array($background['type'], array(
      'solid',
      'both'
    )) && !empty($background['color'])
  ) {
    print 'style="background-color: #' . $background['color'] . '"';
  }
  ?>
  <?php if ($background['type'] == 'image' && !empty($background['image_url'])) {
    print 'style="background-image: url(' . $background['image_url'] . ')"';
  }
  ?>
  >
  <?php if ($background['type'] == 'both' && !empty($background['image'])): ?>
    <div class="background--image">
      <?php print v_bg_render_cover($background['image'], $picture_name = 'vactory_node_wide', $image_style_fallback = 'node-wide-fullcustom_user_large_1x'); ?>
    </div>
  <?php endif; ?>
  <div class="container">
    <div class="row">
      <?php print render($content); ?>
    </div>
  </div>
</div>
