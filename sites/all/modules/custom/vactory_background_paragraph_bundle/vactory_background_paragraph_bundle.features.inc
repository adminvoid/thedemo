<?php
/**
 * @file
 * vactory_background_paragraph_bundle.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function vactory_background_paragraph_bundle_paragraphs_info() {
  $items = array(
    'paragraph_background' => array(
      'name' => 'Paragraph Background',
      'bundle' => 'paragraph_background',
      'locked' => '1',
    ),
  );
  return $items;
}
