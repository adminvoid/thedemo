<?php
/**
 * @file
 * vactory_block_paragraph_bundle.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function vactory_block_paragraph_bundle_paragraphs_info() {
  $items = array(
    'paragraph_block' => array(
      'name' => 'Paragraph Block',
      'bundle' => 'paragraph_block',
      'locked' => '1',
    ),
  );
  return $items;
}
