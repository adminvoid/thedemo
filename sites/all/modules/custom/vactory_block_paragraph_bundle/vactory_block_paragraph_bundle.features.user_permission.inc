<?php
/**
 * @file
 * vactory_block_paragraph_bundle.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vactory_block_paragraph_bundle_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create paragraph content paragraph_block'.
  $permissions['create paragraph content paragraph_block'] = array(
    'name' => 'create paragraph content paragraph_block',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'delete paragraph content paragraph_block'.
  $permissions['delete paragraph content paragraph_block'] = array(
    'name' => 'delete paragraph content paragraph_block',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'update paragraph content paragraph_block'.
  $permissions['update paragraph content paragraph_block'] = array(
    'name' => 'update paragraph content paragraph_block',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content paragraph_block'.
  $permissions['view paragraph content paragraph_block'] = array(
    'name' => 'view paragraph content paragraph_block',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  return $permissions;
}
