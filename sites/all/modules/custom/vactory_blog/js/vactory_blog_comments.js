/**
 * @file
 * Better Comments provides option to configure the comment system.
 */


(function ($) {
    $.fn.vactoryBlogReplyFormComment = function (form) {
        $('#vactory-blog--reply-form').html(form);

        if (window.is_user_fb_connected) {
            window.form_states();
        }
        else {
            window.form_states_defaults();
        }

        $('#vactory-blog--reply-modal').modal('show');
    };
})(jQuery);

(function ($) {
    Drupal.behaviors.betterComments = {
        attach: function (context, settings) {
            // Comments timeago
            var lang = $('html').attr('xml:lang');
            lang = lang.replace('eng', 'en');
            $('.comment-section .timeago').each(function () {
                $(this).text(moment($(this).attr('title'), null, lang).fromNow());
            });

            var comment_limit = 10;

            $('a.comments-reply-count', context).click(function (e) {
                e.preventDefault();
                replyid = $(this).attr("id").split('-')[2];
                $('.parent-' + replyid).filter(function (index) {
                    return index <= comment_limit - 1;
                }).show('slow');

                if ($('.parent-' + replyid).length > comment_limit) {
                    var btn = $('<div class="comment-show-more"><button class="btn btn-primary btn-block">' + Drupal.t("Show more") + '</button></div>');
                    btn.data("replyid", replyid);
                    btn.data("limit", comment_limit);
                    btn.on('click', function (e) {
                        var b_replyid = $(this).data('replyid');
                        var b_limit = $(this).data('limit');

                        $('.parent-' + b_replyid).filter(function (index) {
                            return index >= b_limit && index <= (b_limit * 2) - 1;
                        }).show('slow');

                        if ($('.parent-' + b_replyid + ':hidden').length) {
                            $(this).data('limit', b_limit * 2);
                        }
                        else {
                            $(this).hide();
                        }
                    });
                    btn.insertAfter($('.parent-' + replyid).last());
                }

                $(this).off('click');
                $(this).on('click', function(ev) {
                    ev.preventDefault();
                });
            });

            $('.better-comment-wrapper.depth-0').filter(function(index) {
                return index > comment_limit - 1;
            }).hide('fast');

            if ($('.better-comment-wrapper.depth-0').length > comment_limit && !$('.parent.comment-show-more').length) {
                var btn = $('<div class="parent comment-show-more"><button class="btn btn-primary btn-block">' + Drupal.t("Show more") + '</button></div>');
                btn.data("limit", comment_limit);

                btn.on('click', function (e) {
                    var b_limit = $(this).data('limit');

                    $('.better-comment-wrapper.depth-0').filter(function (index) {
                        return index >= b_limit && index <= (b_limit * 2) - 1;
                    }).show('slow');

                    if ($('.better-comment-wrapper.depth-0:hidden').length) {
                        $(this).data('limit', b_limit * 2);
                    }
                    else {
                        $(this).hide();
                    }
                });

                btn.appendTo('.comment-section');
            }

            $("a.better-comments-reply", context).click(function () {
                replyid = $(this).attr("id").split('-');
                $(this).hide('slow');
            });
            $("a.reply-cancel, a.preview-cancel", context).click(function () {
                cancelid = $(this).attr("id").split('-');
                $('#reply-' + cancelid[1]).show('slow');
            });
            $("a.better-comments-delete", context).click(function () {
                deleteid = $(this).attr("id").split('-');
                $(this).hide('slow');
            });
            $("a.delete-cancel", context).click(function () {
                cancelid = $(this).attr("id").split('-');
                $('#delete-' + cancelid[2]).show('slow');
            });
            /*if ($('.better-comments-reply').is(":hidden")) {
                $.each($('.better-comments-reply', context), function () {
                    child = $(this).closest(".better-comment-wrapper");

                    console.log($(this));
                    console.log(child);
                    console.log($(child).parents(".better-comment-wrapper"));

                    parent = $(child).parents(".better-comment-wrapper").attr("id").split('-');

                    if (($("#" + parent + " .comment-form").length > 0)) {
                    }
                    else {
                        $('#reply-' + parent[2]).show('slow');
                    }
                });
            }*/
        }
    }
}(jQuery));
