<?php

/**
 * @file
 * Default theme implementation to provide an HTML container for comments.
 *
 * Available variables:
 * - $content: The array of content-related elements for the node. Use
 *   render($content) to print them all, or
 *   print a subset such as render($content['comment_form']).
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default value has the following:
 *   - comment-wrapper: The current template type, i.e., "theming hook".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the comments are attached to.
 * The constants below the variables show the possible values and should be
 * used for comparison.
 * - $display_mode
 *   - COMMENT_MODE_FLAT
 *   - COMMENT_MODE_THREADED
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_comment_wrapper()
 *
 * @ingroup themeable
 */
?>
<div id="comments" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($content['comments'] && $node->type != 'forum'): ?>
    <h2
      class="title"><?php print t('@nb_comments Comments', array('@nb_comments' => $comment_count)); ?></h2>
  <?php endif; ?>
  <?php if ($content['comment_form']): ?>
    <?php if (user_is_anonymous()) { ?>
      <!-- Facebook Template -->
    <div id="vactory-blog--facebook-connect">
      <div class="vactory-blog--user-info"></div>
      <div class="vactory-blog--login">
        <a href="#." rel="nofollow" id="vactory-blog--login"><?php print t('Login with Facebook'); ?></a>
      </div>
      <div class="vactory-blog--logout hidden">
        <a href="#." rel="nofollow" id="vactory-blog--logout"><?php print t('Disconnect'); ?></a>
      </div>
    </div>
      <!-- / Facebook Template -->
    <?php } ?>
    <div id="ajax-status-messages-wrapper"></div>
    <?php print render($content['comment_form']); ?>
  <?php endif; ?>

  <div class="comment-section">
    <?php print render($content['comments']); ?>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="vactory-blog--reply-modal" tabindex="-1" role="dialog" aria-labelledby="replyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="replyModal"><?php print t('Reply'); ?></h4>
      </div>
      <div class="modal-body" id="vactory-blog--reply-form">

      </div>
    </div>
  </div>
</div>
