<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="container">
  <div class="row">
    <article
      class="<?php print $classes; ?> col-md-12 clearfix"<?php print $attributes; ?>>

      <?php print render($title_prefix); ?>
      <?php if (!$page && !empty($title)): ?>
        <h2<?php print $title_attributes; ?>><a
            href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
      <?php print render($title_suffix); ?>


      <div class="content"<?php print $content_attributes; ?>>
        <?php if (isset($content['field_v_blog_summary'])): ?>
          <div class="blog-summary">
            <?php print render($content['field_v_blog_summary']); ?>
          </div>
        <?php endif; ?>

        <div class="blog-media-file">
          <?php
          if (isset($content['field_v_blog_video'])) {
            print render($content['field_v_blog_video']);
          }
          else {
            print render($content['field_v_blog_image']);
          }
          ?>
        </div>

        <?php if (isset($content['body'])): ?>
          <div class="blog-content">
            <?php print render($content['body']); ?>
          </div>
        <?php endif; ?>
      </div>

      <?php if (isset($content['field_v_blog_files'])): ?>
        <div class="files">
          <?php print render($content['field_v_blog_files']); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['author_name'])): ?>
        <div class="author">
          <div class="author-container-center">

            <?php if (isset($content['author_image'])): ?>
              <div class="author-image">
                <?php print render($content['author_image']); ?>
              </div>
            <?php endif; ?>

            <div class="author-content">
              <div class="author-name">
                <?php print $content['author_name']; ?>
              </div>

              <?php if (isset($content['author_post'])): ?>
                <div class="author-post">
                  <?php print render($content['author_post']); ?>
                </div>
              <?php endif; ?>

              <div class="author-footer">
                <?php if (isset($content['author_post'])): ?>
                  <div class="author-social">
                    <?php print render($content['author_social']); ?>
                  </div>
                <?php endif; ?>

                <?php if (isset($content['date_published'])): ?>
                  <div class="submitted">
                    <?php print t('Published on') . ' ' . render($content['date_published']); ?>
                  </div>
                <?php endif; ?>
              </div>
            </div>

          </div>
          <!-- ./ .author-container-center -->
        </div>
        <!-- ./ .author -->
      <?php endif; ?>

      <?php print render($content['links']); ?>

      <?php print render($content['comments']); ?>

    </article>
  </div>
</div>