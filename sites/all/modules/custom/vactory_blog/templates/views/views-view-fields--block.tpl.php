<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists.
 * This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field.
 * Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing
 * the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<?php
$row_classes = 'blog-entry--videotype';
if (empty($fields['field_v_blog_video']->content)) {
  $row_classes = 'blog-entry--imagetype';

}
?>
<div class="blog-entry <?php print $row_classes; ?>">
  <div class="blog-entry--media">
    <?php print $view->render_field("field_v_blog_image", $view->row_index); ?>
    <?php print $view->render_field("path", $view->row_index); ?>
  </div>
  <div class="content">
    <div class="blog-entry--title">
      <?php print $view->render_field("title_1", $view->row_index); ?>
    </div>
    <div class="blog-entry--description">
      <?php print $view->render_field("field_v_blog_description", $view->row_index); ?>
    </div>

    <div class="blog-entry--view-count">
      <?php print t("Views :"); ?>
      <?php print $view->render_field("field_v_blog_view_count", $view->row_index); ?>
    </div>
    <div class="blog-entry--publication">
      <span><?php print t('Published by'); ?> </span>
      <?php print $view->render_field("field_v_blog_author", $view->row_index); ?>
      <span><?php print t('at'); ?> </span>
      <?php print $view->render_field("created", $view->row_index); ?>
    </div>

  </div>
</div>
