<?php

/**
 * @file
 * vactory_blog.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function vactory_blog_captcha_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_vactory_blog_entry_form';
  $captcha->module = 'recaptcha';
  $captcha->captcha_type = 'reCAPTCHA';
  $export['comment_node_vactory_blog_entry_form'] = $captcha;

  return $export;
}
