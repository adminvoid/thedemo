<?php

/**
 * @file
 * Code for the Vactory Blog feature.
 */

include_once 'vactory_blog.features.inc';

/**
 * Define the default variable name fo this module.
 */
define('VACTORY_BLOG_VARIABLE_DATA', 'vactory_blog:data');

/**
 * Implements hook_views_api_alter().
 */
function vactory_blog_views_api_alter(&$apis) {
  // Define Vactory Pertner Views default templates path.
  if (!empty($apis['vactory_blog']) && $apis['vactory_blog']['api'] == '3.0') {
    $apis['vactory_blog']['template path'] = drupal_get_path('module', 'vactory_blog') . '/templates';
  }
}

/**
 * Get variable name.
 *
 * @return string
 *   Multilanguage variable name.
 */
function _vactory_blog_get_variable_name() {
  global $language;
  return VACTORY_BLOG_VARIABLE_DATA . $language->language;
}

/**
 * Set admin messages.
 */
function _vactory_blog_set_admin_form_messages() {
  drupal_set_message(t("This is a multilingual form. Make sure to fill it in the other languages"), 'warning');
}

/**
 * Implements hook_libraries_info().
 *
 * For defining external libraries.
 */
function vactory_blog_libraries_info() {
  $libraries['moment'] = array(
    'name'              => 'Moment',
    'vendor url'        => 'http://momentjs.com/',
    'download url'      => 'https://github.com/moment/moment/archive/2.17.1.zip',
    'version callback' => '_vactory_blog_libs_moment_version',
    'files'             => array(
      'js' => array('min/moment-with-locales.min.js'),
    ),
  );

  return $libraries;
}

/**
 * Return version for moment.
 *
 * @return string
 *      Description of the return value, which is a string
 */
function _vactory_blog_libs_moment_version() {
  return "2.6.0";
}

/**
 * Implements hook_menu().
 */
function vactory_blog_menu() {
  $items['admin/config/content/vactory_blog'] = array(
    'title' => 'Vactory Blog',
    'description' => 'Configure listing blog',
    'page callback' => 'drupal_get_form',
    'file' => 'include/vactory_blog.admin.inc',
    'page arguments' => array('vactory_blog_admin_settings'),
    'access arguments' => array('administer vactory blog'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function vactory_blog_permission() {
  $permissions = array();
  $permissions['administer vactory blog'] = array(
    'title' => t('Administer Vactory Blog'),
    'description' => t('Access vactory blog settings pages.'),
  );

  return $permissions;
}

/**
 * Implements hook_theme().
 */
function vactory_blog_theme() {
  return array(
    'node__vactory_blog_entry' => array(
      'render element' => 'content',
      'base hook' => 'node',
      'template' => 'node--vactory_blog_entry',
      'path' => drupal_get_path('module', 'vactory_blog') . '/templates/node',
    ),
    'vactory_blog_better_comments' => array(
      'template' => 'templates/comments/better-comments',
      'render element' => 'comment',
    ),
    'vactory_blog_wrapper' => array(
      'template' => 'templates/comments/better-comments-wrapper',
      'render element' => 'comment-wrapper',
    ),
  );
}

/**
 * Implements hook_node_view().
 */
function vactory_blog_node_view($node, $view_mode, $langcode) {
  if ("vactory_blog_entry" !== $node->type) {
    return;
  }

  // Get variables settings.
  $default_value = variable_get(_vactory_blog_get_variable_name(), array());

  // Add Javascript files.
  $module_path = drupal_get_path('module', 'vactory_blog');
  // Facebook AppID.
  $appID = (isset($default_value['vactory_blog']) && isset($default_value['vactory_blog']['facebook_appid'])) ? $default_value['vactory_blog']['facebook_appid'] : '';
  drupal_add_js(
    array(
      'vactory_blog' => array(
        'appid' => "$appID",
      ),
    ),
    array('type' => 'setting')
  );

  if (user_is_anonymous()) {
    drupal_add_js($module_path . '/js/facebook-connect--comments.js', array('scope' => 'footer'));
  }

  // Check if Moment is present.
  if (($library = libraries_detect('moment')) && !empty($library['installed'])) {
    // Load Slick assets.
    libraries_load('moment');
  }
  else {
    $error_message = $library['error message'];
    drupal_set_message($error_message, 'error');
  }

  // Get author.
  $author_term = taxonomy_term_load($node->field_v_blog_author[LANGUAGE_NONE][0]['tid']);
  $node->content['author_name'] = $author_term->name;

  // Get author picto.
  $field_picto = field_get_items('taxonomy_term', $author_term, 'field_v_blog_author_picto');
  if ($field_picto) {
    $node->content['author_image'] = field_view_value('taxonomy_term', $author_term, 'field_v_blog_author_picto', $field_picto[0], array(
      'type' => 'image',
      'settings' => array(
        'image_style' => 'vactory_blog_author_picto',
      ),
    ));
  }

  // Get author post.
  $field_post = field_get_items('taxonomy_term', $author_term, 'field_field_v_blog_author_post');
  if ($field_post) {
    $node->content['author_post'] = field_view_value('taxonomy_term', $author_term, 'field_field_v_blog_author_post', $field_post[0]);
  }

  // Get author social media links.
  $field_social = field_get_items('taxonomy_term', $author_term, 'field_field_v_blog_author_social');

  // Format author social links as array.
  $social_links = array();
  if (isset($field_social) && is_array($field_social)) {
    foreach ($field_social as $item) {
      $fc = field_collection_field_get_entity($item);
      array_push($social_links, array(
        'title' => '',
        'href' => $fc->field_v_blog_social_url[LANGUAGE_NONE][0]['safe_value'],
        'attributes' => array('class' => array($fc->field_v_blog_social_css[LANGUAGE_NONE][0]['safe_value'])),
      ));
    }
  }

  // Theme author social links.
  if (count($social_links) > 0) {
    $node->content['author_social'] = array(
      '#theme' => 'links',
      '#links' => $social_links,
      "attributes" => array('class' => array('author_social_media')),
    );
  }

  // Format node created date.
  $date_format = format_date($node->created, 'blog_entry', $langcode);
  $date_format = str_replace('-', t('at'), $date_format);
  $node->content['date_published'] = array(
    '#markup' => $date_format,
  );

}

/**
 * Implements template_preprocess_node().
 */
function vactory_blog_preprocess_node(&$variables) {
  // Remove user profile from node tpl.
  if (isset($variables['type']) && $variables['type'] === 'vactory_blog_entry') {
    if (isset($variables['user_picture'])) {
      unset($variables['user_picture']);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function vactory_blog_form_vactory_blog_entry_node_form_alter(&$form, &$form_state) {
  // Hide background processed fields.
  $form['field_v_blog_weight']['#access'] = FALSE;
  $form['field_v_blog_view_count']['#access'] = FALSE;
}

/**
 * Implements hook_node_insert().
 */
function vactory_blog_node_insert($node) {
  // Make this node appear as first in draggable views.
  if ("vactory_blog_entry" === $node->type) {
    $node->field_v_blog_weight[LANGUAGE_NONE][0]['value'] = -1 * ($node->nid + 1);
    field_attach_update('node', $node);
  }
}

/**
 * Implements of hook_element_info().
 */
function vactory_blog_element_info() {
  //dsm(captcha_element_info());
  //return captcha_element_info();
}

/**
 * Implements hook_form_alter().
 */
function vactory_blog_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'comment_node_vactory_blog_entry_form':
      // Hide unnecessary fields.
      $form['author']['homepage']['#access'] = FALSE;
      $form['field_v_blog_comment_source']['#access'] = FALSE;

      // Override better comments Javascript.
      foreach ($form['#attached']['js'] as $key => $js_path) {
        if ("sites/all/modules/contrib/better_comments/js/better_comments.js" === $js_path) {
          $form['#attached']['js'][$key] = drupal_get_path('module', 'vactory_blog') . '/js/vactory_blog_comments.js';
          break;
        }
      }

     /* if (isset($_COOKIE['facebook_user'])) {
        unset($form['captcha']);
      }*/
      $form['captcha']['#captcha_validate'] = '_recaptcha_captcha_validation';

      // Implement our own validation.
      $form['#validate'][] = 'vactory_blog_comment_form_validate';

      // Ajaxify comment submit button.
      $form['actions']['submit']['#ajax'] = array(
        'callback' => 'vactory_blog_comment_submit',
        'wrapper' => 'comment-wrapper',
        'method' => 'append',
        'effect' => 'fade',
      );

      // Get conment object.
      $comment = $form_state['comment'];

      if (user_is_anonymous()) {

        // Toggle facebook connexion.
        $form['facebook_user'] = array(
          '#title' => '',
          '#type' => 'checkbox',
          '#default_value' => isset($_COOKIE['facebook_user']) ? 1 : 0,
        );

        // Add custom author e-mail.
        // By default, this fields is not required
        // On the frontend, we use form states for HTML5 validation,
        // depending on whatever the user is connected via Facebook or not.
        //
        // Then we use vactory_blog_comment_form_validate to validate the email
        // if the user is not connected using Facebook then normal validation
        // happens, otherwise the comment is considered as valid even
        // without having the e-mail field filled.
        $form['author']['mail'] = array(
          '#type' => 'textfield',
          '#title' => t('E-mail'),
          '#default_value' => $comment->mail,
          '#states' => array(
            'required' => array(
              ':input[name*="facebook_user"]' => array('checked' => FALSE),
            ),
          ),
          '#required' => FALSE,
          '#maxlength' => 64,
          '#size' => 30,
          //'#description' => t('The content of this field is kept private and will not be shown publicly.'),
          '#access' => TRUE,
        );
      }
      break;
  }
}

function _recaptcha_captcha_validation() {
  return TRUE;
}

/**
 * Implements hook_comment_presave().
 */
function vactory_blog_comment_presave($comment) {
  // Set source.
  if (isset($comment->facebook_user) && !empty($comment->facebook_user)) {
    $comment->field_v_blog_comment_source[LANGUAGE_NONE][0]['value'] = 'facebook';
  }
  else {
    $comment->field_v_blog_comment_source[LANGUAGE_NONE][0]['value'] = 'form';
  }
}

/**
 * Submit the comment.
 */
function vactory_blog_comment_submit(&$form, &$form_state) {

  //$form['captcha']['#captcha_validate'] = '_recaptcha_captcha_validation';
  if ($errors = form_get_errors()) {
    $form['#attributes'] = array('id' => array('edit-comment-body', 'error'));
    $form['#attributes']['class'][] = 'comment-form';
    // Check if this a replay form.
    if (isset($form_state['values']['pid'])) {
      $pid = $form_state['values']['pid'];
      $form['actions']['cancel'] = array(
        '#markup' => l(t('Cancel'), 'better_comments/reply/' . $pid . '/cancel',
          array('attributes' => array('class' => array('use-ajax', 'button')))),
        '#weight' => 21,
      );
      $form = drupal_render($form);
      $commands[] = ajax_command_replace('#comment-wrap-' . $pid . ' form', $form);

      // Captcha error.
      if ($errors['captcha_response'] && !empty($errors['captcha_response'])) {
        $commands[] = ajax_command_invoke('#comment-wrap-' . $pid . ' fieldset.captcha', 'addClass', array('has-error'));
      }
    }
    else {
      $form = drupal_render($form);
      $commands[] = ajax_command_replace('.comment-form', $form);
      // Captcha error.
      if ($errors['captcha_response'] && !empty($errors['captcha_response'])) {
        $commands[] = ajax_command_invoke('.comment-form fieldset.captcha', 'addClass', array('has-error'));
      }
    }

    $commands[] = ajax_command_remove('div.messages');
    $commands[] = ajax_command_replace('div#ajax-status-messages-wrapper', '<div id="ajax-status-messages-wrapper">' . theme('status_messages') . '</div>');

    return array('#type' => 'ajax', '#commands' => $commands);
  }

  $form_state['redirect'] = '';
  // Unset the preview mode for form submit.
  unset($form_state['comment']->in_preview);
  unset($form_state['comment']->preview);
  $comment = $form_state['comment'];
  $node = $form['#node'];
  $comment_build = comment_view($comment, $node);
  $comment_output = drupal_render($comment_build);
  // Existing comment is edited.
  if (isset($form['cid']['#value'])) {
    $commands[] = ajax_command_remove('.better-comments-confirm-' . $comment->cid);
    $commands[] = ajax_command_replace('.comment-inner-' . $comment->cid, $comment_output);
  }
  elseif (isset($form_state['values']['pid'])) {
    // Else this is a reply.
    // Append comment to parent wrapper.
    $comment_output = '</div>' . $comment_output;
    $commands[] = ajax_command_replace('.indented-' . $comment->pid, $comment_output);
    // Delete the form.
    $commands[] = ajax_command_invoke('#' . $form['#id'], 'remove');
  }
  // A new comment is created.
  else {
    $commands[] = ajax_command_append('.comment-section', $comment_output);
  }

  $commands[] = ajax_command_remove('div.messages');
  $commands[] = ajax_command_replace('div#ajax-status-messages-wrapper', '<div id="ajax-status-messages-wrapper">' . theme('status_messages') . '</div>');

  // Add a new empty form and not the cached form with comment body text.
  $form_state = array();
  $form_state['input'] = array();
  $form_state['build_info']['args'][] = (object) array('nid' => $node->nid);
  $form_build = drupal_build_form($form['#form_id'], $form_state);

/*  if ($facebook_user == 1) {
    unset($form_build['captcha']);
  }*/
  $form = drupal_render($form_build);
  $commands[] = ajax_command_replace('.comment-form', $form);
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Validate the comment on submit.
 */
function vactory_blog_comment_form_validate($form, &$form_state) {
  if (isset($form_state['values']['facebook_user']) && $form_state['values']['facebook_user'] !== 1 && user_is_anonymous()) {
    if (empty($form_state['values']['mail'])) {
      form_set_error('mail', t('The e-mail address field is required.'));
    }

    if ($form_state['values']['mail'] && !valid_email_address($form_state['values']['mail'])) {
      form_set_error('mail', t('The e-mail address you specified is not valid.'));
    }
  }
  else {
    $form_state['values']['mail'] = '';
  }
}

/**
 * Implements hook_preprocess_comment().
 */
function vactory_blog_preprocess_comment(&$variables, $hook) {
  global $language;

  if ($variables['node']->type === 'vactory_blog_entry') {
    $comment = $variables['elements']['#comment'];
    $account = user_load($comment->uid);
    $variables['theme_hook_suggestions'][] = 'vactory_blog_better_comments';
    $variables['picture_set'] = variable_get('better_comments_picture', 0);
    $variables['user_picture'] = theme('user_picture', array('account' => $account));
    // Format node created date.
    $date_format = format_date($variables['elements']['#comment']->created, 'blog_entry', $language->language);
    $date_format = str_replace('-', t('at'), $date_format);

    $variables['date_published'] = array(
      '#markup' => $date_format,
    );

    $variables['time_ago'] = format_date($variables['elements']['#comment']->created, 'custom', 'c');
    $query = db_query('SELECT COUNT(*) FROM {comment} WHERE pid = :cid', array(':cid' => $comment->cid)) ;
    $result = $query->fetchField();

    if ($result > 0) {
      $result = '+' . $result;
      $variables['content']['links']['comment']['#links']['comment-reply-count'] = $variables['content']['links']['comment']['#links']['comment-reply'];
      $variables['content']['links']['comment']['#links']['comment-reply-count']['href'] = '#';
      $variables['content']['links']['comment']['#links']['comment-reply-count']['attributes']['class'] = array('comments-reply-count');
      $variables['content']['links']['comment']['#links']['comment-reply-count']['attributes']['id'] = array(str_replace('reply', 'reply-count', $variables['content']['links']['comment']['#links']['comment-reply-count']['attributes']['id'][0]));
      $variables['content']['links']['comment']['#links']['comment-reply-count']['title'] = '<span class="reply-count">'.$result.'</span> <i class="fa fa-comment" aria-hidden="true"></i>';
      $variables['content']['links']['comment']['#links'] = array_reverse($variables['content']['links']['comment']['#links']);
    }
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function vactory_blog_theme_registry_alter(&$theme_registry) {
  $theme_registry['username']['preprocess functions'][] = '_vactory_blog_username';
}

function _vactory_blog_username(&$variables) {
  $variables['extra'] = '';
}

/**
 * Implement preprocess comment wrapper.
 */
function vactory_blog_preprocess_comment_wrapper(&$variables, $hook) {
  // Force our .tpl & pass in arguments.
  if ($variables['node']->type === 'vactory_blog_entry') {
    $variables['theme_hook_suggestions'][] = 'vactory_blog_wrapper';
    $variables['comment_count'] = $variables['node']->comment_count;
  }
}

/**
 * Implements hook_comments_comment_view().
 */
function vactory_blog_comment_view($comment, $view_mode, $langcode) {
  //$comment->content['links']['comment']['#links']['comment-reply'] = array();
}

/**
 * Implements hook_comment_view_alter().
 */
function vactory_blog_comment_view_alter(&$build, $view_mode) {
  $comment = $build['#comment'];
  if (!isset($comment->depth)) {
    $comment->depth = 0;
  }
  $node = $build['#node'];
  $prefix = '';
  // Add wrapper tag.
  $indent = $comment->pid != 0 && variable_get('comment_default_mode_' . $node->type, COMMENT_MODE_THREADED) == COMMENT_MODE_THREADED;
  $prefix .= '<div class="better-comment-wrapper depth-' . $comment->depth . '' . ($indent == TRUE ? ' indented parent-' . $comment->pid : '') . '" id="comment-wrap-' . $comment->cid . '">';
  // Add anchor tag.
  $prefix .= "<a id=\"comment-$comment->cid\"></a>\n";
  $build['#prefix'] = $prefix;
}

/**
 * Implements hook_views_pre_view().
 */
function vactory_blog_views_pre_view(&$view, &$display_id, &$args) {
  $default_value = variable_get(_vactory_blog_get_variable_name(), array());

  if (!($view->name == 'v_blog_view' && $display_id == 'page')) {
    return;
  }

  // Add header & footer regions.
  if (isset($default_value['vactory_blog'])) {
    foreach ($default_value['vactory_blog'] as $region => $data) {
      if (!empty($data['value'])) {
        $view->display_handler->set_option($region, array(
          'text' => array(
            'id' => 'area',
            'table' => 'views',
            'field' => 'area',
            'empty' => FALSE,
            'content' => check_markup($data['value'], 'full_html'),
            'format' => 'full_html',
            'tokenize' => 0,
          ),
        ));
      }
    }
  }
}

/**
 * Implements hook_views_pre_render().
 */
function vactory_blog_views_pre_render(&$view) {
  if ($view->name == 'v_blog_view' && $view->current_display == 'page') {
    $default_value = variable_get(_vactory_blog_get_variable_name(), array());

    // Update page & views title.
    if (isset($default_value['vactory_blog'])) {
      if (empty($default_value['vactory_blog']['title'])) {
        $default_value['vactory_blog']['title'] = 'Blog';
      }
      drupal_set_title($default_value['vactory_blog']['title']);
      $view->build_info['title'] = $default_value['vactory_blog']['title'];
    }

    // Add view count to rows.
    if (function_exists('nodeviewcount_db_count_node_views')) {
      foreach ($view->result as &$row) {
        $count = nodeviewcount_db_count_node_views($row);
        if (isset($count[0]) && isset($count[0]->expression)) {
          $row->field_field_v_blog_view_count[0]['rendered'] = $count[0]->expression;
          $row->nothing[0]['rendered'] = $count[0]->expression;
        }
      }
    }

  }
}

/**
 * Implements hook_views_pre_execute().
 */
function vactory_blog_views_pre_execute(&$view) {
  //dpq($view->build_info['query']);
}

/**
 * Implements hook_views_query_alter().
 */
function vactory_blog_views_query_alter(&$view, &$query) {
  if ($view->name == 'v_blog_view' && $view->current_display == 'page') {

    // Sort by draggable views weight by default.
    if (!isset($_GET['sort_bef_combine'])) {
      // Post date is replaced with weight field.
      $view->query->orderby[0] = $view->query->orderby[1];
      // Weight field is then removed.
      unset($view->query->orderby[1]);
    }
    else {
      // if the first option is selected then default behavior, we sort by weight.
      if ($_GET['sort_bef_combine'] == ' DESC') {
        // Post date is replaced with weight field.
        $view->query->orderby[0] = $view->query->orderby[1];
        // Weight field is then removed.
        unset($view->query->orderby[1]);
      }
      else {
        // Remove weight field sort and keep only created asc/desc or node_views.
        unset($view->query->orderby[1]);
      }
    }

    // Join views query with nodeviewcount table and allow sorting by count value.
    if (isset($_GET['sort_bef_combine']) && $_GET['sort_bef_combine'] == 'node_views DESC') {
      // Create the join.
      $join = new views_join();
      $join->table = 'nodeviewcount';
      $join->field = 'nid';
      $join->left_table = 'node';
      $join->left_field = 'nid';
      $join->type = 'left';
      // Add the join the the view query.
      $view->query->add_relationship('nodeviewcount',$join,'node');
      // Add field
      $query->add_field(null, "nodeviewcount.nid", 'vnodeviews', array('function' => 'count'));
      // Remove default order.
      unset($view->query->orderby[1]);
      // Add custom order by node views count.
      $view->query->orderby[0]['field'] = 'vnodeviews';
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function vactory_blog_form_views_exposed_form_alter(&$form, &$form_state) {
  if ($form_state['view']->name == 'v_blog_view' && $form_state['view']->current_display == 'page') {
    // Alter month to filter date on month OR year.
    $form['month']['value']['#date_format'] = 'm';
    $form['#validate'][] = 'vactory_blog_views_exposed_form_validate';

    // Add a new options to sort filter.
    $old_options = $form['sort_bef_combine']['#options'];
    // Reset options.
    $form['sort_bef_combine']['#options'] = array();
    // Add Sort -- Any option
    $form['sort_bef_combine']['#options'][' DESC'] = t('Sort');
    // Copy old options back.
    foreach ($old_options as $key => $v) {
      $form['sort_bef_combine']['#options'][$key] = $v;
    }
    // Add node views count custom sort option.
    $form['sort_bef_combine']['#options']['node_views DESC'] = t('Views count');
    // Set default selected option.
    $form['sort_bef_combine']['#default_value'] = ' DESC';
    //$form['sort_bef_combine']['#validated'] = TRUE;
  }
}

/**
 * Custom validate month filter.
 */
function vactory_blog_views_exposed_form_validate(&$form, &$form_state) {
  // Remove year format Y-m and keep m format only.
  if ("views-exposed-form-v-blog-view-page" == $form['#id']) {
    $form_state['view']->filter['date_filter']->options['granularity'] = 'month';
    $form_state['view']->filter['date_filter']->format = 'm';
  }
}

/**
 * Implements hook_date_process_element_alter().
 */
function vactory_blog_date_select_process_alter(&$element, &$form_state, $context) {
  // Replace -Year and -Month options label.
  if (isset($form_state['view']) && $form_state['view']->name == 'v_blog_view' && $form_state['view']->current_display == 'page') {
    if (isset($element['year']) && !empty($element['year']) && isset($element['year']['#options'])) {
      $element['year']['#options'][''] = t('Filter by year');
    }

    if (isset($element['month']) && !empty($element['month']) && isset($element['month']['#options'])) {
      $element['month']['#options'][''] = t('Filter by month');
    }
  }
}
