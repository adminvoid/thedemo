<?php

/**
 * @file
 * Default setting for Field Cover.
 */

/**
 * Base settings of Field Cover.
 *
 * @return array
 *    Base Settings.
 */
function vactory_common_field_cover_base_settings() {
  return array(
    'field_name' => VACTORY_COMMON_FIELD_COVER,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'type' => 'image',
  );
}

/**
 * Instance settings of Field Cover.
 *
 * @param string $bundle
 *    Content type machine name.
 * @param int $weight
 *    Weight value in fields list.
 *
 * @return array
 *    Instance Settings.
 */
function vactory_common_field_cover_instance_settings($bundle, $widget, $weight = 1) {
  if (module_exists('field_group')) {
    $form_groups = field_group_info_groups('node', $bundle, 'form');
    if (count($form_groups) > 0) {
      $weight = 0;
    }
  }
  $field_instance_settings = array(
    'bundle' => $bundle,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'fences_wrapper' => 'div_div_div',
    'field_name' => VACTORY_COMMON_FIELD_COVER,
    'label' => t('Cover Image'),
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'vactory_cover',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'media_library_include_in_library' => 0,
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
  );
  if ($widget == 'media') {
    $field_instance_settings['widget'] = array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 'public',
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_2' => 'media_default--media_browser_2',
          'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          'media_internet' => 0,
          'upload' => 'upload',
        ),
      ),
      'type' => 'media_generic',
      'weight' => $weight,
    );
  } else {
    $field_instance_settings['widget'] = array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => $weight,
    );
  }
  return $field_instance_settings;
}
