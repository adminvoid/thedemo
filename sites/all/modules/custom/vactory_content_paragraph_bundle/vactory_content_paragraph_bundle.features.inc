<?php
/**
 * @file
 * vactory_content_paragraph_bundle.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function vactory_content_paragraph_bundle_paragraphs_info() {
  $items = array(
    'paragraphs_content' => array(
      'name' => 'Paragraph Content',
      'bundle' => 'paragraphs_content',
      'locked' => '1',
    ),
  );
  return $items;
}
