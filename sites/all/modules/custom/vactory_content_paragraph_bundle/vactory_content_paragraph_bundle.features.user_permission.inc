<?php
/**
 * @file
 * vactory_content_paragraph_bundle.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vactory_content_paragraph_bundle_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create paragraph content paragraphs_content'.
  $permissions['create paragraph content paragraphs_content'] = array(
    'name' => 'create paragraph content paragraphs_content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'delete paragraph content paragraphs_content'.
  $permissions['delete paragraph content paragraphs_content'] = array(
    'name' => 'delete paragraph content paragraphs_content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'update paragraph content paragraphs_content'.
  $permissions['update paragraph content paragraphs_content'] = array(
    'name' => 'update paragraph content paragraphs_content',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content paragraphs_content'.
  $permissions['view paragraph content paragraphs_content'] = array(
    'name' => 'view paragraph content paragraphs_content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  return $permissions;
}
