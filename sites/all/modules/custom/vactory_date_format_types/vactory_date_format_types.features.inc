<?php
/**
 * @file
 * vactory_date_format_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_date_format_types_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_fe_date_custom_date_formats().
 */
function vactory_date_format_types_fe_date_custom_date_formats() {
  $custom_date_formats = array();
  return $custom_date_formats;
}

/**
 * Implements hook_date_format_types().
 */
function vactory_date_format_types_date_format_types() {
  $format_types = array();
  // Exported date format type: vactory_long
  $format_types['vactory_long'] = 'Vactory Long';
  // Exported date format type: vactory_medium
  $format_types['vactory_medium'] = 'Vactory Medium';
  // Exported date format type: vactory_mini
  $format_types['vactory_mini'] = 'Vactory Mini';
  // Exported date format type: vactory_short
  $format_types['vactory_short'] = 'Vactory Short';
  return $format_types;
}

/**
 * Implements hook_fe_date_locale_date_format().
 */
function vactory_date_format_types_fe_date_locale_date_format() {
  $locale_date_formats = array();

  // Exported format: vactory_long::ar
  $locale_date_formats['vactory_long::ar'] = array(
    'type' => 'vactory_long',
    'format' => 'l, j F, Y - H:i',
    'locales' => array(
      0 => 'ar',
    ),
  );
  // Exported format: vactory_long::eng
  $locale_date_formats['vactory_long::eng'] = array(
    'type' => 'vactory_long',
    'format' => 'l, F j, Y - H:i',
    'locales' => array(
      0 => 'eng',
    ),
  );
  // Exported format: vactory_long::fr
  $locale_date_formats['vactory_long::fr'] = array(
    'type' => 'vactory_long',
    'format' => 'l, j F, Y - H:i',
    'locales' => array(
      0 => 'fr',
    ),
  );
  // Exported format: vactory_medium::ar
  $locale_date_formats['vactory_medium::ar'] = array(
    'type' => 'vactory_medium',
    'format' => 'D, d/m/Y - g:ia',
    'locales' => array(
      0 => 'ar',
    ),
  );
  // Exported format: vactory_medium::eng
  $locale_date_formats['vactory_medium::eng'] = array(
    'type' => 'vactory_medium',
    'format' => 'D, m/d/Y - g:ia',
    'locales' => array(
      0 => 'eng',
    ),
  );
  // Exported format: vactory_medium::fr
  $locale_date_formats['vactory_medium::fr'] = array(
    'type' => 'vactory_medium',
    'format' => 'D, d/m/Y - g:ia',
    'locales' => array(
      0 => 'fr',
    ),
  );
  // Exported format: vactory_mini::ar
  $locale_date_formats['vactory_mini::ar'] = array(
    'type' => 'vactory_mini',
    'format' => 'd/m/Y - H:i',
    'locales' => array(
      0 => 'ar',
    ),
  );
  // Exported format: vactory_mini::eng
  $locale_date_formats['vactory_mini::eng'] = array(
    'type' => 'vactory_mini',
    'format' => 'm/d/Y - H:i',
    'locales' => array(
      0 => 'eng',
    ),
  );
  // Exported format: vactory_mini::fr
  $locale_date_formats['vactory_mini::fr'] = array(
    'type' => 'vactory_mini',
    'format' => 'd/m/Y - H:i',
    'locales' => array(
      0 => 'fr',
    ),
  );
  // Exported format: vactory_short::ar
  $locale_date_formats['vactory_short::ar'] = array(
    'type' => 'vactory_short',
    'format' => 'j M Y - H:i',
    'locales' => array(
      0 => 'ar',
    ),
  );
  // Exported format: vactory_short::eng
  $locale_date_formats['vactory_short::eng'] = array(
    'type' => 'vactory_short',
    'format' => 'M j Y - H:i',
    'locales' => array(
      0 => 'eng',
    ),
  );
  // Exported format: vactory_short::fr
  $locale_date_formats['vactory_short::fr'] = array(
    'type' => 'vactory_short',
    'format' => 'j M Y - H:i',
    'locales' => array(
      0 => 'fr',
    ),
  );
  return $locale_date_formats;
}
