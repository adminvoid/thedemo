<?php
/**
 * @file
 * vactory_date_format_types.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function vactory_date_format_types_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_vactory_long';
  $strongarm->value = 'l, F j, Y - H:i';
  $export['date_format_vactory_long'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_vactory_medium';
  $strongarm->value = 'D, m/d/Y - g:ia';
  $export['date_format_vactory_medium'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_vactory_mini';
  $strongarm->value = 'm/d/Y - H:i';
  $export['date_format_vactory_mini'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_vactory_short';
  $strongarm->value = 'M j Y - H:i';
  $export['date_format_vactory_short'] = $strongarm;

  return $export;
}
