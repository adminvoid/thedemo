<?php
/**
 * @file
 * vactory_field_bases.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function vactory_field_bases_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_date_fields|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_date_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vactory_fields';
  $field_group->data = array(
    'label' => 'Date fields',
    'weight' => '30',
    'children' => array(
      0 => 'field_vactory_date',
      1 => 'field_vactory_date_interval',
      2 => 'field_vactory_date_time',
      3 => 'field_vactory_year',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-vactory-date-fields field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vactory_date_fields|node|vactory_gen_type|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_fields|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Vactory fields',
    'weight' => '0',
    'children' => array(
      0 => 'group_vactory_long_text_fields',
      1 => 'group_vactory_text_fields',
      2 => 'group_vactory_media_fields',
      3 => 'group_vactory_date_fields',
      4 => 'group_vactory_other_fields',
      5 => 'group_vactory_links_field',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-vactory-fields field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_vactory_fields|node|vactory_gen_type|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_links_field|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_links_field';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vactory_fields';
  $field_group->data = array(
    'label' => 'Links field',
    'weight' => '31',
    'children' => array(
      0 => 'field_vactory_cta',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-vactory-links-field field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vactory_links_field|node|vactory_gen_type|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_long_text_fields|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_long_text_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vactory_fields';
  $field_group->data = array(
    'label' => 'Long text fields',
    'weight' => '28',
    'children' => array(
      0 => 'body',
      1 => 'field_vactory_description',
      2 => 'field_vactory_chapo',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-vactory-long-text-fields field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vactory_long_text_fields|node|vactory_gen_type|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_media_fields|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_media_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vactory_fields';
  $field_group->data = array(
    'label' => 'Media fields',
    'weight' => '29',
    'children' => array(
      0 => 'field_vactory_cover',
      1 => 'field_vactory_image',
      2 => 'field_vactory_image_multi',
      3 => 'field_vactory_thumbnail',
      4 => 'field_vactory_video',
      5 => 'field_vactory_documents',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-vactory-media-fields field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vactory_media_fields|node|vactory_gen_type|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_other_fields|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_other_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vactory_fields';
  $field_group->data = array(
    'label' => 'Other fields',
    'weight' => '32',
    'children' => array(
      0 => 'field_vactory_weight',
      1 => 'field_vactory_flag',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-vactory-other-fields field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vactory_other_fields|node|vactory_gen_type|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vactory_text_fields|node|vactory_gen_type|form';
  $field_group->group_name = 'group_vactory_text_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'vactory_gen_type';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vactory_fields';
  $field_group->data = array(
    'label' => 'Text fields',
    'weight' => '27',
    'children' => array(
      0 => 'field_vactory_email',
      1 => 'field_vactory_name',
      2 => 'field_vactory_telephone',
      3 => 'field_vactory_fax',
      4 => 'field_vactory_adress',
      5 => 'title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-vactory-text-fields field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_vactory_text_fields|node|vactory_gen_type|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Date fields');
  t('Links field');
  t('Long text fields');
  t('Media fields');
  t('Other fields');
  t('Text fields');
  t('Vactory fields');

  return $field_groups;
}
