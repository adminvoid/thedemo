<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<?php foreach ($blocks as $bid => $block): ?>
  <?php print $block['#markup']; ?>
<?php endforeach; ?>
