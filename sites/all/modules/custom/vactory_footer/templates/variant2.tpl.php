<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<footer role="footer">
  <div class="vf-footer-variant2">
    <div class="vf-footer">
      <?php if (isset($top) && !empty($top)): ?>
      <div class="vf-footer__top">
        <div class="vf-footer__layout">
          <?php print $top; ?>
        </div>
      </div>
      <?php endif; ?>
      <?php if (isset($middle) && !empty($middle) && isset($middle[2])): ?>
      <div class="vf-footer__middle">
        <div class="vf-footer__layout">
          <?php print $middle; ?>
        </div>
      </div>
      <?php endif; ?>
      <?php if (isset($bottom) && !empty($bottom)): ?>
      <div class="vf-footer__bottom">
        <div class="vf-footer__layout">
          <?php print $bottom; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>
  </div>
</footer>
