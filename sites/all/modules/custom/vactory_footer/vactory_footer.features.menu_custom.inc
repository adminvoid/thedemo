<?php
/**
 * @file
 * vactory_footer.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function vactory_footer_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-footer-mention-legales.
  $menus['menu-footer-mention-legales'] = array(
    'menu_name' => 'menu-footer-mention-legales',
    'title' => 'Footer Mention Legales',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-menu-footer.
  $menus['menu-menu-footer'] = array(
    'menu_name' => 'menu-menu-footer',
    'title' => 'Menu Footer',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer Mention Legales');
  t('Menu Footer');

  return $menus;
}
