<?php
/**
 * @file
 * vactory_footer.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function vactory_footer_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-menu-footer_footer-link-1:http://example.com.
  $menu_links['menu-menu-footer_footer-link-1:http://example.com'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'http://example.com',
    'router_path' => '',
    'link_title' => 'Footer link 1',
    'options' => array(
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-menu-footer_footer-link-1:http://example.com',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'fr',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-menu-footer_footer-link-2:http://example.com.
  $menu_links['menu-menu-footer_footer-link-2:http://example.com'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'http://example.com',
    'router_path' => '',
    'link_title' => 'Footer link 2',
    'options' => array(
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-menu-footer_footer-link-2:http://example.com',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'fr',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-menu-footer_footer-link-3:http://example.com.
  $menu_links['menu-menu-footer_footer-link-3:http://example.com'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'http://example.com',
    'router_path' => '',
    'link_title' => 'Footer link 3',
    'options' => array(
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-menu-footer_footer-link-3:http://example.com',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'fr',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-menu-footer_footer-link-4:http://example.com.
  $menu_links['menu-menu-footer_footer-link-4:http://example.com'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'http://example.com',
    'router_path' => '',
    'link_title' => 'Footer link 4',
    'options' => array(
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-menu-footer_footer-link-4:http://example.com',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'fr',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-menu-footer_footer-link-5:http://example.com.
  $menu_links['menu-menu-footer_footer-link-5:http://example.com'] = array(
    'menu_name' => 'menu-menu-footer',
    'link_path' => 'http://example.com',
    'router_path' => '',
    'link_title' => 'Footer link 5',
    'options' => array(
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-menu-footer_footer-link-5:http://example.com',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'fr',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Footer link 1');
  t('Footer link 2');
  t('Footer link 3');
  t('Footer link 4');
  t('Footer link 5');

  return $menu_links;
}
