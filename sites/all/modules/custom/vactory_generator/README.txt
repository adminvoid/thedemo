

Vactory Generator 1.x for Drupal 7.x
------------------------------------
Vactory Generator module allows you to generate different types of views listing to any of your content types.


Installation
------------
Vactory Generator can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it.

The Vactory Generator provides an administration settings page on : admin/config/vactory/vactory_generator


HOW TO USE THIS MODULE (French)
-------------------------------
1 - Création du type de contenu
    (Supposant que le type de contenu créé est nommé «content_type_name »)

2 - Création des champs :

	2.1 - Champs basiques : en utilisant les champs fournit par le module « vactory_field_bases »  (add existing field)
	2.2 - Champs spécifiques : (add new field)

3 - Génération des vues (pages listing et block)

	3.1 - Aller à la page d’administration du module vactory_generator « admin/config/vactory/vactory_generator »

	3.2 - Aller sur l’onglet correspondant au type de contenu News et cocher la case « Enable Listing for this content type »

	3.3 - Le module offre la possibilité de générer des differents types de display

		- Les display disponible sont :

            * Page listing 1 colonne
            * Page listing 2 colonnes
            * Page listing 3 colonnes
            * Block 3s
            * Block 1b-3s
            * Block 1b-2s
            * Block slider
            * Block masonry

		- Pour activer un display, cocher la case « Enable Display ».

		- Pour chaque display activé, le module permettra de configurer :

            * Le titre
            * Le path pour les displays de type page.
            * Le path vers la page listing pour les displays de type block (See more path)
            * les champs choisi pour les filtre
            * les champs choisi pour le sort (en cas de choix d’un champs de poids, une vue Draggable views sera aussi généré)
            * Le type de pagination souhaités et/ou le nombre d’éléments à afficher
            * Le reste est configuré dynamiquement.

        - Si un champ de type « number » a été choisi comme filtre un display DraggableViews sera généré également.

	3.4 - Après enregistrement :

		- La vue générée sera nommé « Vactory {content_type_name} » et le nom machine est « vactory_gen_{content_type_name} »

		- Pour chaque display activé, le view mode correspondant sera activé pour le type de contenu concerné.

4 - Configuration des view modes :

	4.1 - Aller sur la page de gestion d’affichage du type de contenu « admin/structure/types/manage/content_type_name/display »

	4.2 - Ensuite configurer l’affichage des champs pour chaque view mode.

	4.3 - Et pour avoir plus de flexibilité dans l’affichage, il est recommandé de définir les templates (.tpl) dans la feature/module crée.

5 - Génération de la feature

6 - Ajout des fichiers templates ( hook_theme_registry_alter() et  hook_preprocess_node() )

7 - Bonnes pratiques :

	   - Chargement des bibliothèque js et du css, par context.

	   - Le fichier « vactory_generator.examples.inc », contient des exemple de bonnes pratiques

8 - Application du js dans le cas des displays masonry et slider



Maintainers
-----------

- Taoufiq El hattami
