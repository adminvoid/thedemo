<?php

/**
 * @file
 * Admin configuration file of vactory_generator module.
 */

/**
 * Inits vactory_generator_admin form.
 */
function vactory_generator_admin_settings($form, $form_state) {
  // @todo Use multiselect module
  $node_types = node_type_get_types();
  $form['vactory_generator']['types_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Listings views configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['vactory_generator']['types_config']['vactory_generator_node_types_list'] = array('#type' => 'vertical_tabs');
  $fieldset = array();
  foreach ($node_types as $type) {
    $fieldset[$type->type] = array(
      '#type' => 'fieldset',
      '#title' => $type->name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'vactory_generator_node_types_list',
    );

    $fieldset[$type->type]["vactory_generator_enable_type_{$type->type}"] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Listing for this content type'),
      '#description' => t('Check this to enable Listing for this content type'),
      '#default_value' => variable_get("vactory_generator_enable_type_{$type->type}", FALSE),
    );

    $fieldset[$type->type]['displays_config']["vactory_generator_displays_list_{$type->type}"] = array('#type' => 'vertical_tabs');
    $view = views_get_view('vactory_generator_view');

    $tabs = array();
    foreach ($view->display as $display) {
      $fields_options = _vactory_generator_get_fields($type, $display->display_plugin);
      if (!in_array($display->id, array('default', 'draggable_sort_view'))) {
        $tabs[$display->id] = array(
          '#type' => 'fieldset',
          '#title' => $display->display_title,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#group' => "vactory_generator_displays_list_{$type->type}",
        );

        $tabs[$display->id]["vactory_generator_enable_display_{$type->type}_{$display->id}"] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable Display'),
          '#description' => t('Check this to enable this display'),
          '#default_value' => variable_get("vactory_generator_enable_display_{$type->type}_{$display->id}", FALSE),
        );

        $tabs[$display->id]["vactory_generator_display_title_{$type->type}_{$display->id}"] = array(
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#description' => t('Enter a title for the display'),
          '#default_value' => variable_get("vactory_generator_display_title_{$type->type}_{$display->id}", ''),
        );

        if ($display->display_plugin == 'page') {
          $tabs[$display->id]["vactory_generator_display_path_{$type->type}_{$display->id}"] = array(
            '#type' => 'textfield',
            '#title' => t('Path'),
            '#description' => t('Enter a path for the display'),
            '#default_value' => variable_get("vactory_generator_display_path_{$type->type}_{$display->id}", ''),
          );
        }
        elseif ($display->display_plugin == 'block') {
          $tabs[$display->id]["vactory_generator_display_path_{$type->type}_{$display->id}"] = array(
            '#type' => 'textfield',
            '#title' => t('See more path'),
            '#description' => t('Enter a path to see more'),
            '#default_value' => variable_get("vactory_generator_display_path_{$type->type}_{$display->id}", ''),
          );
        }

        $tabs[$display->id]["vactory_generator_enabled_filters_{$type->type}_{$display->id}"] = array(
          '#type' => 'checkboxes',
          '#title' => t('Fields to use for filters'),
          '#description' => t('Select fields to use for filters'),
          '#options' => $fields_options['filters'],
          '#default_value' => variable_get("vactory_generator_enabled_filters_{$type->type}_{$display->id}", array()),
          // "#empty_option" => t('- Select -'),
          // '#multiple' => TRUE,
          // '#size' => 6,.
        );
        if ($display->display_plugin == 'page') {
          $tabs[$display->id]["vactory_generator_use_datepicker_{$type->type}_{$display->id}"] = array(
            '#type' => 'checkbox',
            '#title' => t('Use jQuery UI DatePicker widget for date filters'),
            '#description' => t('Check to use jQuery UI DatePicker widget for date filters, otherwise separate select fields will be used'),
            '#default_value' => variable_get("vactory_generator_use_datepicker_{$type->type}_{$display->id}", FALSE),
          );
        }

        if ($display->display_plugin == 'block') {
          foreach ($fields_options['filters'] as $filter_key => $filter_value) {
            $field_info = field_info_field($filter_key);
            if (in_array($field_info['module'], array('taxonomy'))) {
              $type_id = drupal_html_class($type->type);
              $display_id = drupal_html_class($display->id);
              $filter_id = drupal_html_class($filter_key);
              $selector = ":input[id='edit-vactory-generator-enabled-filters-{$type_id}-{$display_id}-{$filter_id}']";
              $tabs[$display->id]["vactory_generator_enabled_filters_{$type->type}_{$display->id}_{$filter_key}"] = array(
                '#type' => 'select',
                '#multiple' => TRUE,
                '#size' => 4,
                '#title' => t("Filter value for :") . " " . $filter_value,
                '#options' => _vactory_generator_get_terms($filter_key),
                '#description' => t("Enter filter value for :") . " " . $filter_value,
                '#default_value' => variable_get("vactory_generator_enabled_filters_{$type->type}_{$display->id}_{$filter_key}", array()),
                '#states' => array(
                  'visible' => array(
                    array(
                      $selector => array('checked' => TRUE),
                    ),
                  ),
                ),
              );
            }
          }
        }

        $tabs[$display->id]["vactory_generator_enabled_sorts_{$type->type}_{$display->id}"] = array(
          '#type' => 'checkboxes',
          '#title' => t('Fields to use for sorts'),
          '#description' => t('Select fields to use for sorts'),
          '#options' => $fields_options['sorts'],
          '#default_value' => variable_get("vactory_generator_enabled_sorts_{$type->type}_{$display->id}", array()),
          // "#empty_option" => t('- Select -'),
          // '#multiple' => TRUE,
          // '#size' => 6,.
        );
        if ($display->display_plugin == 'page') {
          $tabs[$display->id]["vactory_generator_enabled_pagination_{$type->type}_{$display->id}"] = array(
            '#type' => 'radios',
            '#title' => t('Pagination type'),
            '#description' => t('Choose the pagination type.'),
            '#options' => $fields_options['pagination'],
            '#default_value' => variable_get("vactory_generator_enabled_pagination_{$type->type}_{$display->id}", array()),
          );
          $tabs[$display->id]["vactory_generator_enabled_pagination_quantity_{$type->type}_{$display->id}"] = array(
            '#type' => 'textfield',
            '#width' => '30%',
            '#number_type' => 'decimal',
            '#element_validate' => array('element_validate_integer_positive'),
            '#title' => t('Number of items to display'),
            '#description' => t('Choose the number of items to display.'),
            '#default_value' => variable_get("vactory_generator_enabled_pagination_quantity_{$type->type}_{$display->id}", '9'),
            '#states' => array(
              'visible' => array(
                array(
                  ":input[name='vactory_generator_enabled_pagination_{$type->type}_{$display->id}']" => array('value' => 'full'),
                ),
                'xor',
                array(
                  ":input[name='vactory_generator_enabled_pagination_{$type->type}_{$display->id}']" => array('value' => 'some'),
                ),
                'xor',
                array(
                  ":input[name='vactory_generator_enabled_pagination_{$type->type}_{$display->id}']" => array('value' => 'mini'),
                ),
              ),
            ),
          );
        }
        elseif ($display->display_plugin == 'block' && !in_array($display->id, array('block_5s', 'block_3s', 'block_1b_3s', 'block_1b_2s'))) {
          $tabs[$display->id]["vactory_generator_enabled_pagination_quantity_{$type->type}_{$display->id}"] = array(
            '#type' => 'textfield',
            '#width' => '30%',
            '#number_type' => 'decimal',
            '#element_validate' => array('element_validate_integer_positive'),
            '#title' => t('Number of items to display'),
            '#description' => t('Choose the number of items to display.'),
            '#default_value' => variable_get("vactory_generator_enabled_pagination_quantity_{$type->type}_{$display->id}", '9'),
          );
        }
        $fieldset[$type->type]['displays_config'][$display->id] = $tabs[$display->id];
      }

    }

    $form['vactory_generator']['types_config'][$type->type] = $fieldset[$type->type];
  }
  $form['#submit'][] = 'vactory_generator_admin_settings_submit';
  return system_settings_form($form);

}

/**
 * Submit function for vactory_generatoradmin form.
 */
function vactory_generator_admin_settings_submit($form, $form_state) {
  $values = $form_state['values'];
  // Getting enabled types.
  $enabled_types = array();
  foreach ($values as $key => $value) {
    if (strpos($key, 'vactory_generator_enable_type_') === 0) {
      $enabled_types[explode('vactory_generator_enable_type_', $key)[1]] = $value;
    }
  }
  variable_set('vactory_generator_enabled_types', $enabled_types);
  // Clone view with content type specific settings.
  $conf = array();
  foreach ($enabled_types as $content_type_name => $content_type_enabled) {
    // Getting enabled displays per enabled type.
    $enabled_displays = array();
    foreach ($values as $key => $value) {
      if (strpos($key, "vactory_generator_enable_display_{$content_type_name}_") === 0) {
        $display_name = explode("vactory_generator_enable_display_{$content_type_name}_", $key)[1];
        $enabled_displays[$display_name]['enabled'] = $value;
        $enabled_displays[$display_name]['title'] = $values["vactory_generator_display_title_{$content_type_name}_{$display_name}"];
        if (isset($values["vactory_generator_display_path_{$content_type_name}_{$display_name}"])) {
          $enabled_displays[$display_name]['path'] = $values["vactory_generator_display_path_{$content_type_name}_{$display_name}"];
        }
        // Getting enabled filters per enabled display.
        $enabled_displays[$display_name]['filters'] = $values["vactory_generator_enabled_filters_{$content_type_name}_{$display_name}"];
        $enabled_displays[$display_name]['filters_values'] = array();
        foreach ($enabled_displays[$display_name]['filters'] as $filter_key => $filter_value) {
          if (isset($values["vactory_generator_enabled_filters_{$content_type_name}_{$display_name}_{$filter_key}"])) {
            $enabled_displays[$display_name]['filters_values'][$filter_key] = $values["vactory_generator_enabled_filters_{$content_type_name}_{$display_name}_{$filter_key}"];
          }
        }
        // Getting date filter widget type (DatePicker).
        if (isset($values["vactory_generator_use_datepicker_{$content_type_name}_{$display_name}"])) {
          $enabled_displays[$display_name]['filters']['#options']['use_datepicker'] = $values["vactory_generator_use_datepicker_{$content_type_name}_{$display_name}"];
        }
        // Getting enabled sorts per enabled display.
        $enabled_displays[$display_name]['sorts'] = $values["vactory_generator_enabled_sorts_{$content_type_name}_{$display_name}"];
        // Getting enabled pagination type.
        if (isset($values["vactory_generator_enabled_pagination_{$content_type_name}_{$display_name}"])) {
          $enabled_displays[$display_name]['pagination']['type'] = $values["vactory_generator_enabled_pagination_{$content_type_name}_{$display_name}"];

        }
        if (isset($values["vactory_generator_enabled_pagination_quantity_{$content_type_name}_{$display_name}"])) {
          $enabled_displays[$display_name]['pagination']['quantity'] = $values["vactory_generator_enabled_pagination_quantity_{$content_type_name}_{$display_name}"];
        }
      }
    }

    $conf[$content_type_name]['displays'] = $enabled_displays;
    $conf[$content_type_name]['enabled'] = $content_type_enabled;
    if ($content_type_enabled == 0) {
      // Delete view.
      _vactory_generator_delete_view($content_type_name, $conf[$content_type_name]);
    }
    elseif ($content_type_enabled = 1) {
      // Set view.
      _vactory_generator_set_view($content_type_name, $conf[$content_type_name]);
      // Set view modes.
      _vactory_generator_set_view_modes($content_type_name, $conf[$content_type_name]);
    }
  }
  views_invalidate_cache();
  // cache_clear_all('vactory_generator_view' . ':', 'cache_views_data', TRUE);.
}
