<?php

/**
 * @file
 * Utilities functions of vactory_generator module.
 */

/**
 * Delete generated view.
 */
function _vactory_generator_delete_view($content_type_name, $conf) {
  if ($existing_view = views_get_view('vactory_gen_' . $content_type_name)) {
    //$existing_view->delete();
  }
}

/**
 * Generate view.
 */
function _vactory_generator_set_view($content_type_name, $conf) {
  // View already exist : delete it.
  if ($existing_view = views_get_view('vactory_gen_' . $content_type_name)) {
    $existing_view->delete(FALSE);
    views_invalidate_cache();
  }

  // View does not exist : clone it from "vactory_generator_view".
  if ($view = views_get_view('vactory_generator_view')) {
    $view->clone_view();
    $view->vid = 'new';
    $view->disabled = FALSE;
    $view->name = 'vactory_gen_' . $content_type_name;
    $view->human_name = 'Vactory ' . $content_type_name . ' view';
    // Remove disabled displays.
    foreach ($conf['displays'] as $display => $value) {
      if ($value['enabled'] == 0 && isset($view->display[$display])) {
        unset($view->display[$display]);
      }
    }
    // Remove draggable views display.
    unset($view->display['draggable_sort_view']);
    // Set enabled displays.
    foreach ($view->display as $display => $display_value) {

      if (!in_array($display, array('default', 'draggable_sort_view')) && $conf['displays'][$display]['enabled'] == 1) {
        $view->set_display($display);
        $view->init_handlers();

        $filters = $view->display_handler->get_option('filters');
        $sorts = $view->display_handler->get_option('sorts');
        $pager = $view->display_handler->get_option('pager');
        $exposed_forms = $view->display_handler->get_option('exposed_form');

        // Set content type filter.
        _vactory_highlights_add_type_filter($content_type_name, $filters);
        // Set title.
        $view->display_handler->set_option('title', $conf['displays'][$display]['title']);
        // Set path .
        if (isset($conf['displays'][$display]['path'])) {
          $view->display_handler->set_option('path', $conf['displays'][$display]['path']);
        }
        // Set css class.
        $css_class = $view->display_handler->get_option('css_class');
        $new_css_class = drupal_html_class("{$display}-{$content_type_name}");
        $view->display_handler->set_option('css_class', $css_class . " " . $new_css_class);

        // Set filters.
        foreach ($conf['displays'][$display]['filters'] as $filter_key => $filter_value) {
          if ($filter_value === $filter_key) {
            $filter_options = $conf['displays'][$display]['filters']['#options'];
            $filter_value = isset($conf['displays'][$display]['filters_values'][$filter_key]) ? $conf['displays'][$display]['filters_values'][$filter_key] : NULL;
            _vactory_highlights_add_filter($content_type_name, $filter_key, $filters, $exposed_forms, $display_value->display_plugin, $filter_value, $filter_options);
          }
        }
        // Set sorts.
        foreach ($conf['displays'][$display]['sorts'] as $sort_key => $sort_value) {
          if ($sort_key === $sort_value && $sort_key != 'draggable_views') {
            _vactory_highlights_add_sort($content_type_name, $sort_key, $sorts, $exposed_forms, $view->name, $display_value->display_plugin);
            // If field is a number then create a draggable view for it.
            $field_info = field_info_field($sort_key);
            if (in_array($field_info['module'], array('number'))) {
              _vactory_highlights_add_draggable_view($content_type_name, $view, $display, $conf, $field_info);
            }
          }
        }
        // Set pagination.
        if ($display_value->display_plugin == 'page' || !in_array($display, array('block_5s', 'block_3s', 'block_1b_3s', 'block_1b_2s'))) {
          $pager_type = isset($conf['displays'][$display]['pagination']['type']) ? $conf['displays'][$display]['pagination']['type'] : 'some';
          $pager_quantity = $conf['displays'][$display]['pagination']['quantity'];
          _vactory_highlights_set_pager($pager, $pager_type, $pager_quantity);
        }
        // Set more link.
        if ($display_value->display_plugin == 'block') {
          if (isset($conf['displays'][$display]['path'])) {
            $view->display_handler->override_option('use_more', TRUE);
            $view->display_handler->override_option('use_more_always', TRUE);
            $view->display_handler->override_option('link_display', 'custom_url');
            $view->display_handler->override_option('link_url', $conf['displays'][$display]['path']);
            $view->display_handler->override_option('use_more_text', t("See more {$conf['displays'][$display]['title']}"));
          }
        }

        $view->display_handler->override_option('filters', $filters);
        $view->display_handler->override_option('sorts', $sorts);
        $view->display_handler->override_option('exposed_form', $exposed_forms);
        $view->display_handler->override_option('pager', $pager);

      }
    }
    $view->save();
  }
}

/**
 * Set view display pager.
 */
function _vactory_highlights_set_pager(&$pager, $pager_type, $pager_quantity) {
  if ($pager_type == 'none') {
    $pager['type'] = $pager_type;
    $pager['options']['options']['offset'] = '0';
  }
  elseif ($pager_type == 'some') {
    $pager['type'] = $pager_type;
    $pager['options']['items_per_page'] = $pager_quantity;
    $pager['options']['offset'] = '0';
  }
  elseif ($pager_type == 'full') {
    $pager['type'] = $pager_type;
    $pager['options']['items_per_page'] = $pager_quantity;
    $pager['options']['offset'] = '0';
    $pager['options']['id'] = '0';
    $pager['options']['quantity'] = '9';
  }
  elseif ($pager_type == 'mini') {
    $pager['type'] = $pager_type;
    $pager['options']['items_per_page'] = $pager_quantity;
    $pager['options']['offset'] = '0';
    $pager['options']['id'] = '0';
  }

}

/**
 * Generate draggable_view display.
 */
function _vactory_highlights_add_draggable_view($content_type_name, &$view, $display, $conf, $field_info) {
  $field = $field_info['field_name'];

  $handler = $view->new_display('page', "{$display}_draggable_sort", "{$display}_draggable_sort");
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = $conf['displays'][$display]['title'] . ' : ' . 'Draggable Sort';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'row';
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Draggableviews: Content */
  $handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['table'] = 'node';
  $handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['label'] = '';
  $handler->display->display_options['fields']['draggableviews']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['handler'] = 'draggableviews_handler_fieldapi';
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['draggableviews_handler_fieldapi'] = array(
    'field' => "field_data_{$field}:{$field}_value",
  );
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Weight (field_article_weight) */
  $handler->display->display_options['sorts']["{$field}_value"]['id'] = "{$field}_value";
  $handler->display->display_options['sorts']["{$field}_value"]['table'] = "field_data_{$field}";
  $handler->display->display_options['sorts']["{$field}_value"]['field'] = "{$field}_value";
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
  );
  $handler->display->display_options['filters']['language']['group'] = 1;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    $content_type_name => $content_type_name,
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  if (isset($conf['displays'][$display]['path'])) {
    $handler->display->display_options['path'] = 'admin/' . $conf['displays'][$display]['path'] . '/sort';
  }
  else {
    $handler->display->display_options['path'] = 'admin/' . $view->display[$display]->id . '/' . $content_type_name . '/sort';
  }

  // $view->set_display($display);
  // $view->init_handlers();
}

/**
 * Set type filter configuration.
 */
function _vactory_highlights_add_type_filter($content_type_name, &$filters) {
  $filters['type']['id'] = 'type';
  $filters['type']['table'] = 'node';
  $filters['type']['field'] = 'type';
  $filters['type']['value'] = array(
    $content_type_name => $content_type_name,
  );
  $filters['type']['group'] = 1;
}

/**
 * Set sort configuration.
 */
function _vactory_highlights_add_sort($content_type_name, $field, &$sorts, &$exposed_forms, $view_name, $display_plugin) {
  $field_info = field_info_field($field);
  $field_info_instance = field_info_instance('node', $field, $content_type_name);

  if ($field == 'created') {
    $sorts["{$field}"]['id'] = "{$field}";
    $sorts["{$field}"]['table'] = 'node';
    $sorts["{$field}"]['field'] = "{$field}";
    $sorts["{$field}"]['order'] = 'DESC';
    if ($display_plugin == 'page') {
      $sorts["{$field}"]['exposed'] = TRUE;
      $sorts["{$field}"]['expose']['label'] = 'Date';
    }
  }
  elseif (in_array($field_info['module'], array('date'))) {
    $sorts["{$field}_value"]['id'] = "{$field}_value";
    $sorts["{$field}_value"]['table'] = "field_data_{$field}";
    $sorts["{$field}_value"]['field'] = "{$field}_value";
    $sorts["{$field}_value"]['order'] = 'DESC';
    if ($display_plugin == 'page') {
      $sorts["{$field}_value"]['exposed'] = TRUE;
      $sorts["{$field}_value"]['expose']['label'] = $field_info_instance['label'];
    }
  }
  elseif (in_array($field_info['module'], array('number'))) {
    /* Sort criterion: Content: Weight (field_article_weight) */
    $sorts["{$field}_value"]['id'] = "{$field}_value";
    $sorts["{$field}_value"]['table'] = "field_data_{$field}";
    $sorts["{$field}_value"]['field'] = "{$field}_value";
    $sorts["{$field}_value"]['order'] = 'ASC';
    if ($display_plugin == 'page') {
      // Todo : to confirm if sort by weight should be exposed or not !
    }
  }
  // Set BEF setting.
  if ($display_plugin == 'page') {
    $exposed_forms['options']['bef']['sort'] = array(
      'bef_format' => 'default',
      'advanced' => array(
        'collapsible' => 0,
        'collapsible_label' => 'Sort options',
        'combine' => 1,
        'combine_param' => 'sort_bef_combine',
        'reset' => 0,
        'reset_label' => '',
        'is_secondary' => 0,
        'autosubmit' => 0,
      ),
    );
  }
}

/**
 * Set filter configuration.
 */
function _vactory_highlights_add_filter($content_type_name, $field, &$filters, &$exposed_forms, $display_plugin, $filter_value, $filter_options) {
  $field_info = field_info_field($field);
  $field_info_instance = field_info_instance('node', $field, $content_type_name);

  if (in_array($field_info['module'], array('taxonomy'))) {

    /* Filter criterion: Content: Taxonomy ({$field}) */
    $filters["{$field}_tid"]['id'] = "{$field}_tid";
    $filters["{$field}_tid"]['table'] = "field_data_{$field}";
    $filters["{$field}_tid"]['field'] = "{$field}_tid";
    if ($display_plugin == 'page') {
      $filters["{$field}_tid"]['exposed'] = TRUE;
    }
    elseif ($display_plugin = 'block') {
      $filters["{$field}_tid"]['value'] = $filter_value;
      $filters["{$field}_tid"]['exposed'] = FALSE;
    }
    $filters["{$field}_tid"]['expose']['operator_id'] = "{$field}_tid_op";
    $filters["{$field}_tid"]['expose']['operator'] = "{$field}_tid_op";
    $filters["{$field}_tid"]['expose']['identifier'] = "{$field}_tid";
    $filters["{$field}_tid"]['expose']['remember_roles'] = array(
      2 => '2',
      1 => 0,
      3 => 0,
    );
    $filters["{$field}_tid"]['type'] = 'select';
    $filters["{$field}_tid"]['vocabulary'] = $field_info['settings']['allowed_values'][0]['vocabulary'];

    // Set BEF.
    $exposed_forms['options']['bef']["{$field}_tid"] = array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => t('Filter by') . " " . $field_info_instance['label'],
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    );
  }
  elseif ($field == 'created' || in_array($field_info['module'], array('date'))) {
    if (isset($filter_options['use_datepicker']) && $filter_options['use_datepicker'] == TRUE) {
      // DatePicker.
      $filters["date_filter_{$field}"]['id'] = "date_filter_{$field}";
      $filters["date_filter_{$field}"]['table'] = 'node';
      $filters["date_filter_{$field}"]['field'] = "date_filter";
      $filters["date_filter_{$field}"]['group'] = 1;
      $filters["date_filter_{$field}"]['exposed'] = TRUE;
      $filters["date_filter_{$field}"]['expose']['label'] = t('Filter by date');
      $filters["date_filter_{$field}"]['expose']['operator_id'] = "date_filter_{$field}_op";
      $filters["date_filter_{$field}"]['expose']['operator'] = "date_filter_{$field}_op";
      $filters["date_filter_{$field}"]['expose']['identifier'] = "date_filter_{$field}";
      $filters["date_filter_{$field}"]['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
      );
      $filters["date_filter_{$field}"]['granularity'] = 'month';
      $filters["date_filter_{$field}"]['form_type'] = 'date_popup';
      $filters["date_filter_{$field}"]['year_range'] = '-6:+0';
      if ($field == 'created') {
        $filters["date_filter_{$field}"]['date_fields'] = array(
          "node.{$field}" => "node.{$field}",
        );
      } elseif (in_array($field_info['module'], array('date'))) {
        $filters["date_filter_{$field}"]['date_fields'] = array(
          "field_data_{$field}.{$field}_value" => "field_data_{$field}.{$field}_value",
        );
      }

      // Set BEF.
      $exposed_forms['options']['bef']["date_filter_{$field}"] = array(
        'bef_format' => 'default',
        'more_options' => array(
          'is_secondary' => 0,
          'autosubmit' => 0,
          'is_secondary' => 0,
          'any_label' => t('Filter by date'),
          'bef_filter_description' => '',
          'tokens' => array(
            'available' => array(
              0 => 'global_types',
            ),
          ),
          'rewrite' => array(
            'filter_rewrite_values' => '',
          ),
          'datepicker_options' => '',
        ),
      );
    } else {
      // Month.
      $filters["date_filter_m_{$field}"]['id'] = "date_filter_m_{$field}";
      $filters["date_filter_m_{$field}"]['table'] = 'node';
      $filters["date_filter_m_{$field}"]['field'] = "date_filter";
      $filters["date_filter_m_{$field}"]['group'] = 1;
      $filters["date_filter_m_{$field}"]['exposed'] = TRUE;
      $filters["date_filter_m_{$field}"]['expose']['operator_id'] = "date_filter_m_{$field}_op";
      $filters["date_filter_m_{$field}"]['expose']['operator'] = "date_filter_m_{$field}_op";
      $filters["date_filter_m_{$field}"]['expose']['identifier'] = "date_filter_m_{$field}";
      $filters["date_filter_m_{$field}"]['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
      );
      $filters["date_filter_m_{$field}"]['granularity'] = 'month';
      $filters["date_filter_m_{$field}"]['year_range'] = '-6:+0';
      if ($field == 'created') {
        $filters["date_filter_m_{$field}"]['date_fields'] = array(
          "node.{$field}" => "node.{$field}",
        );
      } elseif (in_array($field_info['module'], array('date'))) {
        $filters["date_filter_m_{$field}"]['date_fields'] = array(
          "field_data_{$field}.{$field}_value" => "field_data_{$field}.{$field}_value",
        );
      }

      // Set BEF.
      $exposed_forms['options']['bef']["date_filter_m_{$field}"] = array(
        'bef_format' => 'default',
        'more_options' => array(
          'is_secondary' => 0,
          'autosubmit' => 0,
          'is_secondary' => 0,
          'any_label' => t('Filter by month'),
          'bef_filter_description' => '',
          'tokens' => array(
            'available' => array(
              0 => 'global_types',
            ),
          ),
          'rewrite' => array(
            'filter_rewrite_values' => '',
          ),
          'datepicker_options' => '',
        ),
      );

      // Year.
      $filters["date_filter_y_{$field}"]['id'] = "date_filter_y_{$field}";
      $filters["date_filter_y_{$field}"]['table'] = 'node';
      $filters["date_filter_y_{$field}"]['field'] = "date_filter";
      $filters["date_filter_y_{$field}"]['group'] = 1;
      $filters["date_filter_y_{$field}"]['exposed'] = TRUE;
      $filters["date_filter_y_{$field}"]['expose']['operator_id'] = "date_filter_y_{$field}_op";
      $filters["date_filter_y_{$field}"]['expose']['operator'] = "date_filter_y_{$field}_op";
      $filters["date_filter_y_{$field}"]['expose']['identifier'] = "date_filter_y_{$field}";
      $filters["date_filter_y_{$field}"]['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
      );
      $filters["date_filter_y_{$field}"]['granularity'] = 'year';
      $filters["date_filter_y_{$field}"]['year_range'] = '-6:+0';
      if ($field == 'created') {
        $filters["date_filter_y_{$field}"]['date_fields'] = array(
          "node.{$field}" => "node.{$field}",
        );
      } elseif (in_array($field_info['module'], array('date'))) {
        $filters["date_filter_y_{$field}"]['date_fields'] = array(
          "field_data_{$field}.{$field}_value" => "field_data_{$field}.{$field}_value",
        );
      }

      // Set BEF.
      $exposed_forms['options']['bef']["date_filter_y_{$field}"] = array(
        'bef_format' => 'default',
        'more_options' => array(
          'autosubmit' => 0,
          'is_secondary' => 0,
          'any_label' => t('Filter by year'),
          'bef_filter_description' => '',
          'tokens' => array(
            'available' => array(
              0 => 'global_types',
            ),
          ),
          'rewrite' => array(
            'filter_rewrite_values' => '',
          ),
          'datepicker_options' => '',
        ),
      );
    }
  }
}

/**
 * Enable content type view modes.
 */
function _vactory_generator_set_view_modes($content_type_name, $conf) {
  // Enable view mode for this content type.
  $settings = field_bundle_settings('node', $content_type_name);
  foreach ($conf['displays'] as $key => $value) {
    $settings['view_modes']["vactory_gen_{$key}"]['custom_settings'] = boolval($value['enabled']);
  }
  field_bundle_settings('node', $content_type_name, $settings);
}

/**
 * Get content type fields as array options.
 */
function _vactory_generator_get_fields($type, $display_plugin) {
  $fields = field_info_instances("node", $type->type);
  $options_terms = array();
  $options_date = array();
  $options_number = array();
  $options_date['created'] = t('Post date');
  foreach ($fields as $key => $value) {
    $field_info = field_info_field($key);
    if (in_array($field_info['module'], array('taxonomy'))) {
      $options_terms[$key] = $value['label'];
    }
    elseif (in_array($field_info['module'], array('date'))) {
      $options_date[$key] = $value['label'];
    }
    elseif (in_array($field_info['module'], array('number'))) {
      $options_number[$key] = $value['label'];
    }
  }
  $options = array();
  $options['sorts'] = array_merge($options_number, $options_date);
  if ($display_plugin == 'page') {
    $options['filters'] = array_merge($options_terms, $options_date);
  }
  elseif ($display_plugin == 'block') {
    $options['filters'] = $options_terms;
  }

  $options['pagination'] = array(
    'none' => t('None (all items)'),
    'some' => t('Specified number of items'),
    'full' => t('Full'),
    'mini' => t('Mini'),
  );
  return $options;
}

/**
 * Get vocabulary terms.
 */
function _vactory_generator_get_terms($filter_id) {
  $options = array();
  $field_info = field_info_field($filter_id);
  if (in_array($field_info['module'], array('taxonomy'))) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($field_info['settings']['allowed_values'][0]['vocabulary']);
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    foreach ($terms as $key => $value) {
      $options[$value->tid] = $value->name;
    }
  }
  return $options;
}

/**
 * Render image from .tpl using image style.
 */
function v_gen_render_image($image, $picture_name = 'varticles_thumbnail', $image_style_fallback = 'varticlescustom_user_large_1x') {
  $image_output = '';
  if ($image && isset($image['uri'])) {
    // Get picture mapping.
    $fallback_image_style = $image_style_fallback;
    $picture_mappings = picture_mapping_load($picture_name);
    $breakpoint_styles = picture_get_mapping_breakpoints($picture_mappings, $fallback_image_style);

    // Picture theme.
    $picture = array(
      '#theme' => 'picture',
      '#width' => isset($image['width']) ? $image['width'] : NULL,
      '#height' => isset($image['height']) ? $image['height'] : NULL,
      '#style_name' => $fallback_image_style,
      '#breakpoints' => $breakpoint_styles,
      '#uri' => $image['uri'],
      '#alt' => isset($image['alt']) ? $image['alt'] : '',
      '#attributes' => isset($image['attributes']) ? $image['attributes'] : NULL,
      '#timestamp' => $image['timestamp'],
    );

    // Render picture.
    $image_output = render($picture);
  }
  return $image_output;
}
