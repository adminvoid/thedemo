<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div class="container">
  <article
    class="article-details <?php print $classes; ?>"<?php print $attributes; ?>>

    <div
      class="article-details__title">
      <?php if (!empty($title)): ?>
        <h2<?php print $title_attributes; ?>><a
            href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
      <?php endif; ?>
    </div>

    <div class="article-details-meta">
      <div
        class="article-details__tags"><?php print render($content['field_vactory_news_theme']); ?></div>

      <div
        class="article-details__date"><?php print render($content['field_vactory_date']); ?></div>
    </div>

    <div class="article-details__summary">
      <?php print $node->body['und'][0]['summary']; ?>
    </div>

    <div class="article-details__poster">
      <?php print v_gen_render_image($visual, $picture_name = 'vactory_node_wide', $image_style_fallback = 'node-wide-fullcustom_user_large_1x'); ?>
    </div>

    <div class="article-details__body">
      <?php print render($content['body']); ?>
    </div>

  </article>
</div>
