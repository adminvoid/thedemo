<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>


<div class="row">
  <div class="col-md-7 left-side">
    <?php print $rows[0];  ?>
  </div>
  <div class="col-md-5 right-side">
    <?php array_shift($rows); ?>
    <?php foreach ($rows as $id => $row): ?>
        <?php print $row;  ?>
    <?php endforeach; ?>
  </div>
</div>
