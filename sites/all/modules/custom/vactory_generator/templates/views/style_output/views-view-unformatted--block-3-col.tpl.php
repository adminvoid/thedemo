<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<div class="row">
  <?php foreach ($rows as $id => $row): ?>
    <div<?php if ($classes_array[$id]) {
      print ' class="' . $classes_array[$id] . ' col-md-4 "';
      } ?>>
      <?php print $row; ?>
    </div>
  <?php endforeach; ?>
</div>
