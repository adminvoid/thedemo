<?php

/**
 * @file
 * Theme template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>

<div class="slider-grid">
  <?php foreach ($rows as $id => $row): ?>
    <div class="<?php print $classes_array[$id]; ?> col-md-12"><?php print $row; ?></div>
  <?php endforeach; ?>
</div>
