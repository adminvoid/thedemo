<?php

/**
 * @file
 * Administrative page callbacks for the vactory_header module.
 */

/**
 * Admin settings form.
 *
 * @param array $form
 *   Form API form.
 * @param array $form_state
 *   Form API form.
 * @param bool $no_js_use
 *   Used for this demonstration only. If true means that the form should be
 *   built using a simulated no-javascript approach (ajax.js will not be
 *   loaded.)
 *
 * @return array
 *   Form array.
 */
function vactory_header_admin_settings(array $form, array &$form_state, $no_js_use = FALSE) {
  global $theme;

  $default_value = variable_get(_vactory_header_get_variable_name(), array());

  // Load all available Drupal Blocks.
  $blocks = _block_rehash($theme);
  $weight_delta = count($blocks);

  $form['vactory_header']['template'] = array(
    '#title'         => t('Template'),
    '#type'          => 'select',
    '#description'   => t('Select the desired template.'),
    '#default_value' => (isset($default_value['vactory_header'])) ? $default_value['vactory_header']['template'] : '1',
    '#options'       => array(
      '1' => t('Template 1'),
      '2' => t('Template 2 (deprecated)'),
      '3' => t('Template 3'),
      '4' => t('Template 4'),
      '5' => t('Template 5'),
      '6' => t('Template 6'),
    ),
  );

  // Blocks region configuration for Template 1
  _vactory_header_admin_regions($form, $blocks, $weight_delta, array('template' => 1));

  // Blocks region configuration for Template 2
  $form['vactory_header']['help'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('CSS Help'),
    '#states' => array(
      'visible' => array(
        ':input[name="template"]' => array('value' => '2'),
      ),
    ),
  );

  $form['vactory_header']['help']['css'] = array(
    '#type'   => 'markup',
    '#markup' => '<ul><li>Add CSS class <b>vh-primary-menu</b> to menu block.</li><li>Add CSS class <b>vh-mobile__hide</b> to blocks you wish to hide on mobile.</li><li>Add CSS class <b>vh-mobile__show</b> to blocks you wish to show on mobile.</li></ul>',
  );
  _vactory_header_admin_regions($form, $blocks, $weight_delta, array('template' => 2));

  // Blocks region configuration for Template 3
  $form['vactory_header']['help_3'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('CSS Help'),
    '#states' => array(
      'visible' => array(
        ':input[name="template"]' => array('value' => '3'),
      ),
    ),
  );

  $form['vactory_header']['help_3']['css'] = array(
    '#type'   => 'markup',
    '#markup' => '<ul><li>Add CSS class <b>vh-primary-menu</b> to menu block.</li><li>Add CSS class <b>vh-mobile__hide</b> to blocks you wish to hide on mobile.</li><li>Add CSS class <b>vh-mobile__show</b> to blocks you wish to show on mobile.</li></ul>',
  );

  $form['vactory_header']['v3_hamburger_position'] = array(
    '#type'          => 'radios',
    '#title'         => t('Hamburger Icon position'),
    '#default_value' => (isset($default_value['vactory_header']['v3_hamburger_position'])) ? $default_value['vactory_header']['v3_hamburger_position'] : '1',
    '#options'       => array(t('Left'), t('Right')),
    '#states' => array(
      'visible' => array(
        ':input[name="template"]' => array('value' => '3'),
      ),
    ),
  );

  _vactory_header_admin_regions($form, $blocks, $weight_delta, array('template' => 3));
  _vactory_header_admin_regions($form, $blocks, $weight_delta, array('template' => 4));
  _vactory_header_admin_regions($form, $blocks, $weight_delta, array('template' => 5));

  $form['vactory_header']['help_6'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('CSS Help'),
    '#states' => array(
      'visible' => array(
        ':input[name="template"]' => array('value' => '6'),
      ),
    ),
  );

  $form['vactory_header']['help_6']['css'] = array(
    '#type'   => 'markup',
    '#markup' => '<ul><li>Add CSS class <strong>vh-block--wide</strong> when adding a block to mega menu to make it as wide as possible (Menu item attributes).</li><li>Add CSS class <strong>vh-block--toright</strong> when adding a block to mega menu to make it float right (Menu item attributes).</li><li>Add CSS class <strong>vh-block--toleft</strong> when adding a block to mega menu to make it float left (Menu item attributes).</li></ul>',
  );

  _vactory_header_admin_regions($form, $blocks, $weight_delta, array('template' => 6));

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * @param $form
 * @param $regions
 */
function _vactory_header_admin_regions(&$form, $blocks, $weight_delta, $template) {

  $regions = _vactory_header_get_regions($template['template']);

  $form['vactory_header']['wrapper_' . $template['template']] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Blocks'),
    '#states' => array(
      'visible' => array(
        ':input[name="template"]' => array('value' => $template['template']),
      ),
    ),
  );

  $form['vactory_header']['wrapper_' . $template['template']]['regions_blocks_' . $template['template']] = array(
    '#type'   => 'vertical_tabs',
    '#states' => array(
      'visible' => array(
        ':input[name="template"]' => array('value' => $template['template']),
      ),
    ),
  );

  $i = 1;
  $fieldset = [];

  foreach ($regions as $region => $region_label) {
    $fieldset[$region]['enabled_disabled_blocks'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Enabled / Disabled Blocks in') . ' ' . $region_label . ' ' . t('region'),
      '#description' => t('The region content will be generated from the list of "enabled" blocks. Re-order the list to change the priority of each blocks.'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
      '#group'       => 'regions_blocks_' . $template['template'],
      '#weight'      => $i,
    );

    // Orderable list of block selections.
    $fieldset[$region]['enabled_disabled_blocks']['enabled_disabled_blocks_' . $region] = array(
      '#tree'   => TRUE,
      '#region' => $region,
      '#theme'  => 'vactory_skeleton_blocks_table',
    );

    _vactory_header_set_blocks($blocks, $region);
    foreach ($blocks as $id => $block) {
      // Load block titles.
      $title = !empty($block['info']) ? $block['info'] : $block['delta'];

      // Ensure that regex patterns do not cause invalid id attributes.
      $safe_id_prefix = 'edit-vactory-skeleton-block-' . $block['bid'];

      $block_weight = variable_get('vactory_header_enabled_disabled_blocks_' . $region . '_weight' . $block['bid'], 0);
      $block['weight'] = $block_weight;

      $fieldset[$region]['enabled_disabled_blocks']['enabled_disabled_blocks_' . $region][$block['bid']] = array(
        'enabled'       => array(
          '#type'          => 'checkbox',
          '#id'            => $safe_id_prefix . '-enabled',
          '#title'         => '',
          '#default_value' => variable_get('vactory_header_enabled_disabled_blocks_' . $region . $block['bid']),
        ),
        'label'         => array(
          '#value' => $block['info'],
        ),
        'weight'        => array(
          '#type'          => 'weight',
          '#default_value' => $block['weight'],
          '#delta'         => $weight_delta,
          '#id'            => $safe_id_prefix . '-weight-wrapper',
        ),
        'module'        => array(
          '#type'  => 'value',
          '#value' => $block['module'],
        ),
        'title'         => array(
          '#type'  => 'value',
          '#value' => $title,
        ),
        'title_display' => array(
          '#type'   => 'markup',
          '#markup' => check_plain($title),
        ),
        'bid'           => array(
          '#type'  => 'value',
          '#value' => $block['bid'],
        ),
        'delta'         => array(
          '#type'  => 'value',
          '#value' => $block['delta'],
        ),
      );
    }

    $form['vactory_header']['wrapper_' . $template['template']][$region] = $fieldset[$region];

    $i++;
  }
}

/**
 * Add a submit handler/function to the form.
 *
 * This will save data and add a completion message to the screen when the
 * form successfully processes.
 */
function vactory_header_admin_settings_submit($form, &$form_state) {
  $data['vactory_header'] = array(
    'template' => $form_state['values']['template'],
    'v3_hamburger_position' => $form_state['values']['v3_hamburger_position'],
  );

  $regions = _vactory_header_get_regions($form_state['values']['template']);

  foreach ($regions as $region => $region_label) {
    $enabled_blocks = array();
    $blocks = $form_state['values']['enabled_disabled_blocks_' . $region];
    _vactory_header_sort_blocks($blocks);

    foreach ($blocks as $block) {
      variable_set('vactory_header_enabled_disabled_blocks_' . $region . $block['bid'], $block['enabled']);
      variable_set('vactory_header_enabled_disabled_blocks_' . $region . '_weight' . $block['bid'], $block['weight']);

      if ($block['enabled'] == 1) {
        $enabled_blocks[] = $block;
      }
    }

    variable_set('vactory_header_enabled_blocks_' . $region, $enabled_blocks);
  }

  variable_set(_vactory_header_get_variable_name(), $data);
  drupal_set_message(t('Successfully updated settings.'));
}
