;
(function ($) {

    'use strict';

    var header = {};
    header.behaviors = {};
    header.dom = {
        'mobile_menu': $('#vhm-menu')
    };

    header.init = function () {
        header.behaviors.preprocess();
        header.behaviors.submenu();
        header.behaviors.burger();
    };

    header.behaviors.preprocess = function () {
        $('#main-wrapper, #vh-header').addClass('vh-push-left');
    };

    // Open submenu.
    header.behaviors.submenu = function () {
        // Desktop.
        $('.vh-primary-menu > .block__content > .menu-wrapper > .menu > li.expanded > a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            $('.vh-header .primary-menu > .menu-wrapper > .menu > li.expanded > .menu-wrapper').removeClass('open');
            $('.vh-header .primary-menu > .menu-wrapper > .menu > li.expanded > a').removeClass('selected');

            if (submenu.css('visibility') == 'visible') {
                selected.removeClass('selected');
                submenu.removeClass('open');
            } else {
                selected.addClass('selected');
                submenu.addClass('open');
            }
        });

        // Mobile
        $('.vhm-menu li.expanded>a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            if (submenu.css('display') == 'block') {
                selected.removeClass('selected');
                submenu.slideUp();

                submenu.find('.expanded>a').removeClass('selected');
                submenu.find('.menu-wrapper').slideUp();
            } else {
                selected.addClass('selected');
                submenu.slideDown();
            }
        });

        $('.vhm-menu li.expanded>a[class^="active"]').trigger('click');

    };

    // Burger button.
    header.behaviors.burger = function () {
        $('#mmenu-btn').click(function () {
            if (!header.dom.mobile_menu.hasClass('is-open')) {
                header.dom.mobile_menu.addClass('is-open');
                $('#mmenu-btn').addClass('is-active');
                $('#main-wrapper, #vh-header').addClass('is-open');
                $('#main-wrapper').addClass('has-overlay');
            }
            else {
                header.dom.mobile_menu.removeClass('is-open');
                $('#mmenu-btn').removeClass('is-active');
                $('#main-wrapper, #vh-header').removeClass('is-open');
                $('#main-wrapper').removeClass('has-overlay');
            }
        });

        $('#main-wrapper').click(function () {
            if ($(this).hasClass('has-overlay')) {
                $('#mmenu-btn').trigger('click');
            }
        });
    };

    header.init();

})(jQuery);