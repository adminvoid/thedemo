;
(function ($) {

    'use strict';

    var header = {};
    header.behaviors = {};
    header.components = {};
    header.dom = {
        'mobile_menu': $('#vhm-menu')
    };
    header.states = {
        current_subMenu: []
    };

    header.init = function () {
        header.behaviors.preprocess();
        header.behaviors.submenu();
        header.behaviors.burger();
    };

    header.behaviors.preprocess = function () {
        //$('#main-wrapper, #vh-header').addClass('vh-push-left');
    };

    // Open submenu.
    header.behaviors.submenu = function () {

    };

    // Burger button.
    header.behaviors.burger = function () {
        $('#menu-btn').click(function (e) {
            e.stopPropagation();
            if ($('.main-wrap').hasClass('is-menu-open')) {
                $('.main-wrap').removeClass('is-menu-open');
                $(this).removeClass('is-active');
                $('html, body').removeClass('vh-disable__scroll');
            }
            else {
                $('.main-wrap').addClass('is-menu-open');
                $(this).addClass('is-active');
                $('html, body').addClass('vh-disable__scroll');
            }

        });

        $(document).on('keyup', function(ev) {
            // Escape key.
            if( ev.keyCode == 27 ) {
                if ($('.main-wrap').hasClass('is-menu-open')) {
                    $('#menu-btn').trigger('click');
                }
            }
        });


        // Back button.
        $('#menu-back-link').click(function(e) {
            e.preventDefault();

            var size = header.states.current_subMenu.length;

            if (size == 0) {
                $('.vh-main_menu').removeClass('is-sub-menu-open');
                return;
            }

            // Remove CSS class from last item and remove it.
            header.states.current_subMenu[size - 1].removeClass('selected');
            header.states.current_subMenu.pop();

            // Check again.
            size = header.states.current_subMenu.length;
            if (size == 0) {
                $('.vh-main_menu').removeClass('is-sub-menu-open');
            }


        });

        // Open sub menus.
        $('.vh-main_menu li.expanded a').click(function (e) {
            e.preventDefault();

            var parent = $(this).parent('li');

            parent.addClass('selected');
            $('.vh-main_menu').addClass('is-sub-menu-open');
            header.states.current_subMenu.push(parent);
        });


        $('.vh-header--menu .expanded > a').click(function(e) {
            e.preventDefault();
        });

    };


    header.init();

})(jQuery);