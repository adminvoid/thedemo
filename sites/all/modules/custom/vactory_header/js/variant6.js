;
(function ($) {

    'use strict';

    var header = {};
    header.behaviors = {};
    header.structure = {};
    header.dom = {
        'mobile_menu': $('#vhm-menu')
    };

    header.init = function () {
        header.structure.mobile_menu();
        header.behaviors.stick();
        header.behaviors.submenu();
        header.behaviors.burger();
        header.behaviors.scrollIndicator();
    };

    // Open submenu.
    header.behaviors.submenu = function () {
        // Desktop.
        $('.vh-header .primary-menu .block__content > .menu-wrapper > .menu > li.expanded > a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            $('.vh-header .primary-menu .block__content > .menu-wrapper > .menu > li.expanded > .menu-wrapper').removeClass('open');
            $('.vh-header .primary-menu .block__content > .menu-wrapper > .menu > li.expanded > a').removeClass('selected');

            if (submenu.css('visibility') == 'visible') {
                selected.removeClass('selected');
                submenu.removeClass('open');
            } else {
                selected.addClass('selected');
                submenu.addClass('open');
            }
        });

        // Mobile
        $('.vhm-menu li.expanded>a').on('click', function (event) {
            event.preventDefault();

            var selected = $(this);
            var submenu = selected.next('.menu-wrapper');

            if (submenu.css('display') == 'block') {
                selected.removeClass('selected');
                submenu.slideUp();

                submenu.find('.expanded>a').removeClass('selected');
                submenu.find('.menu-wrapper').slideUp();
            } else {
                selected.addClass('selected');
                submenu.slideDown();
            }
        });

        $('.vhm-menu li.expanded>a[class^="active"]').trigger('click');

        // Sub menu close button.
        var $button_close = $('<div class="vh-menu-button-close"><div class="container"><a href="#." class="button-close"><i class="icon-close-circle-flat"></i></a></div></div>');
        $('.vh-header .primary-menu .block__content > .menu-wrapper > .menu > .expanded > .menu-wrapper').append($button_close);
        $('.vh-menu-button-close .button-close').on('click', function (e) {
            e.preventDefault();
            $('.vh-header .primary-menu .block__content > .menu-wrapper > .menu > li.expanded > a[class*="selected"]').trigger('click');
        });
    };

    // Sticky header.
    header.behaviors.stick = function () {
        var $head = $('#vh-header');
        var $offset = '100px';

        if ($('body').hasClass('logged-in')) {
            $offset = '50px';
        }

        $('#content').waypoint(function (direction) {

            if ($('.vh-header.variant6 .vh-menus').css('display') == 'none') {
                $head.removeClass('vh-header-small');
                return;
            }

            if (direction === 'down') {
                $head.addClass('vh-header-small');
            }
            else if (direction === 'up') {
                $head.removeClass('vh-header-small');
            }
        }, {offset: $offset});
    };


    header.behaviors.responsive = function () {
        if ($('.vh-header.variant6 .vh-menus').css('display') == 'none') {
            $('body').addClass('is-mobile');
        }
        else {
            $('body').removeClass('is-mobile');
        }
    };

    // Scroll Indicator.
    header.behaviors.scrollIndicator = function () {
        var element = $('.vh-header .scroll-indicator');
        $(window).scroll(function () {
            var offsettop = parseInt($(this).scrollTop());
            var parentHeight = parseInt($('body, html').height() - $(window).height());
            var vscrollwidth = offsettop / parentHeight * 100;
            element.css({width: vscrollwidth + '%'});
        });
    };

    // Burger button.
    header.behaviors.burger = function () {
        $('#mmenu-open-btn').click(function () {
            if (!header.dom.mobile_menu.hasClass('is-open')) {
                header.dom.mobile_menu.addClass('is-open');

                header.dom.mobile_menu.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                    $('#mmenu-open-btn').addClass('is-active');
                    $('#mmenu-close-btn').addClass('is-active');
                });

            }
        });

        $('#mmenu-close-btn').click(function () {
            if (header.dom.mobile_menu.hasClass('is-open')) {
                $('#mmenu-open-btn').removeClass('is-active');
                header.dom.mobile_menu.removeClass('is-open');

                header.dom.mobile_menu.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function (e) {
                    $('#mmenu-open-btn').removeClass('is-active');
                    $('#mmenu-close-btn').removeClass('is-active');
                });
            }
        });
    };

    // Clone primary menu to mobile.
    header.structure.mobile_menu = function () {
        if (!$('.vh-header .primary-menu .block__content > .menu-wrapper').length) {
            return;
        }

        var $menu = $('.vh-header .primary-menu .block__content > .menu-wrapper').clone();
        $('.vhm-menu__primary-menu').append($menu);
    };

    header.init();

})(jQuery);