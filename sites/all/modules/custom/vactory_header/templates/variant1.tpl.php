<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div id="vh-header" class="vh-header variant1 vh-sticky vh-header-large">
  <div class="layout">
    <div class="vh-logo">
      <?php print $logo; ?>
    </div>
    <div class="vh-menus">
      <div class="secondary-menu">
        <?php print $region_top_left; ?>
        <?php print $secondary_menu; ?>
        <?php print $region_top_right; ?>
      </div>
      <div class="separator"></div>
      <div class="primary-menu">
        <?php print $region_bottom_left; ?>
        <?php print $main_menu; ?>
        <?php print $region_bottom_right; ?>
      </div>
    </div>
    <div class="vh-hamburger">
      <button id="mmenu-open-btn" class="hamburger hamburger--collapse"
              type="button" aria-label="Menu" role="button"
              aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
      </button>
    </div>
  </div>
</div>

<div class="variant1">
  <div id="vhm-menu" class="vhm-menu">
    <div class="vhm-menu__top-wrapper">
      <div class="vhm-menu__logo">
        <?php print $logo; ?>
      </div>
      <div class="vhm-menu__hamburger">
        <button id="mmenu-close-btn" class="hamburger hamburger--collapse"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
        </button>
      </div>
    </div>
    <div class="vhm-menu_search">
      <?php print $search; ?>
    </div>
    <div class="vhm-menu__primary-menu">
      <?php // Using Javascript. We clone $main_menu and append it here. ?>
    </div>

    <?php print $region_mobile_bottom; ?>
  </div>
</div>
