<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div class="variant3">
  <div id="vh-header" class="vh-header vh-sticky vh-header-large">
    <div class="container">
      <?php if ($config['hamburger_position'] == 0): ?>
        <div class="vhm-menu__hamburger hidden vh-mobile__show">
          <button id="mmenu-btn" class="hamburger hamburger--spin"
                  type="button" aria-label="Menu" role="button"
                  aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
          </button>
        </div>
      <?php endif; ?>

      <?php print $desktop; ?>

      <?php if ($config['hamburger_position'] == 1): ?>
      <div class="vhm-menu__hamburger hidden vh-mobile__show">
        <button id="mmenu-btn" class="hamburger hamburger--spin"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
        </button>
      </div>
      <?php endif; ?>
    </div>
  </div>

  <div id="vhm-menu" class="vhm-menu <?php print $config['menu_position']; ?>">
    <?php print $mobile; ?>
  </div>
</div>
