<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div class="variant5">
  <div id="vh-header"
       class="vh-header">
    <div class="container-fluid">

      <!-- Burger menu -->
      <div class="vhm-menu__hamburger">
        <button id="menu-btn" class="hamburger hamburger--spin"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
        </button>
      </div>
      <!-- ./Burger menu -->

      <!-- Logo -->
      <div class="block vh-logo">
        <div class="block__content">
          <?php print $defaults_blocks['logo']; ?>
        </div>
      </div>
      <!-- ./Logo -->

      <?php if (isset($custom_blocks['t5_desktop_left']) && !empty($custom_blocks['t5_desktop_left'])): ?>
        <!-- Custom Blocks Left -->
        <div class="block vh-custom_blocks--left">
          <div class="block__content">
            <?php print $custom_blocks['t5_desktop_left']; ?>
          </div>
        </div>
        <!-- ./Custom Blocks Left -->
      <?php endif; ?>

      <!-- Navigation Menu -->
      <div class="block vh-navigation_menu">
        <div class="block__content">
          <?php print $defaults_blocks['navigation_menu']; ?>
        </div>
      </div>
      <!-- ./Navigation Menu -->

      <?php if (isset($custom_blocks['t5_desktop_right']) && !empty($custom_blocks['t5_desktop_right'])): ?>
        <!-- Custom Blocks Right -->
        <div class="block vh-custom_blocks--right">
          <div class="block__content">
            <?php print $custom_blocks['t5_desktop_right']; ?>
          </div>
        </div>
        <!-- ./Custom Blocks Right -->
      <?php endif; ?>

      <!-- Navigation Menu -->
      <?php // @note: To enable search, you may quickly use vactory_search_overlay module. Otherwise, have fun ^^.  ?>
      <div class="block vh-mobile_search visible-xs-block">
        <div class="block__content">
          <a href="javascript:void(0);" class="btn-search-overlay"><i class="icon-search" aria-hidden="true"></i></a>
        </div>
      </div>
      <!-- ./Navigation Menu -->


    </div>
    <!-- ./Container -->

    <div class="scroll-indicator"></div>

  </div>
  <!-- ./Header -->

</div>

<div class="variant5">
  <div class="vh-header--menu">

    <!-- Main Menu -->
    <div class="block vh-main_menu">
      <h6 class="menu-heading"><a href="#." rel="nofollow"
                                  id="menu-back-link"><i class="icon-chevron-left" aria-hidden="true"></i></a><?php print t('Menu'); ?>
      </h6>

      <div class="block__content">
        <?php print $defaults_blocks['main_menu']; ?>
      </div>
    </div>
    <!-- ./Main Menu -->

    <!-- Secondary Menu -->
    <div class="block vh-secondary_menu">
      <div class="block__content">
        <?php print $defaults_blocks['secondary_menu']; ?>
      </div>
    </div>
    <!-- ./Secondary Menu -->

    <!-- Secondary Menu -->
    <div class="block vh-social-media--links">
      <div class="block__content">
        <?php print $defaults_blocks['social_media']; ?>
      </div>
    </div>
    <!-- ./Secondary Menu -->

    <?php if (isset($custom_blocks['t5_mobile_push_bottom']) && !empty($custom_blocks['t5_mobile_push_bottom'])): ?>
      <!-- Custom Blocks Bottom -->
      <div class="block vh-custom_blocks--bottom">
        <div class="block__content">
          <?php print $custom_blocks['t5_mobile_push_bottom']; ?>
        </div>
      </div>
      <!-- ./Custom Blocks Bottom -->
    <?php endif; ?>

  </div>
</div>
