<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<div id="vh-header" class="vh-header variant6 vh-sticky vh-header-large">
  <div class="vh-header--top">
    <div class="layout">
      <?php print $custom_blocks['t6_top_left']; ?>
      <!-- Social Links -->
      <div class="block vh-social-links">
        <div class="block__content">
          <?php print $defaults_blocks['social_media']; ?>
        </div>
      </div>
      <!-- ./Social Links -->

      <div class="secondary-menu">
        <?php print $defaults_blocks['secondary_menu']; ?>
      </div>
      <!-- Search Icon Button -->
      <div class="block vh-component__search">
        <a class="btn-search-overlay" href="javascript:void(0);">
          <i class="icon-search"></i>
        </a>
      </div>
      <!-- ./Search Icon Button -->
      <?php print $custom_blocks['t6_top_right']; ?>
    </div>
  </div>
  <div class="layout">
    <div class="vh-logo">
      <?php print $defaults_blocks['logo']; ?>
    </div>
    <div class="vh-menus">
      <div class="primary-menu">
        <?php print $custom_blocks['t6_bottom_left']; ?>
        <?php print $defaults_blocks['main_menu']; ?>
        <!-- Language Dropdown -->
        <div class="block vh-language_dropdown">
          <div class="block__content">
            <?php print $defaults_blocks['language_dropdown']; ?>
          </div>
        </div>
        <!-- ./Language Dropdown -->
        <?php print $custom_blocks['t6_bottom_right']; ?>
      </div>
    </div>
    <div class="vh-hamburger">
      <button id="mmenu-open-btn" class="hamburger hamburger--collapse"
              type="button" aria-label="Menu" role="button"
              aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
      </button>
    </div>
  </div>
  <div class="scroll-indicator"></div>
</div>

<div class="variant6 visible-xs-block">
  <div id="vhm-menu" class="vhm-menu">
    <div class="vhm-menu__top-wrapper">
      <div class="vhm-menu__logo">
        <?php print $defaults_blocks['logo']; ?>
      </div>
      <div class="vhm-menu__hamburger">
        <button id="mmenu-close-btn" class="hamburger hamburger--collapse"
                type="button" aria-label="Menu" role="button"
                aria-controls="navigation" aria-expanded="true/false">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
        </button>
      </div>
    </div>
    <div class="vhm-menu_search">
      <?php print $defaults_blocks['search_form']; ?>
    </div>
    <div class="vhm-menu__primary-menu">
      <?php // Using Javascript. We clone $main_menu and append it here. ?>
    </div>

    <?php print $custom_blocks['t6_mobile_bottom']; ?>
  </div>
</div>
