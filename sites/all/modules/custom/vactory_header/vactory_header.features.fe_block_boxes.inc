<?php
/**
 * @file
 * vactory_header.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function vactory_header_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Mega Menu News Block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'vh_mega_menu_news_block';
  $fe_block_boxes->body = '<p>&nbsp;</p><p><img alt="" class="img-responsive" src="https://placehold.it/768x432?text=Push+Block"></p><br><p>Le <b>Lorem Ipsum</b>est simplement du faux texte employé dans la composition et la mise en page avant impression.</p><p><br><a class="btn btn-primary" href="#">view more</a></p>';

  $export['vh_mega_menu_news_block'] = $fe_block_boxes;

  return $export;
}
