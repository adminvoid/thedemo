<?php
/**
 * @file
 * vactory_header.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function vactory_header_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-v-header-v1-secondary-menu.
  $menus['menu-v-header-v1-secondary-menu'] = array(
    'menu_name' => 'menu-v-header-v1-secondary-menu',
    'title' => 'Secondary Menu',
    'description' => 'The <em>Secondary</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Exported menu: menu-v-header-v5-navigation.
  $menus['menu-v-header-v5-navigation'] = array(
    'menu_name' => 'menu-v-header-v5-navigation',
    'title' => 'Header Navigation',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => 5,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Header Navigation');
  t('Main menu');
  t('Secondary Menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>Secondary</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');

  return $menus;
}
