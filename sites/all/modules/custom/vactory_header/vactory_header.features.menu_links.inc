<?php
/**
 * @file
 * vactory_header.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function vactory_header_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-v-header-v5-navigation_contact:<front>.
  $menu_links['menu-v-header-v5-navigation_contact:<front>'] = array(
    'menu_name' => 'menu-v-header-v5-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Contact',
    'options' => array(
      'identifier' => 'menu-v-header-v5-navigation_contact:<front>',
      'attributes' => array(
        'class' => array(
          0 => 'icon-phone',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-v-header-v5-navigation_pro:<front>.
  $menu_links['menu-v-header-v5-navigation_pro:<front>'] = array(
    'menu_name' => 'menu-v-header-v5-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Pro',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => 'pro-btn',
        'style' => '',
      ),
      'identifier' => 'menu-v-header-v5-navigation_pro:<front>',
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-v-header-v5-navigation_profile:<front>.
  $menu_links['menu-v-header-v5-navigation_profile:<front>'] = array(
    'menu_name' => 'menu-v-header-v5-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Profile',
    'options' => array(
      'identifier' => 'menu-v-header-v5-navigation_profile:<front>',
      'attributes' => array(
        'class' => array(
          0 => 'icon-user',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-v-header-v5-navigation_search:<front>.
  $menu_links['menu-v-header-v5-navigation_search:<front>'] = array(
    'menu_name' => 'menu-v-header-v5-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Search',
    'options' => array(
      'identifier' => 'menu-v-header-v5-navigation_search:<front>',
      'attributes' => array(
        'class' => array(
          0 => 'icon-search',
          1 => 'btn-search-overlay',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-v-header-v5-navigation_wishlist:<front>.
  $menu_links['menu-v-header-v5-navigation_wishlist:<front>'] = array(
    'menu_name' => 'menu-v-header-v5-navigation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Wishlist',
    'options' => array(
      'identifier' => 'menu-v-header-v5-navigation_wishlist:<front>',
      'attributes' => array(
        'class' => array(
          0 => 'icon-heart',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Contact');
  t('Pro');
  t('Profile');
  t('Search');
  t('Wishlist');

  return $menu_links;
}
