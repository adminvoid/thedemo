<?php
/**
 * @file
 * Vactory Headers.
 */

/**
 * Define the default variable name fo this module.
 */
define('VACTORY_HEADER_VARIABLE_DATA', 'vactory_header:data');

/**
 * Implements hook_theme().
 */
function vactory_header_theme($existing, $type, $theme, $path) {
  return array(
    'vh_variant1'    => array(
      'template'  => 'variant1',
      'variables' => array(
        'logo'                 => NULL,
        'main_menu'            => NULL,
        'secondary_menu'       => NULL,
        'search'               => NULL,
        'region_top_left'      => array(),
        'region_top_right'     => array(),
        'region_bottom_left'   => array(),
        'region_bottom_right'  => array(),
        'region_mobile_bottom' => array(),
      ),
      'path'      => $path . '/templates',
    ),
    'vh_variant2'    => array(
      'template'  => 'variant2',
      'variables' => array(
        'linear' => NULL,
        'mobile' => NULL,
      ),
      'path'      => $path . '/templates',
    ),
    'vh_variant3'    => array(
      'template'  => 'variant3',
      'variables' => array(
        'config' => array(),
        'linear' => NULL,
        'mobile' => NULL,
      ),
      'path'      => $path . '/templates',
    ),
    'vh_variant4'    => array(
      'template'  => 'variant4',
      'variables' => array(
        'config'          => array(),
        'defaults_blocks' => array(),
        'custom_blocks'   => array(),
      ),
      'path'      => $path . '/templates',
    ),
    'vh_variant5'    => array(
      'template'  => 'variant5',
      'variables' => array(
        'config'          => array(),
        'defaults_blocks' => array(),
        'custom_blocks'   => array(),
      ),
      'path'      => $path . '/templates',
    ),
    'vh_variant6'    => array(
      'template'  => 'variant6',
      'variables' => array(
        'config'          => array(),
        'defaults_blocks' => array(),
        'custom_blocks'   => array(),
      ),
      'path'      => $path . '/templates',
    ),
    'vh_region'      => array(
      'template'  => 'region_blocks',
      'variables' => array(
        'custom_classes' => '',
        'blocks'         => array(),
      ),
      'path'      => $path . '/templates',
    ),
    'vh_search_icon' => array(
      'template' => 'search_icon',
      'path'     => $path . '/templates',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function vactory_header_menu() {
  $items['admin/config/user-interface/vactory_header'] = array(
    'title'            => 'Header',
    'description'      => 'Configure header',
    'page callback'    => 'drupal_get_form',
    'file'             => 'include/vactory_header.admin.inc',
    'page arguments'   => array('vactory_header_admin_settings'),
    'access arguments' => array('administer vactory header'),
    'type'             => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Get variable name.
 *
 * @return string
 *   Multilanguage variable name.
 */
function _vactory_header_get_variable_name() {
  global $language;
//  return VACTORY_HEADER_VARIABLE_DATA . $language->language;
  return VACTORY_HEADER_VARIABLE_DATA;
}

/**
 * @param $template
 * @return array
 */
function _vactory_header_get_regions($template) {
  switch ($template) {
    case 1:
      return array(
        't1_top_left'      => t('Top Left'),
        't1_top_right'     => t('Top Right'),
        't1_bottom_left'   => t('Bottom Left'),
        't1_bottom_right'  => t('Bottom Right'),
        't1_mobile_bottom' => t('Mobile Bottom'),
      );
    case 2:
      return array(
        't2_linear' => t('Desktop'),
        't2_mobile' => t('Mobile Push Menu'),
      );
    case 3:
      return array(
        't3_linear' => t('Desktop'),
        't3_mobile' => t('Mobile Push Menu'),
      );
    case 4:
      return array(
        't4_linear' => t('Desktop'),
        't4_mobile' => t('Mobile Push Menu'),
      );
    case 5:
      return array(
        't5_desktop_left'       => t('Desktop Left'),
        't5_desktop_right'      => t('Desktop Right'),
        't5_mobile_push_bottom' => t('Mobile Push Bottom'),
      );
    case 6:
      return array(
        't6_top_left'      => t('Top Left'),
        't6_top_right'     => t('Top Right'),
        't6_bottom_left'   => t('Bottom Left'),
        't6_bottom_right'  => t('Bottom Right'),
        't6_mobile_bottom' => t('Mobile Bottom'),
      );
  }

  return array();
}

/**
 * Implements hook_block_info().
 */
function vactory_header_block_info() {
  $blocks['headers'] = array(
    'info'  => t('Header'),
    // Default setting.
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );

  $blocks['search_icon'] = array(
    'info'  => t('Search Overlay Icon'),
    // Default setting.
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function vactory_header_block_view($delta = '') {
  global $language;

  $is_rtl = ($language->direction == 1) ? TRUE : FALSE;

  $default_value = variable_get(_vactory_header_get_variable_name(), array(
    'vactory_header' => array(
      'template'              => 1,
      'v3_hamburger_position' => 1,
    )
  ));

  $template_id = $default_value['vactory_header']['template'];
  $regions = _vactory_header_get_regions($template_id);

  $block = [];
  $path = drupal_get_path('module', 'vactory_header');

  switch ($delta) {
    case 'headers':
      $block['subject'] = '';

      // Get custom blocks.
      $custom_blocks = array();
      foreach ($regions as $region => $region_label) {
        $enabled_blocks = variable_get('vactory_header_enabled_blocks_' . $region);
        if (isset($enabled_blocks)) {
          $blocks = array();
          foreach ($enabled_blocks as $key => $value) {
            $_block = block_load($value['module'], $value['delta']);
            $block_render = _block_render_blocks(array($_block));
            $block_renderable_array = _block_get_renderable_array($block_render);
            $output = drupal_render($block_renderable_array);
            $blocks[$value['bid']] = array('#markup' => $output);

            // Special case.
            // La variant 3 est composé de façon manuelle
            // Pour éviter que le menu soit dupliquer il faut le supprimer
            // de la version mobile et utiliser JS pour le déplacer.
            if ($template_id == 3 && $region == 't3_mobile' && strpos($_block->css_class, 'vh-primary-menu') !== FALSE) {
              unset($blocks[$value['bid']]);
              $blocks[$value['bid']] = array('#markup' => '<div id="vhm-primary-menu-placeholder"></div>');
            }
          }

          $custom_blocks[$region] = theme('vh_region', array(
            'custom_classes' => 'vh-region vh-region__' . $region,
            'blocks'         => $blocks,
          ));
        }
      }

      // Add Waypoints common JS.
      //drupal_add_js($path . "/js/jquery.waypoints.min.js", array('scope' => 'footer'));
      drupal_add_js($path . "/js/selectFx.js", array('scope' => 'footer'));

      // Variant 1.
      if ($template_id == 1) {
        // Get logo.
        $block_logo = blockify_block_view('blockify-logo');
        // Get menu.
        $block_main_menu = module_invoke('system', 'block_view', 'main-menu');
        // Get secondary menu.
        $block_secondary_menu = block_load('menu', 'menu-v-header-v1-secondary-menu');
        // Get search form.
        $search_form = drupal_get_form('search_block_form');
        // Get custom regions blocks.

        // Render whole block.
        $block['content'] = theme('vh_variant1', array(
          'logo'                 => render($block_logo['content']),
          'main_menu'            => render($block_main_menu['content']),
          'secondary_menu'       => _vactory_render_block($block_secondary_menu),
          'search'               => drupal_render($search_form),
          'region_top_left'      => $custom_blocks['t1_top_left'],
          'region_top_right'     => $custom_blocks['t1_top_right'],
          'region_bottom_left'   => $custom_blocks['t1_bottom_left'],
          'region_bottom_right'  => $custom_blocks['t1_bottom_right'],
          'region_mobile_bottom' => $custom_blocks['t1_mobile_bottom'],
        ));

        // Add CSS & JS.
        drupal_add_js($path . "/js/variant1.js", array('scope' => 'footer'));
      }

      // Variant 2.
      elseif ($template_id == 2) {
        // Render whole block.
        $block['content'] = theme('vh_variant2', array(
          'desktop' => isset($custom_blocks['t2_linear']) ? $custom_blocks['t2_linear'] : '',
          'mobile'  => isset($custom_blocks['t2_mobile']) ? $custom_blocks['t2_mobile'] : '',
        ));
        // Add CSS & JS.
        drupal_add_js($path . "/js/variant2.js", array('scope' => 'footer'));
      }

      // Variant 3.
      elseif ($template_id == 3) {
        // Render whole block.
        $block['content'] = theme('vh_variant3', array(
          'desktop' => isset($custom_blocks['t3_linear']) ? $custom_blocks['t3_linear'] : '',
          'mobile'  => isset($custom_blocks['t3_mobile']) ? $custom_blocks['t3_mobile'] : '',
          'config'  => array(
            'hamburger_position' => $default_value['vactory_header']['v3_hamburger_position'],
            'menu_position'      => ($default_value['vactory_header']['v3_hamburger_position'] == 0) ? 'is-leftside' : '',
          ),
        ));
        // Add CSS & JS.
        drupal_add_js($path . "/js/variant3.js", array('scope' => 'footer'));

        drupal_add_js(
          array(
            'v_header' => array(
              'hamburger_position' => $default_value['vactory_header']['v3_hamburger_position'],
            ),
          ),
          array('type' => 'setting')
        );

      }
      // Variant 4.
      elseif ($template_id == 4) {
        $defaults_blocks = [];
        $config = [];

        // Get logo.
        $defaults_blocks['logo'] = render(blockify_block_view('blockify-logo')['content']);
        // Get menu.
        $defaults_blocks['main_menu'] = render(module_invoke('system', 'block_view', 'main-menu')['content']);
        // Get language dropdown.
        $defaults_blocks['language_dropdown'] = render(module_invoke('lang_dropdown', 'block_view', 'language')['content']);
        // Get social media links.
        $defaults_blocks['social_media'] = render(module_invoke('social_media_links', 'block_view', 'social-media-links')['content']);
        // Get search icon.
        $defaults_blocks['search_icon'] = render(module_invoke('vactory_header', 'block_view', 'search_icon')['content']);
        // Get search form.
        $search_form = drupal_get_form('search_block_form');
        $defaults_blocks['search_form'] = render($search_form);
        // Get login form.
        $login_form = drupal_get_form("user_login");
        $defaults_blocks['login_form'] = drupal_render($login_form);


        // RTL Support.
        $config['is_rtl'] = $is_rtl;

        // Render whole block.
        $block['content'] = theme('vh_variant4', array(
          'config'          => $config,
          'defaults_blocks' => $defaults_blocks,
          'desktop'         => isset($custom_blocks['t4_linear']) ? $custom_blocks['t4_linear'] : '',
          'mobile'          => isset($custom_blocks['t4_mobile']) ? $custom_blocks['t4_mobile'] : '',
        ));
        // Add CSS & JS.
        drupal_add_js($path . "/js/variant4.js", array('scope' => 'footer'));
      }

      // Variant 5.
      elseif ($template_id == 5) {
        $defaults_blocks = [];
        $config = [];

        // Get logo.
        $defaults_blocks['logo'] = render(blockify_block_view('blockify-logo')['content']);

        // Get menu.
        $defaults_blocks['main_menu'] = render(module_invoke('system', 'block_view', 'main-menu')['content']);
        $defaults_blocks['navigation_menu'] = render(module_invoke('menu', 'block_view', 'menu-v-header-v5-navigation')['content']);
        $defaults_blocks['secondary_menu'] = render(module_invoke('menu', 'block_view', 'menu-v-header-v1-secondary-menu')['content']);
        // Get social media links.
        $defaults_blocks['social_media'] = render(module_invoke('social_media_links', 'block_view', 'social-media-links')['content']);

        // Render whole block.
        $block['content'] = theme('vh_variant5', array(
          'config'          => $config,
          'defaults_blocks' => $defaults_blocks,
          'custom_blocks'   => $custom_blocks,
        ));
        // Add CSS & JS.
        drupal_add_js($path . "/js/variant5.js", array('scope' => 'footer'));
      }

      // Variant 1.
      elseif ($template_id == 6) {
        $defaults_blocks = [];
        $config = [];

        // Get logo.
        $defaults_blocks['logo'] = render(blockify_block_view('blockify-logo')['content']);
        // Get menu.
        $defaults_blocks['main_menu'] = v_render_block('system', 'main-menu');
        // Get secondary menu.
        $defaults_blocks['secondary_menu'] = v_render_block('menu', 'menu-v-header-v1-secondary-menu');
        // Get search form.
        $search_form = drupal_get_form('search_block_form');
        $defaults_blocks['search_form'] = render($search_form);
        // Get search icon.
        $defaults_blocks['search_icon'] = render(module_invoke('vactory_header', 'block_view', 'search_icon')['content']);
        // Get language dropdown.
        $defaults_blocks['language_dropdown'] = render(module_invoke('lang_dropdown', 'block_view', 'language')['content']);
        // Get social media links.
        $defaults_blocks['social_media'] = render(module_invoke('social_media_links', 'block_view', 'social-media-links')['content']);

        $block['content'] = theme('vh_variant6', array(
          'config'          => $config,
          'defaults_blocks' => $defaults_blocks,
          'custom_blocks'   => $custom_blocks,
        ));

        // Add CSS & JS.
        drupal_add_js($path . "/js/variant6.js", array('scope' => 'footer'));
      }

      break;
    case 'search_icon':
      $block['subject'] = '';
      $block['content'] = theme('vh_search_icon');
      break;
  }

  return $block;
}

/**
 * Implements theme_preprocess_html().
 */
function vactory_header_preprocess_html(&$vars) {

  $default_value = variable_get(_vactory_header_get_variable_name(), array(
    'vactory_header' => array('template' => 1)
  ));

  $template_id = $default_value['vactory_header']['template'];

  $vars['classes_array'][] = drupal_clean_css_identifier("vh-variant" . $template_id);
}

/**
 * Implements hook_form_alter().
 */
function vactory_header_form_alter(&$form, &$form_state, $form_id) {

  $default_value = variable_get(_vactory_header_get_variable_name(), array(
    'vactory_header' => array('template' => 1)
  ));

  $template_id = $default_value['vactory_header']['template'];

  if ($template_id == 4 && $form_id == 'lang_dropdown_form' && isset($form['lang_dropdown_select'])) {
    $form['lang_dropdown_select']['#attributes']['class'][] .= 'cs-select';
    $form['lang_dropdown_select']['#attributes']['class'][] .= 'cs-skin-elastic';
  }

}

/**
 * Set Blocks weight.
 */
function _vactory_header_set_blocks(&$blocks, $region) {
  // Set blocks weight.
  foreach ($blocks as $key => &$block) {
    $block_weight = variable_get('vactory_header_enabled_disabled_blocks_' . $region . '_weight' . $block['bid']);
    if (isset($block_weight)) {
      $block['weight'] = $block_weight;
    }
  }
  // Sort blocks by weight.
  _vactory_header_sort_blocks($blocks);
  return $blocks;
}

/**
 * Sort Blocks by weight.
 */
function _vactory_header_sort_blocks(&$blocks) {
  // Sort blocks by weight.
  usort($blocks, function ($block1, $block2) {
    if ($block1['weight'] == $block2['weight']) {
      return 0;
    }
    return $block1['weight'] < $block2['weight'] ? -1 : 1;
  });
}

/**
 * Render block.
 */
function _vactory_render_block($block) {
  $render_block = _block_render_blocks(array($block));
  $get_render = _block_get_renderable_array($render_block);

  return drupal_render($get_render);
}