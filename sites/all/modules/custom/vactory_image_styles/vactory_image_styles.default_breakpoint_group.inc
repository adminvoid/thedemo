<?php
/**
 * @file
 * vactory_image_styles.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function vactory_image_styles_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'vactory_breakpoints_group';
  $breakpoint_group->name = 'Vactory Breakpoints group';
  $breakpoint_group->breakpoints = array(
    0 => 'custom.user.mobile',
    1 => 'custom.user.narrow',
    2 => 'custom.user.medium',
    3 => 'custom.user.large',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['vactory_breakpoints_group'] = $breakpoint_group;

  return $export;
}
