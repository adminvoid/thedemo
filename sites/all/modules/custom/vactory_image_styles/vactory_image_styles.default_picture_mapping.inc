<?php
/**
 * @file
 * vactory_image_styles.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function vactory_image_styles_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Articles Thumbnail Wide';
  $picture_mapping->machine_name = 'articles_thumbnail_wide';
  $picture_mapping->breakpoint_group = 'vactory_breakpoints_group';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_mobile_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_mobile_2x',
      ),
    ),
    'custom.user.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_narrow_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_narrow_2x',
      ),
    ),
    'custom.user.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_medium_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_medium_2x',
      ),
    ),
    'custom.user.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_large_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticles_widecustom_user_large_2x',
      ),
    ),
  );
  $export['articles_thumbnail_wide'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Vactory image large';
  $picture_mapping->machine_name = 'vactory_image_large';
  $picture_mapping->breakpoint_group = 'vactory_breakpoints_group';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_mobile_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_large_2x',
      ),
    ),
    'custom.user.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_narrow_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_narrow_2x',
      ),
    ),
    'custom.user.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_medium_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_medium_2x',
      ),
    ),
    'custom.user.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_large_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_largecustom_user_large_2x',
      ),
    ),
  );
  $export['vactory_image_large'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Vactory image medium';
  $picture_mapping->machine_name = 'vactory_image_medium';
  $picture_mapping->breakpoint_group = 'vactory_breakpoints_group';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_mobile_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_mobile_2x',
      ),
    ),
    'custom.user.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_narrow_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_narrow_2x',
      ),
    ),
    'custom.user.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_medium_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_medium_2x',
      ),
    ),
    'custom.user.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_large_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'vactory_image_mediumcustom_user_large_2x',
      ),
    ),
  );
  $export['vactory_image_medium'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Vactory Node Wide';
  $picture_mapping->machine_name = 'vactory_node_wide';
  $picture_mapping->breakpoint_group = 'vactory_breakpoints_group';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_mobile_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_mobile_2x',
      ),
    ),
    'custom.user.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_narrow_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_narrow_2x',
      ),
    ),
    'custom.user.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_medium_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_medium_2x',
      ),
    ),
    'custom.user.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_large_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'node-wide-fullcustom_user_large_2x',
      ),
    ),
  );
  $export['vactory_node_wide'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Articles Thumbnail';
  $picture_mapping->machine_name = 'varticles_thumbnail';
  $picture_mapping->breakpoint_group = 'vactory_breakpoints_group';
  $picture_mapping->mapping = array(
    'custom.user.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_mobile_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_mobile_2x',
      ),
    ),
    'custom.user.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_narrow_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_narrow_2x',
      ),
    ),
    'custom.user.medium' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_medium_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_medium_2x',
      ),
    ),
    'custom.user.large' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_large_1x',
      ),
      '2x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'varticlescustom_user_large_2x',
      ),
    ),
  );
  $export['varticles_thumbnail'] = $picture_mapping;

  return $export;
}
