<?php
/**
 * @file
 * vactory_image_styles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_image_styles_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function vactory_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: node-wide-fullcustom_user_large_1x.
  $styles['node-wide-fullcustom_user_large_1x'] = array(
    'label' => 'node-wide-fullcustom_user_large_1x',
    'effects' => array(
      30 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_large_2x.
  $styles['node-wide-fullcustom_user_large_2x'] = array(
    'label' => 'node-wide-fullcustom_user_large_2x',
    'effects' => array(
      29 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_medium_1x.
  $styles['node-wide-fullcustom_user_medium_1x'] = array(
    'label' => 'node-wide-fullcustom_user_medium_1x',
    'effects' => array(
      28 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_medium_2x.
  $styles['node-wide-fullcustom_user_medium_2x'] = array(
    'label' => 'node-wide-fullcustom_user_medium_2x',
    'effects' => array(
      27 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_mobile_1x.
  $styles['node-wide-fullcustom_user_mobile_1x'] = array(
    'label' => 'node-wide-fullcustom_user_mobile_1x',
    'effects' => array(
      1 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_mobile_2x.
  $styles['node-wide-fullcustom_user_mobile_2x'] = array(
    'label' => 'node-wide-fullcustom_user_mobile_2x',
    'effects' => array(
      23 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_narrow_1x.
  $styles['node-wide-fullcustom_user_narrow_1x'] = array(
    'label' => 'node-wide-fullcustom_user_narrow_1x',
    'effects' => array(
      26 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node-wide-fullcustom_user_narrow_2x.
  $styles['node-wide-fullcustom_user_narrow_2x'] = array(
    'label' => 'node-wide-fullcustom_user_narrow_2x',
    'effects' => array(
      25 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_article_thumbnail_1x.
  $styles['vactory_article_thumbnail_1x'] = array(
    'label' => 'Vactory Article Thumbnail 1x',
    'effects' => array(
      1 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_article_thumbnail_2x.
  $styles['vactory_article_thumbnail_2x'] = array(
    'label' => 'Vactory Article Thumbnail 2x',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1536,
          'height' => 864,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_article_thumbnail_wide_1x.
  $styles['vactory_article_thumbnail_wide_1x'] = array(
    'label' => 'Vactory Article Thumbnail Wide 1x',
    'effects' => array(
      11 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_article_thumbnail_wide_x2.
  $styles['vactory_article_thumbnail_wide_x2'] = array(
    'label' => 'Vactory Article Thumbnail Wide x2',
    'effects' => array(
      12 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 2400,
          'height' => 1350,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_large_1x.
  $styles['vactory_image_large_1x'] = array(
    'label' => 'Vactory image large 1x',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_large_2x.
  $styles['vactory_image_large_2x'] = array(
    'label' => 'Vactory image large 2x',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1600,
          'height' => 800,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_large_1x.
  $styles['vactory_image_largecustom_user_large_1x'] = array(
    'label' => 'vactory_image_largecustom_user_large_1x',
    'effects' => array(
      13 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_large_2x.
  $styles['vactory_image_largecustom_user_large_2x'] = array(
    'label' => 'vactory_image_largecustom_user_large_2x',
    'effects' => array(
      12 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_medium_1x.
  $styles['vactory_image_largecustom_user_medium_1x'] = array(
    'label' => 'vactory_image_largecustom_user_medium_1x',
    'effects' => array(
      11 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_medium_2x.
  $styles['vactory_image_largecustom_user_medium_2x'] = array(
    'label' => 'vactory_image_largecustom_user_medium_2x',
    'effects' => array(
      10 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_mobile_1x.
  $styles['vactory_image_largecustom_user_mobile_1x'] = array(
    'label' => 'vactory_image_largecustom_user_mobile_1x',
    'effects' => array(
      7 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_mobile_2x.
  $styles['vactory_image_largecustom_user_mobile_2x'] = array(
    'label' => 'vactory_image_largecustom_user_mobile_2x',
    'effects' => array(
      6 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_narrow_1x.
  $styles['vactory_image_largecustom_user_narrow_1x'] = array(
    'label' => 'vactory_image_largecustom_user_narrow_1x',
    'effects' => array(
      9 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_largecustom_user_narrow_2x.
  $styles['vactory_image_largecustom_user_narrow_2x'] = array(
    'label' => 'vactory_image_largecustom_user_narrow_2x',
    'effects' => array(
      8 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_medium_1x.
  $styles['vactory_image_medium_1x'] = array(
    'label' => 'Vactory image medium 1x',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_medium_2x.
  $styles['vactory_image_medium_2x'] = array(
    'label' => 'Vactory image medium 2x',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 760,
          'height' => 560,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_large_1x.
  $styles['vactory_image_mediumcustom_user_large_1x'] = array(
    'label' => 'vactory_image_mediumcustom_user_large_1x',
    'effects' => array(
      21 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_large_2x.
  $styles['vactory_image_mediumcustom_user_large_2x'] = array(
    'label' => 'vactory_image_mediumcustom_user_large_2x',
    'effects' => array(
      20 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_medium_1x.
  $styles['vactory_image_mediumcustom_user_medium_1x'] = array(
    'label' => 'vactory_image_mediumcustom_user_medium_1x',
    'effects' => array(
      19 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_medium_2x.
  $styles['vactory_image_mediumcustom_user_medium_2x'] = array(
    'label' => 'vactory_image_mediumcustom_user_medium_2x',
    'effects' => array(
      18 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_mobile_1x.
  $styles['vactory_image_mediumcustom_user_mobile_1x'] = array(
    'label' => 'vactory_image_mediumcustom_user_mobile_1x',
    'effects' => array(
      15 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_mobile_2x.
  $styles['vactory_image_mediumcustom_user_mobile_2x'] = array(
    'label' => 'vactory_image_mediumcustom_user_mobile_2x',
    'effects' => array(
      14 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_narrow_1x.
  $styles['vactory_image_mediumcustom_user_narrow_1x'] = array(
    'label' => 'vactory_image_mediumcustom_user_narrow_1x',
    'effects' => array(
      17 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_image_mediumcustom_user_narrow_2x.
  $styles['vactory_image_mediumcustom_user_narrow_2x'] = array(
    'label' => 'vactory_image_mediumcustom_user_narrow_2x',
    'effects' => array(
      16 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 380,
          'height' => 280,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_partner_logo.
  $styles['vactory_partner_logo'] = array(
    'label' => 'Logo Partner',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => '',
          'height' => 100,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_wide_full_x1.
  $styles['vactory_wide_full_x1'] = array(
    'label' => 'Vactory Wide Full x1',
    'effects' => array(
      21 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vactory_wide_full_x2.
  $styles['vactory_wide_full_x2'] = array(
    'label' => 'Vactory Wide Full x2',
    'effects' => array(
      22 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 2400,
          'height' => 1350,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_large_1x.
  $styles['varticles_widecustom_user_large_1x'] = array(
    'label' => 'varticles_widecustom_user_large_1x',
    'effects' => array(
      20 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_large_2x.
  $styles['varticles_widecustom_user_large_2x'] = array(
    'label' => 'varticles_widecustom_user_large_2x',
    'effects' => array(
      19 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 2400,
          'height' => 1350,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_medium_1x.
  $styles['varticles_widecustom_user_medium_1x'] = array(
    'label' => 'varticles_widecustom_user_medium_1x',
    'effects' => array(
      18 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_medium_2x.
  $styles['varticles_widecustom_user_medium_2x'] = array(
    'label' => 'varticles_widecustom_user_medium_2x',
    'effects' => array(
      17 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_mobile_1x.
  $styles['varticles_widecustom_user_mobile_1x'] = array(
    'label' => 'varticles_widecustom_user_mobile_1x',
    'effects' => array(
      14 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_mobile_2x.
  $styles['varticles_widecustom_user_mobile_2x'] = array(
    'label' => 'varticles_widecustom_user_mobile_2x',
    'effects' => array(
      13 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_narrow_1x.
  $styles['varticles_widecustom_user_narrow_1x'] = array(
    'label' => 'varticles_widecustom_user_narrow_1x',
    'effects' => array(
      16 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticles_widecustom_user_narrow_2x.
  $styles['varticles_widecustom_user_narrow_2x'] = array(
    'label' => 'varticles_widecustom_user_narrow_2x',
    'effects' => array(
      15 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 675,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_large_1x.
  $styles['varticlescustom_user_large_1x'] = array(
    'label' => 'varticlescustom_user_large_1x',
    'effects' => array(
      10 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_large_2x.
  $styles['varticlescustom_user_large_2x'] = array(
    'label' => 'varticlescustom_user_large_2x',
    'effects' => array(
      9 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_medium_1x.
  $styles['varticlescustom_user_medium_1x'] = array(
    'label' => 'varticlescustom_user_medium_1x',
    'effects' => array(
      8 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_medium_2x.
  $styles['varticlescustom_user_medium_2x'] = array(
    'label' => 'varticlescustom_user_medium_2x',
    'effects' => array(
      7 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_mobile_1x.
  $styles['varticlescustom_user_mobile_1x'] = array(
    'label' => 'varticlescustom_user_mobile_1x',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_mobile_2x.
  $styles['varticlescustom_user_mobile_2x'] = array(
    'label' => 'varticlescustom_user_mobile_2x',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_narrow_1x.
  $styles['varticlescustom_user_narrow_1x'] = array(
    'label' => 'varticlescustom_user_narrow_1x',
    'effects' => array(
      6 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: varticlescustom_user_narrow_2x.
  $styles['varticlescustom_user_narrow_2x'] = array(
    'label' => 'varticlescustom_user_narrow_2x',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 432,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
