<div class="timeline-item">
  <div class="timeline-icon top-timeline-icon"></div>
  <div class="date-wrapper  top-date"><?php print $date; ?></div>
  <div class="timeline-content ">
    <?php if ($image): ?>
      <div class="image-wrapper"><?php print $image; ?></div>
    <?php endif; ?>
    <?php if ($title) : ?>
      <h2><?php print $title; ?></h2>
    <?php endif; ?>
    <?php if ($subtitle): ?>
      <?php print $subtitle; ?>
    <?php endif; ?>
    <?php if ($cta) : ?>
      <span class="timeline-cta">
        <?php print $cta; ?>
      </span>
    <?php endif ?>
  </div>
  <div class="date-wrapper  bottom-date">
    <?php print $date; ?>
  </div>

  <div class="timeline-icon bottom-timeline-icon">
  </div>
</div>

