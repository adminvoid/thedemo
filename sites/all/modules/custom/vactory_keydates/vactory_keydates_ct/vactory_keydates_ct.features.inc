<?php
/**
 * @file
 * vactory_keydates_ct.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_keydates_ct_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function vactory_keydates_ct_node_info() {
  $items = array(
    'keydates' => array(
      'name' => t('Key dates'),
      'base' => 'node_content',
      'description' => t('Test content type for Key dates formatter.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
