<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<article
  class="article-card standing-layout <?php print $classes; ?>"<?php print $attributes; ?>>

  <div class="article-card__thumbnail">
    <a
      href="<?php print $node_url; ?>">
      <?php print v_gen_render_image($visual, $picture_name = 'articles_thumbnail_wide', $image_style_fallback = 'varticles_widecustom_user_large_1x'); ?>
    </a>

  </div>

  <div class="article-card__content">


    <?php if (!empty($title)): ?>
      <h3 class="article-card__title"<?php print $title_attributes; ?>><a
          href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
    <?php endif; ?>

    <div class="inline-blocks">
      <?php if (isset($content['field_vactory_news_theme'])): ?>
        <div
          class="article-card__tags"><?php print render($content['field_vactory_news_theme']); ?></div>
      <?php endif; ?>
      <?php if (isset($content['field_vactory_date'])): ?>
        <div
          class="article-card__date"><?php print render($content['field_vactory_date']); ?></div>
      <?php endif; ?>
    </div>

    <?php if (isset($content['field_vactory_chapo'])): ?>
      <div
        class="article-card__excerpt">
        <?php print render($content['field_vactory_chapo']); ?>
      </div>
    <?php endif; ?>

    <div
      class="article-card__permalink">
      <a class="permalink"
         href="<?php print $node_url; ?>"><?php print t('Read more'); ?></a>
    </div>


  </div>

</article>