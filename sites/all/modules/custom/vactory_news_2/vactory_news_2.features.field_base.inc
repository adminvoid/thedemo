<?php
/**
 * @file
 * vactory_news_2.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function vactory_news_2_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_vactory_news_theme'.
  $field_bases['field_vactory_news_theme'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_vactory_news_theme',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'vactory_news_theme',
          'parent' => 0,
        ),
      ),
      'options_list_callback' => 'i18n_taxonomy_allowed_values',
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
