<?php
/**
 * @file
 * vactory_paragraphs_container.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function vactory_paragraphs_container_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_css'.
  $field_bases['field_css'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_css',
    'indexes' => array(
      'css' => array(
        0 => 'css',
      ),
    ),
    'locked' => 0,
    'module' => 'vactory_paragraphs_css',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'vactory_paragraphs_css',
  );

  // Exported field_base: 'field_v_paragraphs'.
  $field_bases['field_v_paragraphs'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_v_paragraphs',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'paragraphs',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'paragraphs',
  );

  return $field_bases;
}
