<?php
/**
 * @file
 * vactory_paragraphs_container.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function vactory_paragraphs_container_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'paragraphs_item-paragraph_container-field_css'.
  $field_instances['paragraphs_item-paragraph_container-field_css'] = array(
    'bundle' => 'paragraph_container',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'CSS Classes: container, container-fluid, stacked-container, marge-haut, marge-bas',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_css',
    'label' => 'CSS',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'vactory_paragraphs_css',
      'settings' => array(),
      'type' => 'vactory_paragraphs_css_text_widget',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-paragraph_container-field_v_paragraphs'.
  $field_instances['paragraphs_item-paragraph_container-field_v_paragraphs'] = array(
    'bundle' => 'paragraph_container',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'field_multiple_limit' => -1,
          'field_multiple_limit_offset' => 0,
          'field_multiple_limit_order' => 0,
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_v_paragraphs',
    'label' => 'Paragraphs',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'paragraph_container' => -1,
        'paragraphs_background' => -1,
        'paragraphs_block' => -1,
        'paragraphs_pack_content' => -1,
        'paragraphs_pack_node_list' => -1,
        'paragraphs_slider' => -1,
      ),
      'bundle_weights' => array(
        'paragraph_container' => 7,
        'paragraphs_background' => 2,
        'paragraphs_block' => 3,
        'paragraphs_pack_content' => 4,
        'paragraphs_pack_node_list' => 5,
        'paragraphs_slider' => 6,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Paragraph',
      'title_multiple' => 'Paragraphs',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('CSS');
  t('CSS Classes: container, container-fluid, stacked-container, marge-haut, marge-bas');
  t('Paragraphs');

  return $field_instances;
}
