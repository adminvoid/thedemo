<?php
/**
 * @file
 * vactory_paragraphs_container.features.inc
 */

/**
 * Implements hook_paragraphs_info().
 */
function vactory_paragraphs_container_paragraphs_info() {
  $items = array(
    'paragraph_container' => array(
      'name' => 'Paragraph Container',
      'bundle' => 'paragraph_container',
      'locked' => '1',
    ),
  );
  return $items;
}
