<?php
/**
 * @file
 * vactory_paragraphs_container.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vactory_paragraphs_container_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create paragraph content paragraph_container'.
  $permissions['create paragraph content paragraph_container'] = array(
    'name' => 'create paragraph content paragraph_container',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'delete paragraph content paragraph_container'.
  $permissions['delete paragraph content paragraph_container'] = array(
    'name' => 'delete paragraph content paragraph_container',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'update paragraph content paragraph_container'.
  $permissions['update paragraph content paragraph_container'] = array(
    'name' => 'update paragraph content paragraph_container',
    'roles' => array(
      'administrator' => 'administrator',
      'contributor' => 'contributor',
      'master' => 'master',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  // Exported permission: 'view paragraph content paragraph_container'.
  $permissions['view paragraph content paragraph_container'] = array(
    'name' => 'view paragraph content paragraph_container',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'paragraphs_bundle_permissions',
  );

  return $permissions;
}
