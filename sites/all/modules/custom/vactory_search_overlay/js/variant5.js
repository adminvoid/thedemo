/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2016, Codrops
 * http://www.codrops.com
 */
;(function ($) {

    'use strict';

    var mainContainer = $('.main-wrap'),
        openCtrl = $('.btn-search-overlay'),
        closeCtrl = $('#btn-search-overlay-close'),
        searchContainer = $('.search-overlay'),
        inputSearch = searchContainer.find('.search__input'),
        lastFocusedElement;

    function init() {
        initEvents();
    }

    function initEvents() {
        openCtrl.on('click', openSearch);
        closeCtrl.on('click', closeSearch);
        $(document).on('keyup', function(ev) {
            // Escape key.
            if( ev.keyCode == 27 ) {
                closeSearch();
            }
        });
    }

    function openSearch() {
        lastFocusedElement = document.activeElement;
        mainContainer.addClass('main-wrap--move');
        searchContainer.addClass('search--open');
        setTimeout(function() {
            inputSearch.focus();
        }, 600);
    }

    function closeSearch() {
        mainContainer.removeClass('main-wrap--move');
        searchContainer.removeClass('search--open');
        inputSearch.blur();
        inputSearch.value = '';
        if (lastFocusedElement) { // restore focus
            lastFocusedElement.focus();
        }
    }

    init();

})(jQuery);