<?php

/**
 * @file
 * Radix theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<svg class="hidden">
  <defs>
    <symbol id="icon-cross" viewBox="0 0 24 24">
      <title>cross</title>
      <path
        d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/>
    </symbol>
  </defs>
</svg>

<div class="search-overlay">
  <button id="btn-search-overlay-close" class="btn btn--search-close"
          aria-label="<?php print t('Close search form'); ?>"
          accept-charset="UTF-8" role="form">
    <svg class="icon icon--cross">
      <use xlink:href="#icon-cross"></use>
    </svg>
  </button>
  <form class="search__form" id="<?php print $form['#id']; ?>"
        action="<?php print $form['#action']; ?>"
        method="<?php print $form['#method']; ?>">
    <input class="search__input" name="<?php print $input_name; ?>"
           id="<?php print $form['search_block_form']['#id']; ?>" type="search"
           placeholder="" autocomplete="off" autocorrect="off"
           autocapitalize="off" spellcheck="false"
           title="<?php print t('@search_form_input_title', array('@search_form_input_title' => $form['search_block_form']['#attributes']['title'])); ?>"/>
    <span
      class="search__info"><?php print t('Hit enter to search or ESC to close'); ?></span>
      <!--
    <input type="hidden" name="form_build_id"
           value="<?php print $form['#build_id']; ?>">
    <input type="hidden" name="form_id"
           value="<?php print $form['#form_id']; ?>">
    <?php if (isset($form['form_token'])): ?>
    <input type="hidden" name="form_token"
           value="<?php print $form['form_token']['#default_value']; ?>">
    <?php endif; ?>
    <input type="hidden" id="edit-submit" name="op" value="Search"
           class="form-submit btn btn-default btn-primary"> -->
  </form>

</div><!-- /search-overlay -->
