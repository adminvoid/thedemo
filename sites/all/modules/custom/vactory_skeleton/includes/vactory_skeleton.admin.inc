<?php

/**
 * @file
 * Admin configuration file of vactory_skeleton module.
 */

/**
 * Inits vactory_skeleton_admin form.
 */
function vactory_skeleton_admin_settings($form, $form_state) {

  global $theme;
  // Load all available Drupal Blocks.
  $blocks = _block_rehash($theme);
  $weight_delta = count($blocks);
  // Build form.
  $form['vactory_skeleton'] = array(
    '#type' => 'fieldset',
    '#title' => t('Skeleton configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['vactory_skeleton']['regions_blocks'] = array('#type' => 'vertical_tabs');

  $i = 0;
  foreach (_vactory_skeleton_get_enabled_regions() as $region => $region_value) {
    // $fieldset[$region] = array(
    //      '#type' => 'fieldset',
    //      '#title' => $region_value,
    //      '#collapsible' => TRUE,
    //      '#collapsed' => TRUE,
    //      '#group' => 'regions_blocks',
    //    );.
    $fieldset[$region]['enabled_disabled_blocks'] = array(
      '#type' => 'fieldset',
      '#title' => t('Enabled / Disabled Blocks in') . ' ' . $region_value . ' ' . t('region'),
      '#description' => t('The region content will be generated from the list of "enabled" blocks. Re-order the list to change the priority of each blocks.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'regions_blocks',
      '#weight' => $i,
    );

    // Orderable list of block selections.
    $fieldset[$region]['enabled_disabled_blocks']['enabled_disabled_blocks_' . $region] = array(
      '#tree' => TRUE,
      '#region' => $region,
      '#theme' => 'vactory_skeleton_blocks_table',

    );

    _vactory_skeleton_set_blocks($blocks, $region);
    foreach ($blocks as $id => $block) {
      // Load block titles.
      $title = !empty($block['info']) ? $block['info'] : $block['delta'];

      // Ensure that regex patterns do not cause invalid id attributes.
      $safe_id_prefix = 'edit-vactory-skeleton-block-' . $block['bid'];

      $fieldset[$region]['enabled_disabled_blocks']['enabled_disabled_blocks_' . $region][$block['bid']] = array(
        'enabled' => array(
          '#type' => 'checkbox',
          '#id' => $safe_id_prefix . '-enabled',
          '#title' => '',
          '#default_value' => variable_get('vactory_skeleton_enabled_disabled_blocks_' . $region . $block['bid']),
        ),
        'label' => array(
          '#value' => $block['info'],
        ),
        'weight' => array(
          '#type' => 'weight',
          '#default_value' => $block['weight'],
          '#delta' => $weight_delta,
          '#id' => $safe_id_prefix . '-weight-wrapper',
        ),
        'module' => array(
          '#type' => 'value',
          '#value' => $block['module'],
        ),
        'title' => array(
          '#type' => 'value',
          '#value' => $title,
        ),
        'title_display' => array(
          '#type' => 'markup',
          '#markup' => check_plain($title),
        ),
        'bid' => array(
          '#type' => 'value',
          '#value' => $block['bid'],
        ),
        'delta' => array(
          '#type' => 'value',
          '#value' => $block['delta'],
        ),
      );
    }
    $form['vactory_skeleton'][$region] = $fieldset[$region];
    $i++;
  }
  $form['#submit'][] = 'vactory_skeleton_admin_settings_submit';
  return system_settings_form($form);
}

/**
 * Submit function for vactory_skeleton_admin form.
 */
function vactory_skeleton_admin_settings_submit($form, $form_state) {
  foreach (_vactory_skeleton_get_enabled_regions() as $region => $region_value) {
    $enabled_blocks = array();
    $blocks = $form_state['values']['enabled_disabled_blocks_' . $region];
    _vactory_skeleton_sort_blocks($blocks);
    foreach ($blocks as $block) {
      variable_set('vactory_skeleton_enabled_disabled_blocks_' . $region . $block['bid'], $block['enabled']);
      variable_set('vactory_skeleton_enabled_disabled_blocks_' . $region . '_weight' . $block['bid'], $block['weight']);
      if ($block['enabled'] == 1) {
        $enabled_blocks[] = $block;
      }
    }
    variable_set('vactory_skeleton_enabled_blocks_' . $region, $enabled_blocks);
  }

}
