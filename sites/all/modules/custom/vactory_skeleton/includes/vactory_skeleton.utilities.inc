<?php

/**
 * @file
 * Utilities functions of vactory_skeleton module.
 */

/**
 * List of enabled regions.
 */
function _vactory_skeleton_get_enabled_regions() {
  return array('top' => 'Top', 'header' => 'Header', 'bridge' => 'Bridge', 'footer' => 'Footer', 'bottom' => 'Bottom');
}

/**
 * Set Blocks weight.
 */
function _vactory_skeleton_set_blocks(&$blocks, $region) {
  // Set blocks weight.
  foreach ($blocks as $key => &$block) {
    $block_weight = variable_get('vactory_skeleton_enabled_disabled_blocks_' . $region . '_weight' . $block['bid']);
    if (isset($block_weight)) {
      $block['weight'] = $block_weight;
    }
  }
  // Sort blocks by weight.
  _vactory_skeleton_sort_blocks($blocks);
  return $blocks;
}

/**
 * Sort Blocks by weight.
 */
function _vactory_skeleton_sort_blocks(&$blocks) {
  // Sort blocks by weight.
  usort($blocks, function ($block1, $block2) {
    if ($block1['weight'] == $block2['weight']) {
      return 0;
    }
    return $block1['weight'] < $block2['weight'] ? -1 : 1;
  });
}
