<?php

/**
 * @file
 * vactory_skeleton.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function vactory_skeleton_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
