<?php

/**
 * @file
 * vactory_skeleton.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function vactory_skeleton_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer vactory skeleton'.
  $permissions['administer vactory skeleton'] = array(
    'name' => 'administer vactory skeleton',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'vactory_skeleton',
  );

  return $permissions;
}
