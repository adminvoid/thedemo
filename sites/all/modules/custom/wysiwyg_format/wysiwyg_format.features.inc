<?php
/**
 * @file
 * wysiwyg_format.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wysiwyg_format_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
}
