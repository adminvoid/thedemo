<?php
/**
 * @file
 * wysiwyg_format.linkit_profiles.inc
 */

/**
 * Implements hook_default_linkit_profiles().
 */
function wysiwyg_format_default_linkit_profiles() {
  $export = array();

  $linkit_profile = new LinkitProfile();
  $linkit_profile->disabled = FALSE; /* Edit this to true to make a default linkit_profile disabled initially */
  $linkit_profile->api_version = 1;
  $linkit_profile->name = 'wysiwyg';
  $linkit_profile->admin_title = 'WYSIWYG';
  $linkit_profile->admin_description = '';
  $linkit_profile->profile_type = '1';
  $linkit_profile->data = array(
    'text_formats' => array(
      'filtered_html' => 'filtered_html',
      'full_html' => 'full_html',
      'mailchimp_campaign' => 0,
      'plain_text' => 0,
    ),
    'search_plugins' => array(
      'entity:taxonomy_term' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:vactory_highlights' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:node' => array(
        'enabled' => 1,
        'weight' => '-10',
      ),
      'entity:vactory_map_item' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:comment' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:vactory_slider' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:file' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:entityform_type' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'entity:user' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'entity:comment' => array(
      'result_description' => '',
      'bundles' => array(
        'comment_node_accordion' => 0,
        'comment_node_article' => 0,
        'comment_node_banner' => 0,
        'comment_node_page' => 0,
        'comment_node_vactory_highlights_block' => 0,
        'comment_node_vactory_media' => 0,
        'comment_node_vactory_news' => 0,
        'comment_node_vactory_page' => 0,
        'comment_node_vactory_partner' => 0,
        'comment_node_vactory_slider_type' => 0,
        'comment_node_vactory_blog_entry' => 0,
        'comment_node_vactory_gen_type' => 0,
        'comment_node_webform' => 0,
      ),
      'group_by_bundle' => 0,
    ),
    'entity:vactory_highlights' => array(
      'result_description' => '',
    ),
    'entity:vactory_map_item' => array(
      'result_description' => '',
    ),
    'entity:vactory_slider' => array(
      'result_description' => '',
    ),
    'entity:entityform_type' => array(
      'result_description' => '',
    ),
    'entity:node' => array(
      'result_description' => '',
      'bundles' => array(
        'accordion' => 0,
        'article' => 0,
        'banner' => 0,
        'page' => 0,
        'vactory_highlights_block' => 0,
        'vactory_media' => 0,
        'vactory_news' => 0,
        'vactory_page' => 0,
        'vactory_partner' => 0,
        'vactory_slider_type' => 0,
        'vactory_blog_entry' => 0,
        'vactory_gen_type' => 0,
        'webform' => 0,
      ),
      'group_by_bundle' => 1,
      'include_unpublished' => 0,
    ),
    'entity:file' => array(
      'result_description' => '',
      'bundles' => array(
        'image' => 0,
        'video' => 0,
        'audio' => 0,
        'document' => 0,
      ),
      'group_by_bundle' => 0,
      'show_scheme' => 0,
      'group_by_scheme' => 0,
      'url_type' => 'entity',
      'image_extra_info' => array(
        'thumbnail' => 'thumbnail',
        'dimensions' => 'dimensions',
      ),
    ),
    'entity:taxonomy_term' => array(
      'result_description' => '',
      'bundles' => array(
        'tags' => 0,
        'vactory_news_theme' => 0,
        'vactory_blog_author' => 0,
        'vactory_blog_taxo' => 0,
        'vactory_media_theme' => 0,
        'vactory_media_type' => 0,
        'profil_contact_' => 0,
        'citys' => 0,
        'country' => 0,
        'type' => 0,
      ),
      'group_by_bundle' => 0,
    ),
    'entity:user' => array(
      'result_description' => '',
    ),
    'insert_plugin' => array(
      'url_method' => '2',
    ),
    'attribute_plugins' => array(
      'accesskey' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'target' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'class' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'rel' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'id' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
      'title' => array(
        'enabled' => 0,
        'weight' => '-10',
      ),
    ),
    'autocomplete' => array(
      'charLimit' => '2',
      'wait' => '350',
      'remoteTimeout' => '10000',
    ),
  );
  $export['wysiwyg'] = $linkit_profile;

  return $export;
}
