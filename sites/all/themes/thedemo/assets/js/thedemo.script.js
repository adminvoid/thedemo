/**
 * @file
 * Custom scripts for theme.
 */

// @todo: Add viewport example
// @todo: Add Variables example.
// @todo: update nomcloture.

(function ($, Drupal, viewport, window, document, undefined) {

    //== Scaffolding
    //
    //## Settings for some of the most global objects.
    Drupal.vactory = Drupal.vactory || {};
    Drupal.vactory.utility = Drupal.vactory.utility || {};
    Drupal.vars = Drupal.vars || {};
    Drupal.vars.vactory = Drupal.vars.vactory || {};

    //== Variables
    //
    //## Global variables
    Drupal.vars.vactory = {
        lang: $('html').attr('xml:lang'),
        is_rtl: ($('html[dir="rtl"]').length) ? true : false
    };

    //== Datepicker
    //
    //## Apply datepicker
    Drupal.behaviors.vactory_datePicker = {
        attach: function () {
            $('.datepicker').datepicker({
                language: Drupal.vars.vactory.lang,
                disableTouchKeyboard: true
            });
        }
    };

    //== Custom Select
    //
    //## Apply custom select
    Drupal.behaviors.vactory_customSelect = {
        attach: function () {
            $.each($('select:not([multiple="multiple"]), #lang-dropdown-select-language'), function (i, el) {
                $(el).selectpicker();
            });
        }
    };

    //== Custom Select Multiple
    //
    //## Apply multiple select
    Drupal.behaviors.selectMultiple = {
        attach: function () {
            // Skin Select
            $.each($('select[multiple="multiple"]'), function (i, el) {
                $(el).multiselect({
                    nonSelectedText: Drupal.t('Selectionner...')
                });
            });
        }
    };

    // Show or hide the sticky footer button
    Drupal.vactory.utility.gotoStickyButton = function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('.go-top').fadeIn(200);
            } else {
                $('.go-top').fadeOut(200);
            }
        });

        // Animate the scroll to top.
        $('.go-top').click(function (event) {
            event.preventDefault();
            $('html, body').animate({scrollTop: 0}, 300);
        })
    };

    // Views Filter
    Drupal.vactory.utility.viewsFilter = function () {
        // Mobile views filters.
        // Add control panel button
        // Show filters in a modal
        // Only appear if there is content.
        if (!$('.view-empty').length) {
            var _m_control_panel_button = $('<button class="js btn btn-sm btn-primary btn-control-panel"><i class="icon-control-panel filter-gear-icon"></i></button>');
            var _m_control_panel_button_wrap_text_side = Drupal.vars.vactory.is_rtl ? 'text-left' : 'text-right';
            _m_control_panel_button.insertAfter('.view .view-filters');
            _m_control_panel_button.wrap('<div class="visible-xs-block ' + _m_control_panel_button_wrap_text_side + '"></div>');

            _m_control_panel_button.on('click', function () {
                $.fancybox.open({
                    src: '.view .view-filters',
                    type: 'inline',
                    opts: {
                        animationDuration: 200,
                        animationEffect: 'material',
                        modal: true
                    }
                });
            });

            // Add help text.
            $('.view .view-filters').prepend('<h4 class="visible-xs-block">' + Drupal.t("Filter") + '</h4>');
        }
    };

    //== Views Date Filter
    //
    //## Apply datepicker > Months view mode.
    Drupal.vactory.utility.viewsDateFilter = function () {
        $('input[name*="date_filter_field_vactory_date"]').each(function () {
            $(this).datepicker('destroy');
            $(this).datepicker({
                language: Drupal.vars.vactory.lang,
                disableTouchKeyboard: true,
                format: "mm/yyyy",
                startView: 1,
                minViewMode: 1,
                autoclose: true
            });
        });
    };

    //== Webform validation
    //
    //## Apply validation to webforms.
    Drupal.behaviors.vactory_formValidation = {
        attach: function () {

            // Form.
            var $_forms = $('.node-webform');

            // Update JQuery plugin
            jQuery.extend(jQuery.validator.messages, {
                // required:Drupal.t("This field is required."),
                remote: Drupal.t("Please fix this field."),
                email: Drupal.t("Please enter a valid email address."),
                url: Drupal.t("Please enter a valid URL."),
                date: Drupal.t("Please enter a valid date."),
                dateISO: Drupal.t("Please enter a valid date (ISO)."),
                number: Drupal.t("Please enter a valid number."),
                digits: Drupal.t("Please enter only digits."),
                creditcard: Drupal.t("Please enter a valid credit card number."),
                equalTo: Drupal.t("Please enter the same value again."),
                accept: Drupal.t("Please enter a value with a valid extension."),
                maxlength: jQuery.validator.format(Drupal.t("Please enter no more than {0} characters.")),
                minlength: jQuery.validator.format(Drupal.t("Please enter at least {0} characters.")),
                rangelength: jQuery.validator.format(Drupal.t("Please enter a value between {0} and {1} characters long.")),
                range: jQuery.validator.format(Drupal.t("Please enter a value between {0} and {1}.")),
                max: jQuery.validator.format(Drupal.t("Please enter a value less than or equal to {0}.")),
                min: jQuery.validator.format(Drupal.t("Please enter a value greater than or equal to {0}."))
            });

            $.validator.messages.required = function (param, input) {
                var _input = $(input), _name = "";
                // console.log(_input.is("#edit-mail"));
                if (_input.is("#edit-date-end-datepicker-popup-0") == true) {
                    _name = $(input).parents('#edit-date-end').siblings('label').text();
                } else if (_input.is("#edit-date-start-datepicker-popup-0") == true) {
                    _name = $(input).parents('#edit-date-start').siblings('label').text();
                } else if (_input.is("#edit-pass") == true) {
                    _name = $(input).siblings('label').text();
                } else if (_input.is("input[type='text']") || _input.is("input[type='email']")) {
                    _name = $(input).siblings('label').text();
                } else if (_input.is("select")) {
                    _name = $(input).parents('div,span').siblings('label').text();
                } else if (_input.is("textarea")) {
                    _name = $(input).parents('div').siblings('label').text();
                } else if (_input.is("input[type='file']")) {
                    _name = $(input).parents('#edit-image-ajax-wrapper').siblings('label').text();
                } else if (_input.is("input[type='radio']")) {
                    _name = $(input).parents('.form-radios').siblings('label').text();
                } else if (_input.is("input[type='checkbox']")) {
                    _name = Drupal.t("Terms and conditions");
                }
                return _name + Drupal.t(' is required');
            };

            jQuery.validator.addMethod('alphab', function (value, element) {
                return this.optional(element) || /^[a-zA-Z\W\-_\s]+$/.test(value);
            }, Drupal.t("This field accept just letters"));

            jQuery.validator.addMethod('tele', function (value, element) {
                return this.optional(element) || /^(06)((-).[0-9]+){4}$/.test(value);
            }, Drupal.t("Please enter a valid phone number"));

            jQuery.validator.addMethod('fullEmail', function (value, element) {
                return this.optional(element) || /\S+@\S+\.\S+/.test(value);
            }, Drupal.t("Please enter a valid email"));

            jQuery.validator.addMethod('emailOrPhone', function (value, element) {
                return $("#edit-email").val() != "" || $("#edit-phone").val() != "";
            }, Drupal.t("You must provide at least one contact field"));

            if ($_forms.length) {

                $_forms.find('form').each(function () {
                    var _this = $(this),
                        _formid = _this.attr('id'),
                        _errorsHTML = $('<ul></ul>').attr({
                            'id': _formid,
                            'class': 'validation-messages-box alert alert-danger'
                        }).css('display', 'none'),
                        _rules = {};

                    $_forms.prepend(_errorsHTML);

                    var __isTop = false;
                    _this.validate({
                        errorLabelContainer: $('#' + _formid),
                        wrapper: 'li',
                        ignore: '',
                        rules: _rules,
                        onkeyup: false,
                        onclick: false,
                        highlight: function (element) {
                            var phoneLabelIndex = "";
                            $.each(this.errorList, function (key, value) {
                                if (value.element.name == 'phone' && value.method == 'emailOrPhone') {
                                    phoneLabelIndex = key;
                                }
                            });
                            if (phoneLabelIndex != "") {
                                this.errorList.splice(phoneLabelIndex, 1)
                            }
                            if (__isTop == false) {
                                $('html,body').animate({scrollTop: 0}, 700);
                                __isTop = true;
                            }
                            var _el = $(element);
                            if (_el.is('select')) {
                                _el.siblings('button').addClass('error');
                                _el.siblings('.btn-group').find('button').addClass('error');
                            } else if (_el.is('input:radio')) {
                                _el.parents('.form-radios').addClass('radio-error');
                            } else if (_el.is('input:file')) {
                                _el.parents('.form-type-managed-file').addClass('error');
                            } else {
                                _el.addClass('error');
                            }
                        },
                        unhighlight: function (element) {
                            var _el = $(element);
                            if (_el.is('select')) {
                                _el.siblings('button').removeClass('error');
                            } else if (_el.is('input:radio')) {
                                _el.parents('.form-radios').removeClass('radio-error');
                            } else if (_el.is('input:file')) {
                                _el.parents('.form-type-managed-file').removeClass('error');
                            } else {
                                _el.removeClass('error');
                            }
                        }
                    })
                });
            }
        }
    };

    //== Portrait / Landscape detection
    //
    //## Disable Portrait for Tablet & Landscape for Mobile.
    Drupal.vactory.utility.detectInterstitiel = function () {
        var $_body = $('body'),
            $_window = $(window);

        // Init defaults
        // Whatever we have passed this before or not.
        $_body.data('interstitielDisabled', false);

        // Apply interstitiel
        if (matchMedia("(min-width: 768px) and (max-width: 1024px) and (orientation: portrait)").matches || matchMedia("(max-width: 768px) and (orientation: landscape)").matches) {
            $_body.addClass("interstitiel-mode");
        }

        // Wait until innerheight changes, for max 120 frames
        function orientationChanged() {
            var timeout = 120;
            return new window.Promise(function (resolve) {
                var go = function (i, height0) {
                    window.innerHeight != height0 || i >= timeout ?
                        resolve() :
                        window.requestAnimationFrame(function () {
                            go(i + 1, height0)
                        });
                };
                go(0, window.innerHeight);
            });
        }

        $_window.on("orientationchange", function () {
            orientationChanged().then(function() {
                // Apply interstitiel
                if ($_body.data('interstitielDisabled') == false) {
                    if (matchMedia("(min-width: 768px) and (max-width: 1024px) and (orientation: portrait)").matches || matchMedia("(max-width: 768px) and (orientation: landscape)").matches) {
                        $_body.addClass("interstitiel-mode");
                    }
                }
            });
        });

        // Close Interstitiel
        $('#interstitiel-button--close').on("click touchstart", function (e) {
            e.preventDefault();
            $_body.removeClass("interstitiel-mode");
            $_body.data('interstitielDisabled', true);
        });
    };

    // Document Ready.
    $(document).ready(function () {
        Drupal.vactory.utility.gotoStickyButton();
        Drupal.vactory.utility.viewsFilter();
        Drupal.vactory.utility.viewsDateFilter();
        Drupal.vactory.utility.detectInterstitiel();
    });

})(jQuery, Drupal, ResponsiveBootstrapToolkit, this, this.document);
