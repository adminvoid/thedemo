##### 02.Typographie
    @example
    <div class="col-md-6 typo-font sans-serif">
      <h1>Open sans</h1>
      <p>
        ​‌A​‌B​‌C​‌Ć​‌Č​‌D​‌Đ​‌E​‌F​‌G​‌H​‌I​‌J​‌K​‌L​‌M​‌N​‌O​‌P​‌Q​‌R​‌S​‌Š​‌T​‌U​‌V​‌W​‌X​‌Y​‌Z​‌Ž​‌a​‌b​‌c​‌č​‌ć​‌d​‌đ​‌e​‌f​‌g​‌h​‌i​‌j​‌k​‌l​‌m​‌n​‌o​‌p​‌q​‌r​‌s​‌š​‌t​‌u​‌v​‌w​‌x​‌y​‌z​‌ž​‌1​‌2​‌3​‌4​‌5​‌6​‌7​‌8​‌9​‌0​‌‘​‌?​‌’​‌“​‌!​‌”​‌(​‌%​‌)​‌[​‌#​‌]​‌{​‌@​‌}​‌/​‌&​‌<​‌-​‌+​‌÷​‌×​‌=​‌>​‌®​‌©​‌$​‌€​‌£​‌¥​‌¢​‌:​‌;​‌,​‌.​‌*
      </p>
    </div>
    <div class="col-md-6 typo-font serif">
      <h1>Georgia</h1>
      <p>
        ​‌A​‌B​‌C​‌Ć​‌Č​‌D​‌Đ​‌E​‌F​‌G​‌H​‌I​‌J​‌K​‌L​‌M​‌N​‌O​‌P​‌Q​‌R​‌S​‌Š​‌T​‌U​‌V​‌W​‌X​‌Y​‌Z​‌Ž​‌a​‌b​‌c​‌č​‌ć​‌d​‌đ​‌e​‌f​‌g​‌h​‌i​‌j​‌k​‌l​‌m​‌n​‌o​‌p​‌q​‌r​‌s​‌š​‌t​‌u​‌v​‌w​‌x​‌y​‌z​‌ž​‌1​‌2​‌3​‌4​‌5​‌6​‌7​‌8​‌9​‌0​‌‘​‌?​‌’​‌“​‌!​‌”​‌(​‌%​‌)​‌[​‌#​‌]​‌{​‌@​‌}​‌/​‌&​‌<​‌-​‌+​‌÷​‌×​‌=​‌>​‌®​‌©​‌$​‌€​‌£​‌¥​‌¢​‌:​‌;​‌,​‌.​‌*
      </p>
    </div>

##### 03.Titres

    @example
    <div class="sg-heading" data-sg-heading="h1"><h1>h1 open Sans 36px</h1></div>
    <div class="sg-heading" data-sg-heading="h2"><h2>h2 open Sans 30px</h2></div>
    <div class="sg-heading" data-sg-heading="h3"><h3>h3 open Sans 24px</h3></div>
    <div class="sg-heading" data-sg-heading="h4"><h4>h4 open Sans 18px</h2></div>
    <div class="sg-heading" data-sg-heading="h5"><h5>h5 open Sans 14px</h3></div>

##### 04.Textes

    @example
    <div data-sg-heading="p normal" class="sg-heading sg-text">
      <h5 class="sg-text-title text-center large">open Sans</h5>
    </div>
    <p>
      Body style 2 open Sans. ipsum dolor sit amet, consectetur adipiscing elit. Quisque vestibulum, nibh pellentesque vestibulum mattis, lacus tortor posuere nulla, vel sagittis risus mauris ac tortor. Vestibulum et lacus a tellus sodales iaculis id vel dui. Etiam euismod lacus ornare risus egestas dignissim. Fusce mattis justo vitae congue varius. Suspendisse auctor dapibus ornare. Praesent venenatis lacus a sem interdum tempor et vitae magna. Aenean vel consectetur odio. Curabitur malesuada scelerisque massa varius volutpat.
    </p>
    <div data-sg-heading="P bold" class="sg-heading sg-text">
      <h5 class="sg-text-title text-center p-small">open Sans Bold</h5>
    </div>
    <p class="text-bold">
      Le 3 juillet 1777, lors d'un souper auquel il convie une trentaine d'auteurs, Beaumarchais propose la fondation de la première société des auteurs dramatiques. La lutte qu'il décide d'engager aboutit à la reconnaissance légale du droit d'auteur par l'Assemblée Constituante le 13 janvier 1791 (loi ratifiée le 19 janvier 1791 par Louis XVI). C'est la première loi édictée dans le monde pour protéger les auteurs et leurs droits : elle énonce déjà que « la plus sacrée, la plus inattaquable et la plus personnelle de toutes les propriétés est l'ouvrage, fruit de la pensée de l'écrivain ».
    </p>
    <div data-sg-heading="p semi-bold" class="sg-heading sg-text">
      <h5 class="sg-text-title text-center large">open Sans </h5>
    </div>
    <p class="text-semibold">
      Body style 2 open Sans, Regular 16px. ipsum dolor sit amet, consectetur adipiscing elit. Quisque vestibulum, nibh pellentesque vestibulum mattis, lacus tortor posuere nulla, vel sagittis risus mauris ac tortor. Vestibulum et lacus a tellus sodales iaculis id vel dui. Etiam euismod lacus ornare risus egestas dignissim. Fusce mattis justo vitae congue varius. Suspendisse auctor dapibus ornare. Praesent venenatis lacus a sem interdum tempor et vitae magna. Aenean vel consectetur odio. Curabitur malesuada scelerisque massa varius volutpat.
    </p>
    <div class="sg-heading sg-text" data-sg-heading="P small semi-bold">
      <h5 class="sg-text-title text-center p-small">open Sans, semi-bold Small</h5>
    </div>
    <p class="text-small text-semibold">
      Le 3 juillet 1777, lors d'un souper auquel il convie une trentaine d'auteurs, Beaumarchais propose la fondation de la première société des auteurs dramatiques. La lutte qu'il décide d'engager aboutit à la reconnaissance légale du droit d'auteur par l'Assemblée Constituante le 13 janvier 1791 (loi ratifiée le 19 janvier 1791 par Louis XVI). C'est la première loi édictée dans le monde pour protéger les auteurs et leurs droits : elle énonce déjà que « la plus sacrée, la plus inattaquable et la plus personnelle de toutes les propriétés est l'ouvrage, fruit de la pensée de l'écrivain ».
    </p>
    <div class="sg-heading sg-text" data-sg-heading="P small">
      <h5 class="sg-text-title text-center p-small">open Sans, Small</h5>
    </div>
    <p class="text-small">
      Le 3 juillet 1777, lors d'un souper auquel il convie une trentaine d'auteurs, Beaumarchais propose la fondation de la première société des auteurs dramatiques. La lutte qu'il décide d'engager aboutit à la reconnaissance légale du droit d'auteur par l'Assemblée Constituante le 13 janvier 1791 (loi ratifiée le 19 janvier 1791 par Louis XVI). C'est la première loi édictée dans le monde pour protéger les auteurs et leurs droits : elle énonce déjà que « la plus sacrée, la plus inattaquable et la plus personnelle de toutes les propriétés est l'ouvrage, fruit de la pensée de l'écrivain ».
    </p>

##### 05.PULL QUOTE

    @example
    <blockquote class="testimonial quote-half">
        <p>Pull quote we accelerate product-to-market for enterprise clients through a signature metrics-driven process, dedicated product teams, and a blend of User Experience and Engineering expertise.</p>
    </blockquote>
