##### 05.BOUTONS

    @example
    <div class="row">
      <div class="col-md-2">
        <span class="titre-partie">“Boutton Default</span>
        <span class="sg-d-block">Normal</span>
        <a href="#" class="btn btn-default">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">Hover</span>
        <a href="#" class="btn btn-default active">En savoir plus</a>
        <br>
      </div>
      <div class="col-md-2">
        <span class="titre-partie">“Boutton Primary</span>
        <span class="sg-d-block">Normal</span>
        <a href="#" class="btn btn-primary">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">Hover</span>
        <a href="#" class="btn btn-primary active">En savoir plus</a>
        <br>
      </div>
      <div class="col-md-3">
        <span class="titre-partie">“Boutton Secondary</span>
        <span class="sg-d-block">Normal</span>
        <a href="#" class="btn btn-secondary ">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">Hover</span>
        <a href="#" class="btn btn-secondary active">En savoir plus</a>
        <br>
        <a href="#" class="btn go-top ">Back to top</a>
      </div>
      <div class="col-md-3">
      	<span class="titre-partie">“Boutton Sizes</span>
        <span class="sg-d-block">Large</span>
      	<a href="#" class="btn btn-default btn-lg">Ouvrir mon nouveau compte</a>
        <br>
      	<br>
        <span class="sg-d-block">Small</span>
        <a href="#" class="btn btn-default btn-sm">Ouvrir mon nouveau compte</a>
        <br>
        <br>
        <span class="sg-d-block">extra small</span>
        <a href="#" class="btn btn-default btn-xs">Ouvrir mon nouveau compte</a>
        <br>
      	<br>
      </div>
      <div class="col-md-2">
        <span class="titre-partie">“Boutton with icon</span>
        <span class="sg-d-block">on left</span>
        <a href="#" class="btn btn-default active btn-icon btn-icon-left search">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">on right</span>
        <a href="#" class="btn btn-default active btn-icon btn-icon-right download">En savoir plus</a>
        <br>
        <br>
      </div>
    </div>

##### 05.LINKS

    @example
    <div class="row">
      <div class="col-md-2">
        <span class="titre-partie">Default Link</span>
        <span class="sg-d-block">Normal</span>
        <a href="#" class="btn btn-link">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">Hover</span>
        <a href="#" class="btn btn-link active">En savoir plus</a>
        <br>
      </div>
       <div class="col-md-2">
        <span class="titre-partie">Link with Icon</span>
        <span class="sg-d-block">Normal</span>
        <a href="#" class="btn btn-link btn-icon btn-icon-left search">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">Hover</span>
        <a href="#" class="more-link">Read more</a>
        <br>
      </div>
      <div class="col-md-2">
        <span class="titre-partie">Default Link underlined</span>
        <span class="sg-d-block">Normal</span>
        <a href="#" class="btn btn-link underlined">En savoir plus</a>
        <br>
        <br>
        <span class="sg-d-block">Hover</span>
        <a href="#" class="btn btn-link underlined active">En savoir plus</a>
        <br>
      </div>
    </div>


