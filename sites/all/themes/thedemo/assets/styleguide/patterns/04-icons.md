
##### 06. ICÔNES
    @example
    <div class="icons flex-container">
        <div>
          <span class="icon-download">
          </span>
          <span class="mls"> icon-download</span>
        </div>
        <div>
          <span class="icon-check">
          </span>
          <span class="mls"> icon-check</span>
        </div>
        <div>
          <span class="icon-user">
          </span>
          <span class="mls"> icon-user</span>
        </div>
        <div>
          <span class="icon-phone">
          </span>
          <span class="mls"> icon-phone</span>
        </div>
        <div>
          <span class="icon-heart">
          </span>
          <span class="mls"> icon-heart</span>
        </div>
        <div>
          <span class="icon-close">
          </span>
          <span class="mls"> icon-close</span>
        </div>
        <div>
          <span class="icon-close-circle-flat">
          </span>
          <span class="mls"> icon-close-circle-flat</span>
        </div>
        <div>
          <span class="icon-close-flat">
          </span>
          <span class="mls"> icon-close-flat</span>
        </div>
        <div>
          <span class="icon-close-circle">
          </span>
          <span class="mls"> icon-close-circle</span>
        </div>
        <div>
          <span class="icon-close-spaced">
          </span>
          <span class="mls"> icon-close-spaced</span>
        </div>
        <div>
          <span class="icon-close-spaced-flat">
          </span>
          <span class="mls"> icon-close-spaced-flat</span>
        </div>
        <div>
          <span class="icon-chevron-right-flat">
          </span>
          <span class="mls"> icon-chevron-right-flat</span>
        </div>
        <div>
          <span class="icon-chevron-left-flat">
          </span>
          <span class="mls"> icon-chevron-left-flat</span>
        </div>
        <div>
          <span class="icon-chevron-right-flat--circle">
          </span>
          <span class="mls"> icon-chevron-right-flat--circle</span>
        </div>
        <div>
          <span class="icon-chevron-left-flat--circle">
          </span>
          <span class="mls"> icon-chevron-left-flat--circle</span>
        </div>
        <div>
          <span class="icon-arrow-right-flat--circle">
          </span>
          <span class="mls"> icon-arrow-right-flat--circle</span>
        </div>
        <div>
          <span class="icon-arrow-left-flat--circle">
          </span>
          <span class="mls"> icon-arrow-left-flat--circle</span>
        </div>
        <div>
          <span class="icon-arrow-right-flat">
          </span>
          <span class="mls"> icon-arrow-right-flat</span>
        </div>
        <div>
          <span class="icon-arrow-left-flat">
          </span>
          <span class="mls"> icon-arrow-left-flat</span>
        </div>
        <div>
          <span class="icon-chevron-right">
          </span>
          <span class="mls"> icon-chevron-right</span>
        </div>
        <div>
          <span class="icon-chevron-left">
          </span>
          <span class="mls"> icon-chevron-left</span>
        </div>
        <div>
          <span class="icon-chevron-right-circle">
          </span>
          <span class="mls"> icon-chevron-right-circle</span>
        </div>
        <div>
          <span class="icon-arrow-preview">
          </span>
          <span class="mls"> icon-arrow-preview</span>
        </div>
        <div>
          <span class="icon-arrow-right--circle">
          </span>
          <span class="mls"> icon-arrow-right--circle</span>
        </div>
        <div>
          <span class="icon-arrow-left--circle">
          </span>
          <span class="mls"> icon-arrow-left--circle</span>
        </div>
        <div>
          <span class="icon-arrow-right">
          </span>
          <span class="mls"> icon-arrow-right</span>
        </div>
        <div>
          <span class="icon-arrow-left">
          </span>
          <span class="mls"> icon-arrow-left</span>
        </div>
        <div>
          <span class="icon-search">
          </span>
          <span class="mls"> icon-search</span>
        </div>
        <div>
          <span class="icon-search-bolder">
          </span>
          <span class="mls"> icon-search-bolder</span>
        </div>
        <div>
          <span class="icon-search-bold">
          </span>
          <span class="mls"> icon-search-bold</span>
        </div>
        <div>
          <span class="icon-search-spaced">
          </span>
          <span class="mls"> icon-search-spaced</span>
        </div>
        <div>
          <span class="icon-search-light-spaced">
          </span>
          <span class="mls"> icon-search-light-spaced</span>
        </div>
        <div>
          <span class="icon-search-lightest">
          </span>
          <span class="mls"> icon-search-lightest</span>
        </div>
        <div>
          <span class="icon-behance">
          </span>
          <span class="mls"> icon-behance</span>
        </div>
        <div>
          <span class="icon-blogger">
          </span>
          <span class="mls"> icon-blogger</span>
        </div>
        <div>
          <span class="icon-dailymotion">
          </span>
          <span class="mls"> icon-dailymotion</span>
        </div>
        <div>
          <span class="icon-dribbble">
          </span>
          <span class="mls"> icon-dribbble</span>
        </div>
        <div>
          <span class="icon-drive">
          </span>
          <span class="mls"> icon-drive</span>
        </div>
        <div>
          <span class="icon-dropbox">
          </span>
          <span class="mls"> icon-dropbox</span>
        </div>
        <div>
          <span class="icon-drupal">
          </span>
          <span class="mls"> icon-drupal</span>
        </div>
        <div>
          <span class="icon-evernote">
          </span>
          <span class="mls"> icon-evernote</span>
        </div>
        <div>
          <span class="icon-facebook">
          </span>
          <span class="mls"> icon-facebook</span>
        </div>
        <div>
          <span class="icon-github">
          </span>
          <span class="mls"> icon-github</span>
        </div>
        <div>
          <span class="icon-googleplus">
          </span>
          <span class="mls"> icon-googleplus</span>
        </div>
        <div>
          <span class="icon-hangouts">
          </span>
          <span class="mls"> icon-hangouts</span>
        </div>
        <div>
          <span class="icon-instagram">
          </span>
          <span class="mls"> icon-instagram</span>
        </div>
        <div>
          <span class="icon-linkedin">
          </span>
          <span class="mls"> icon-linkedin</span>
        </div>
        <div>
          <span class="icon-messenger">
          </span>
          <span class="mls"> icon-messenger</span>
        </div>
        <div>
          <span class="icon-pinterest">
          </span>
          <span class="mls"> icon-pinterest</span>
        </div>
        <div>
          <span class="icon-pocket">
          </span>
          <span class="mls"> icon-pocket</span>
        </div>
        <div>
          <span class="icon-rss">
          </span>
          <span class="mls"> icon-rss</span>
        </div>
        <div>
          <span class="icon-snapchat">
          </span>
          <span class="mls"> icon-snapchat</span>
        </div>
        <div>
          <span class="icon-souncloud">
          </span>
          <span class="mls"> icon-souncloud</span>
        </div>
        <div>
          <span class="icon-spotify">
          </span>
          <span class="mls"> icon-spotify</span>
        </div>
        <div>
          <span class="icon-tumblr">
          </span>
          <span class="mls"> icon-tumblr</span>
        </div>
        <div>
          <span class="icon-twitter">
          </span>
          <span class="mls"> icon-twitter</span>
        </div>
        <div>
          <span class="icon-vimeo">
          </span>
          <span class="mls"> icon-vimeo</span>
        </div>
        <div>
          <span class="icon-whatsapp">
          </span>
          <span class="mls"> icon-whatsapp</span>
        </div>
        <div>
          <span class="icon-youtube">
          </span>
          <span class="mls"> icon-youtube</span>
        </div>
        <div>
          <span class="icon-burger-menu-bolder">
          </span>
          <span class="mls"> icon-burger-menu-bolder</span>
        </div>
        <div>
          <span class="icon-burger-menu">
          </span>
          <span class="mls"> icon-burger-menu</span>
        </div>
        <div>
          <span class="icon-burger-menu-middle">
          </span>
          <span class="mls"> icon-burger-menu-middle</span>
        </div>
        <div>
          <span class="icon-burger-menu-stairs">
          </span>
          <span class="mls"> icon-burger-menu-stairs</span>
        </div>
        <div>
          <span class="icon-burger-menu-narrow">
          </span>
          <span class="mls"> icon-burger-menu-narrow</span>
        </div>
        <div>
          <span class="icon-burger-menu-narrow-middle">
          </span>
          <span class="mls"> icon-burger-menu-narrow-middle</span>
        </div>
        <div>
          <span class="icon-burger-menu-narrow-stairs">
          </span>
          <span class="mls"> icon-burger-menu-narrow-stairs</span>
        </div>
        <div>
          <span class="icon-burger-menu-narrow-bold-flat">
          </span>
          <span class="mls"> icon-burger-menu-narrow-bold-flat</span>
        </div>
        <div>
          <span class="icon-burger-menu-thin">
          </span>
          <span class="mls"> icon-burger-menu-thin</span>
        </div>
        <div>
          <span class="icon-burger-menu-lighter">
          </span>
          <span class="mls"> icon-burger-menu-lighter</span>
        </div>
    </div>
