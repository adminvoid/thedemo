
##### 09.Cookie layer
    @example
    <div id="sliding-popup" class="sliding-popup-bottom" style="height: auto; width: 100%; bottom: 0px;">
      <div class="popup-content info">
        <div id="popup-text">
          <p>We use cookies on this site to enhance your user experienceBy clicking any link on this page you are giving your consent for us to set cookies.</p>
        </div>
        <div id="popup-buttons">
          <button type="button" class="agree-button">OK, I agree</button>
          <button type="button" class="find-more-button">No, give me more info</button>
        </div>
      </div>
    </div>

##### 09.Lists
    @example
    <div class="row">
      <h2>Unordered Lists</h2>
      <div class="col-md-4">
        <span class="sg-d-block">Default list without style-type</span>
        <ul>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
        </ul>
      </div>
      <div class="col-md-4">
        <span class="sg-d-block">Check list</span>
        <ul class="list-check">
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
        </ul>
      </div>
      <div class="col-md-4">
        <span class="sg-d-block">Square list</span>
        <ul class="list-square">
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <h2>Ordered Lists</h2>
      <div class="col-md-4">
        <span class="sg-d-block">Default ordered list</span>
        <ol>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
        </ol>
      </div>
      <div class="col-md-4">
        <span class="sg-d-block">Custom ordered list</span>
        <ol class="custom-ordered-list">
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
          <li>this is unorderd an item list</li>
        </ol>
      </div>
    </div>

##### 09.Lists
    @example
    <div class="row">
      <div class="col-md-6">
        <table class="table">
          <caption> Table caption : we accelerate product-to-market.</caption>
          <thead>
            <tr>
              <th>/</th>
              <th>First col</th>
              <th>Second col</th>
              <th>Third col</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">First</th>
              <td>data cell</td>
              <td>data cell</td>
              <td>data cell</td>
            </tr>
            <tr>
              <th scope="row">second</th>
              <td>data cell</td>
              <td>data cell</td>
              <td>data cell</td>
            </tr>
            <tr>
              <th scope="row">third</th>
              <td>data cell</td>
              <td>data cell</td>
              <td>data cell</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
