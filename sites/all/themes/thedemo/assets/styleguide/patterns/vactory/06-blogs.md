##### TEMPLATES BLOGS
[//]: #
[//]: #	@example
[//]: #	<div class="blogs grid-listing vactory-slider">
[//]: #    <div class="col-md-4">
[//]: #      <article class="node grid-item node-teaser clearfix" about="/fr/content/[//]: #adipiscing-valde" typeof="sioc:Item foaf:Document" data-thmr="thmr_175">
[//]: #        <div class="author">
[//]: #          <img class="img-rounded" src="http://placehold.it/50x50&text=auteur">
[//]: #          <div class="info"><span class="name">Bertran Travernier</span>
[//]: #          <span>Réalisateur</span></div>
[//]: #        </div>
[//]: #        <div class="text"><h3>
[//]: #          <a href="/fr/content/adipiscing-valde">Adipiscing Valde</a>
[//]: #        </h3></div>
[//]: #        <span class="field field-name-field-date field-type-datetime field-[//]: #label-hidden" data-thmr="thmr_190">
[//]: #          <span class="date-display-single" property="dc:date" [//]: #datatype="xsd:dateTime" content="2016-03-09T23:30:30+01:00" data-[//]: #thmr="thmr_161 thmr_162">9 March 2016</span>
[//]: #        </span>
[//]: #        <div class="read-article-links">
[//]: #          <a href="#" class="read-article btn-label ">→ Accéder au blog</a>
[//]: #        </div>
[//]: #      </article>
[//]: #    </div>
[//]: #    <div class="col-md-4">
[//]: #      <article class="node grid-item node-teaser clearfix" about="/fr/content/[//]: #adipiscing-valde" typeof="sioc:Item foaf:Document" data-thmr="thmr_175">
[//]: #        <div class="author">
[//]: #          <img class="img-rounded" src="http://placehold.it/50x50&text=auteur">
[//]: #          <div class="info"><span class="name">Bertran Travernier</span>
[//]: #          <span>Réalisateur</span></div>
[//]: #        </div>
[//]: #        <div class="text"><h3>
[//]: #          <a href="/fr/content/adipiscing-valde">Appellatio luctus molior [//]: #vicis</a>
[//]: #        </h3></div>
[//]: #        <span class="field field-name-field-date field-type-datetime field-[//]: #label-hidden" data-thmr="thmr_190">
[//]: #          <span class="date-display-single" property="dc:date" [//]: #datatype="xsd:dateTime" content="2016-03-09T23:30:30+01:00" data-[//]: #thmr="thmr_161 thmr_162">9 March 2016</span>
[//]: #        </span>
[//]: #        <div class="read-article-links">
[//]: #          <a href="#" class="read-article btn-label ">→ Accéder au blog</a>
[//]: #        </div>
[//]: #      </article>
[//]: #    </div>
[//]: #    <div class="col-md-4">
[//]: #      <article class="node grid-item node-teaser clearfix" about="/fr/content/[//]: #adipiscing-valde" typeof="sioc:Item foaf:Document" data-thmr="thmr_175">
[//]: #        <div class="author">
[//]: #          <img class="img-rounded" src="http://placehold.it/50x50&text=auteur">
[//]: #          <div class="info"><span class="name">Bertran Travernier</span>
[//]: #          <span>Réalisateur</span></div>
[//]: #        </div>
[//]: #        <div class="text"><h3>
[//]: #          <a href="/fr/content/adipiscing-valde">Eligo enim jumentum lucidus [//]: #magna populus</a>
[//]: #        </h3></div>
[//]: #        <span class="field field-name-field-date field-type-datetime field-[//]: #label-hidden" data-thmr="thmr_190">
[//]: #          <span class="date-display-single" property="dc:date" [//]: #datatype="xsd:dateTime" content="2016-03-09T23:30:30+01:00" data-[//]: #thmr="thmr_161 thmr_162">9 March 2016</span>
[//]: #        </span>
[//]: #        <div class="read-article-links">
[//]: #          <a href="#" class="read-article btn-label ">→ Accéder au blog</a>
[//]: #        </div>
[//]: #      </article>
[//]: #    </div>
[//]: #	</div>
