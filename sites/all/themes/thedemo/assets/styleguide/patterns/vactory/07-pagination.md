##### FILTRES & PAGINATION
###### PAGINATION
	@example
	<div class="item-list">
    <ul class="pagination pager">
      <li class="pager-previous">
        <a href="/fr/actualites?page=1" ></a>
      </li>
      <li class="pager-item">
        <a title="Go to page 1" href="/fr/actualites" >1</a>
      </li>
      <li class="pager-item">
        <a title="Go to page 2" href="/fr/actualites?page=1" >2</a>
      </li>
      <li class="pager-current active">
        <span>3</span>
      </li>
      <li class="pager-item">
        <a title="Go to page 4" href="/fr/actualites?page=3" >4</a>
      </li>
      <li class="pager-item">
        <a title="Go to page 5" href="/fr/actualites?page=4" >5</a>
      </li>
      <li class="pager-ellipsis"><span>…</span></li>
      <li class="pager-item">
        <a title="Go to page 15" href="/fr/actualites?page=15" >15</a>
      </li>
      <li class="pager-next">
        <a href="/fr/actualites?page=3" ></a>
      </li>
    </ul>
	</div>
  <br><br><br>
###### Filtre années
  <div class="item-list">
    <ul class="pager years">
    <li class="pager-current active">
        <span>2016</span>
      </li>
      <li class="pager-item">
        <a title="Filter by year 2017" href="#" >2017</a>
      </li>
      <li class="pager-item">
        <a title="Filter by year 2018" href="#" >2018</a>
      </li>
      <li class="pager-item">
        <a title="Filter by year 2019" href="#" >2019</a>
      </li>
      <li class="pager-ellipsis"><span>…</span></li>
      <li class="pager-item">
        <a title="Filter by year 2020" href="#" >2020</a>
      </li>
      <li class="pager-item">
        <a title="Filter by year 2022" href="#" >2022</a>
      </li>
    </ul>
    <br>
  </div>

###### Filtres Discipline And Typologie
  <div>
    <label>Filtrer par  : </label>
    <div class="dropdown filter">
      <button class="dropdown-toggle" type="button" data-toggle="dropdown">DISCIPLINE
      <span class="icon-chevron-down"></span></button>
      <ul class="dropdown-menu">
        <li><a href="#">Filter 1</a></li>
        <li><a href="#">Filter 2</a></li>
        <li><a href="#">Filter 3</a></li>
      </ul>
    </div>
    <div class="dropdown filter">
      <button class="dropdown-toggle" type="button" data-toggle="dropdown">TYPOLOGIE
      <span class="icon-chevron-down"></span></button>
      <ul class="dropdown-menu">
        <li><a href="#">Filter 1</a></li>
        <li><a href="#">Filter 2</a></li>
        <li><a href="#">Filter 3</a></li>
      </ul>
    </div>
    <br><br><br>
  </div>
