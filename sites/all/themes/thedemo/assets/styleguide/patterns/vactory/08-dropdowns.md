##### DROPDOWNS

	@example
	<div class="dropdown-wr">
    <div class="dropdown ">
      <button class="dropdown-toggle" type="button" data-toggle="dropdown">La maison des auteurs
      <span class="icon-chevron-down"></span></button>
      <ul class="dropdown-menu">
        <li><a href="#">Service 1</a></li>
        <li><a href="#">Service 2</a></li>
        <li><a href="#">Service 3</a></li>
      </ul>
    </div>
    <br><br><br><br>
    <div class="dropdown filter">
      <button class="dropdown-toggle" type="button" data-toggle="dropdown">DISCIPLINE
      <span class="icon-chevron-down"></span></button>
      <ul class="dropdown-menu">
        <li><a href="#">Filter 1</a></li>
        <li><a href="#">Filter 2</a></li>
        <li><a href="#">Filter 3</a></li>
      </ul>
    </div>
    <br><br><br><br>
    <div class="dropdown-multiple">
      <select class="" multiple="multiple">
        <option value="1">Formations & Emplois</option>
        <option value="2">Manifestations & Spectacles</option>
        <option value="3">Autre</option>
      </select>
    </div>
    <br><br><br><br><br><br><br>
	</div>
