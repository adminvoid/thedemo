##### TEMPLATES CITATION

    @example
    <blockquote class="testimonial">
      <img src="http://placehold.it/85x85&amp;text=auteur" class="img-rounded">
      <div>
        <div class="text">
          <p>Ce n'est rien d'entreprendre une chose dangereuse, mais d'échapper au péril en la menant à bien</p>
        </div>
        <div class="author">
          <div class="info">
            <span>Pierre Augustin Caron de Beaumarchais</span>
          </div>
        </div>
      </div>
    </blockquote>
    <blockquote class="testimonial">
      <div>
        <div class="text">
          <p>Ce n'est rien d'entreprendre une chose dangereuse, mais d'échapper au péril en la menant à bien</p>
        </div>
        <div class="author">
          <div class="info">
            <span>Pierre Augustin Caron de Beaumarchais</span>
          </div>
        </div>
      <div>
    </blockquote>
