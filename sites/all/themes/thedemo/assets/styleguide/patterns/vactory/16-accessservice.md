##### Accès service

    @example
    <div class="service-access">
      <label>Accéder au service :</label>
      <div class="dropdown">
        <button class="dropdown-toggle l-size" type="button" data-toggle="dropdown">La maison des auteurs
        <span class="icon-chevron-down"></span></button>
        <ul class="dropdown-menu">
          <li><a href="#">Service 1</a></li>
          <li><a href="#">Service 2</a></li>
          <li><a href="#">Service 3</a></li>
        </ul>
      </div>
      <br/><br/><br/><br/><br/>
    </div>
