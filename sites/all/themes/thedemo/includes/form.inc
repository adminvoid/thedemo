<?php
/**
 * @file
 * Theme and preprocess functions for forms
 */

function thedemo_form_search_block_form_alter(&$form, $form_state) {
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
}
