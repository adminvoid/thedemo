<?php
/**
 * @file
 * Theme and preprocess functions for search.
 */


/**
 * Implements hook_preprocess_search_results().
 */
function thedemo_preprocess_search_results(&$vars) {
  $vars['search_totals'] = '';

  if (isset($GLOBALS['pager_total_items'])) {
    // Get the total number of results from the global pager
    $total = $GLOBALS['pager_total_items'][0];
    $vars['search_totals'] = t('A total of !total !results_label found.', array(
      '!total'         => $total,
      // Be smart about labels: show "result" for one, "results" for multiple
      '!results_label' => format_plural($total, 'result', 'results'),
    ));
  }

}
