<?php
/**
 * @file
 * Theme functions
 */

// Include all files from the includes directory.
$includes_path = dirname(__FILE__) . '/includes/*.inc';
foreach (glob($includes_path) as $filename) {
  require_once dirname(__FILE__) . '/includes/' . basename($filename);
}

/**
 * Implements template_preprocess_page().
 */
function thedemo_preprocess_page(&$variables) {

  // Modernizr.
  drupal_add_js(libraries_get_path("modernizr")."/modernizr-custom.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // Bootstrap select.
  drupal_add_js(libraries_get_path("bootstrap-select")."/dist/js/bootstrap-select.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // Bootstrap date picker.
  drupal_add_js(libraries_get_path("bootstrap-datepicker")."/dist/js/bootstrap-datepicker.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));
  drupal_add_css(libraries_get_path("bootstrap-datepicker")."/dist/css/bootstrap-datepicker3.css");

  // Bootstrap multiselect.
  drupal_add_js(libraries_get_path("bootstrap-multiselect")."/dist/js/bootstrap-multiselect.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // jQuery Validator.
  drupal_add_js(libraries_get_path("jquery-validation")."/dist/jquery.validate.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // Bootstrap validator.
  drupal_add_js(libraries_get_path("bootstrap-validator")."/dist/validator.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // Tweenmax.
//  drupal_add_js(libraries_get_path("gsap")."/src/minified/TweenMax.min.js",
//    array("scope" => "footer", "group" => JS_LIBRARY));

  // Scrollreveal.
  drupal_add_js(libraries_get_path("scrollreveal")."/dist/scrollreveal.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY));

  // Slick.
  $slick_path = libraries_get_path('slick');
  drupal_add_js($slick_path."/slick/slick.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));
  drupal_add_css($slick_path . '/slick/slick.css');
  drupal_add_css($slick_path . '/slick/slick-theme.css');

  // FancyBox
  drupal_add_js(libraries_get_path("fancybox")."/dist/jquery.fancybox.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));
  drupal_add_css(libraries_get_path("fancybox")."/dist/jquery.fancybox.css");

  // Masonry
  drupal_add_js(libraries_get_path("masonry-layout")."/dist/masonry.pkgd.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // Responsive toolkit.
  drupal_add_js(libraries_get_path("responsive-bootstrap-toolkit")."/dist/bootstrap-toolkit.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

  // Waypoints.
  drupal_add_js(libraries_get_path("waypoints")."/jquery.waypoints.min.js",
    array("scope" => "footer", "group" => JS_LIBRARY, "requires_jquery" => TRUE,));

    // Custom theme script.
  drupal_add_js(path_to_theme() . "/assets/js/thedemo.script.js",
    array("scope" => "footer", "group" => JS_THEME, "requires_jquery" => TRUE,));

}


/**
 * Implements hook_advagg_js_pre_alter().
 */
function thedemo_advagg_js_pre_alter($javascript) {
  foreach ($javascript as $type => $data) {
    foreach ($data as $path => $info) {
      if ($path == 'load.sumome.com') {
        $javascript[$type][$path]['preprocess'] = FALSE;
      }
    }
  }
}

/**
 * Implements template_preprocess_block().
 */
function thedemo_preprocess_block(&$variables, $hook) {
    //Removing blocks title
    $variables['elements']['#block']->subject = NULL;
}

/*
 * Implements preprocess node
 */
function thedemo_preprocess_node(&$variables) {
  //Removing language links in node links
  unset($variables['content']['links']['translation']);
}

/**
 * Implements theme_breacrumb().
 */
function thedemo_breadcrumb($variables) {
    $breadcrumb = $variables['breadcrumb'];
    if (!empty($breadcrumb)) {
        // Provide a navigational heading to give context for breadcrumb links to
        // screen-reader users. Make the heading invisible with .element-invisible.
        $output = '<div class="container"><div class="row"><div class="col-md-12"><h2 class="element-invisible">' . t('You are here') . '</h2>';
        $output .= '<ul class="breadcrumb">';
        foreach ($breadcrumb as $key => $value) {
          $output .= '<li>' . $value . '</li>';
        }
        $output .= '</ul></div></div></div>';
        return $output;
    }
}


/**
 * Implement hook_form_alter().
 */
function thedemo_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'mailchimp_signup_subscribe_block_newsletter_form') {
      unset( $form['mergevars']['EMAIL']['#title']);
      unset( $form['mergevars']['FNAME']['#title']);
      unset( $form['mergevars']['LNAME']['#title']);
      $form['mergevars']['EMAIL']['#attributes']['placeholder'] = t('Your email address');
      $form['mergevars']['FNAME']['#attributes']['placeholder'] = t('First Name');
      $form['mergevars']['LNAME']['#attributes']['placeholder'] = t('Last Name');
    }
}
