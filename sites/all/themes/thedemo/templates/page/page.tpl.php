<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>
<div class="main-wrap">
<?php print render($page['top']); ?>
<header id="header" class="header" role="header">
     <?php print render($page['header']); ?>
</header>

<div id="main-wrapper">
  <div id="main" class="main">
        <?php print render($page['bridge']); ?>
        <?php if ($messages): ?>
          <div id="messages" class="container">
            <div class="row">
              <?php print $messages; ?>
            </div>
          </div>
        <?php endif; ?>
        <div id="page-header">
          <?php if ($tabs): ?>
            <div class="container">
              <div class="tabs">
                <?php print render($tabs); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <div class="container">
              <ul class="action-links">
                <?php print render($action_links); ?>
              </ul>
            </div>
          <?php endif; ?>
        </div>

    <div id="content">
      <?php print render($page['content']); ?>
    </div>
  </div> <!-- /#main -->

    <?php print render($page['footer']); ?>
    <small class="pull-right"><a href="#" class="go-top"><?php print t('Top'); ?></a></small>
</div> <!-- /#main-wrapper -->
</div> <!-- /.main-wrap -->
<?php print render($page['bottom']); ?>
